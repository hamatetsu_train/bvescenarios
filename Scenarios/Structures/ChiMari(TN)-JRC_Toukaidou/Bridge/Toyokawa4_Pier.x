xof 0303txt 0032
template Vector {
 <3d82ab5e-62da-11cf-ab39-0020af71e433>
 FLOAT x;
 FLOAT y;
 FLOAT z;
}

template MeshFace {
 <3d82ab5f-62da-11cf-ab39-0020af71e433>
 DWORD nFaceVertexIndices;
 array DWORD faceVertexIndices[nFaceVertexIndices];
}

template Mesh {
 <3d82ab44-62da-11cf-ab39-0020af71e433>
 DWORD nVertices;
 array Vector vertices[nVertices];
 DWORD nFaces;
 array MeshFace faces[nFaces];
 [...]
}

template ColorRGBA {
 <35ff44e0-6c7c-11cf-8f52-0040333594a3>
 FLOAT red;
 FLOAT green;
 FLOAT blue;
 FLOAT alpha;
}

template ColorRGB {
 <d3e16e81-7835-11cf-8f52-0040333594a3>
 FLOAT red;
 FLOAT green;
 FLOAT blue;
}

template Material {
 <3d82ab4d-62da-11cf-ab39-0020af71e433>
 ColorRGBA faceColor;
 FLOAT power;
 ColorRGB specularColor;
 ColorRGB emissiveColor;
 [...]
}

template MeshMaterialList {
 <f6f23f42-7686-11cf-8f52-0040333594a3>
 DWORD nMaterials;
 DWORD nFaceIndexes;
 array DWORD faceIndexes[nFaceIndexes];
 [Material <3d82ab4d-62da-11cf-ab39-0020af71e433>]
}

template TextureFilename {
 <a42790e1-7810-11cf-8f52-0040333594a3>
 STRING filename;
}

template Coords2d {
 <f6f23f44-7686-11cf-8f52-0040333594a3>
 FLOAT u;
 FLOAT v;
}

template MeshTextureCoords {
 <f6f23f40-7686-11cf-8f52-0040333594a3>
 DWORD nTextureCoords;
 array Coords2d textureCoords[nTextureCoords];
}

template MeshNormals {
 <f6f23f43-7686-11cf-8f52-0040333594a3>
 DWORD nNormals;
 array Vector normals[nNormals];
 DWORD nFaceNormals;
 array MeshFace faceNormals[nFaceNormals];
}


Mesh  {
 10;
 -1.300000;-1.500000;-0.600000;,
 -1.600000;-1.500000;0.000000;,
 -1.300000;-1.500000;0.600000;,
 1.300000;-1.500000;0.600000;,
 1.600000;-1.500000;0.000000;,
 1.300000;-1.500000;-0.600000;,
 -1.400000;-7.000000;-0.800000;,
 -1.700000;-7.000000;0.000000;,
 1.700000;-7.000000;0.000000;,
 1.400000;-7.000000;-0.800000;;
 10;
 3;0,1,2;,
 3;0,2,3;,
 3;0,3,4;,
 3;0,4,5;,
 3;6,7,1;,
 3;6,1,0;,
 3;8,9,5;,
 3;8,5,4;,
 3;0,5,9;,
 3;0,9,6;;

 MeshMaterialList  {
  1;
  10;
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0;

  Material  {
   1.000000;1.000000;1.000000;1.000000;;
   0.000000;
   0.000000;0.000000;0.000000;;
   0.000000;0.000000;0.000000;;

   TextureFilename  {
    "Pier2.png";
   }
  }
 }

 MeshTextureCoords  {
  10;
  0.100000;0.050000;,
  0.050000;0.000000;,
  0.000000;0.050000;,
  0.000000;0.150000;,
  0.050000;0.200000;,
  0.100000;0.150000;,
  0.100000;0.050000;,
  0.000000;0.000000;,
  0.000000;0.200000;,
  0.100000;0.150000;;
 }

 MeshNormals  {
  10;
  0.000000;1.000000;0.000000;,
  0.000000;1.000000;0.000000;,
  0.000000;1.000000;0.000000;,
  0.000000;1.000000;0.000000;,
  -0.936193;0.017022;-0.351073;,
  -0.893954;0.032507;-0.446977;,
  0.935914;0.029779;-0.350968;,
  0.894309;0.016260;-0.447155;,
  0.000000;0.036340;-0.999339;,
  0.000000;0.036340;-0.999339;;
  10;
  3;0,0,0;,
  3;1,1,1;,
  3;2,2,2;,
  3;3,3,3;,
  3;4,4,4;,
  3;5,5,5;,
  3;6,6,6;,
  3;7,7,7;,
  3;8,8,8;,
  3;9,9,9;;
 }
}