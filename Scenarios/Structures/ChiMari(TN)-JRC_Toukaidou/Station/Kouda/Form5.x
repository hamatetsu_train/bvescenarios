xof 0303txt 0032
template Vector {
 <3d82ab5e-62da-11cf-ab39-0020af71e433>
 FLOAT x;
 FLOAT y;
 FLOAT z;
}

template MeshFace {
 <3d82ab5f-62da-11cf-ab39-0020af71e433>
 DWORD nFaceVertexIndices;
 array DWORD faceVertexIndices[nFaceVertexIndices];
}

template Mesh {
 <3d82ab44-62da-11cf-ab39-0020af71e433>
 DWORD nVertices;
 array Vector vertices[nVertices];
 DWORD nFaces;
 array MeshFace faces[nFaces];
 [...]
}

template ColorRGBA {
 <35ff44e0-6c7c-11cf-8f52-0040333594a3>
 FLOAT red;
 FLOAT green;
 FLOAT blue;
 FLOAT alpha;
}

template ColorRGB {
 <d3e16e81-7835-11cf-8f52-0040333594a3>
 FLOAT red;
 FLOAT green;
 FLOAT blue;
}

template Material {
 <3d82ab4d-62da-11cf-ab39-0020af71e433>
 ColorRGBA faceColor;
 FLOAT power;
 ColorRGB specularColor;
 ColorRGB emissiveColor;
 [...]
}

template MeshMaterialList {
 <f6f23f42-7686-11cf-8f52-0040333594a3>
 DWORD nMaterials;
 DWORD nFaceIndexes;
 array DWORD faceIndexes[nFaceIndexes];
 [Material <3d82ab4d-62da-11cf-ab39-0020af71e433>]
}

template TextureFilename {
 <a42790e1-7810-11cf-8f52-0040333594a3>
 STRING filename;
}

template Coords2d {
 <f6f23f44-7686-11cf-8f52-0040333594a3>
 FLOAT u;
 FLOAT v;
}

template MeshTextureCoords {
 <f6f23f40-7686-11cf-8f52-0040333594a3>
 DWORD nTextureCoords;
 array Coords2d textureCoords[nTextureCoords];
}

template MeshNormals {
 <f6f23f43-7686-11cf-8f52-0040333594a3>
 DWORD nNormals;
 array Vector normals[nNormals];
 DWORD nFaceNormals;
 array MeshFace faceNormals[nFaceNormals];
}


Mesh  {
 18;
 1.500000;-0.450000;0.000000;,
 1.500000;1.100000;0.000000;,
 2.700000;1.100000;0.000000;,
 1.484200;-0.450000;12.500000;,
 1.484200;1.100000;12.500000;,
 2.684200;1.100000;12.500000;,
 1.500000;-0.450000;25.000000;,
 1.500000;1.100000;25.000000;,
 2.700000;1.100000;25.000000;,
 8.237000;1.100000;0.000000;,
 7.037000;1.100000;0.000000;,
 8.299700;1.100000;25.000000;,
 7.099700;1.100000;25.000000;,
 2.700000;1.100000;0.000000;,
 7.037000;1.100000;0.000000;,
 2.684200;1.100000;12.500000;,
 2.700000;1.100000;25.000000;,
 7.099700;1.100000;25.000000;;
 13;
 3;3,4,1;,
 3;3,1,0;,
 3;4,5,2;,
 3;4,2,1;,
 3;6,7,4;,
 3;6,4,3;,
 3;7,8,5;,
 3;7,5,4;,
 3;9,10,12;,
 3;9,12,11;,
 3;13,15,16;,
 3;13,16,17;,
 3;13,17,14;;

 MeshMaterialList  {
  3;
  13;
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  1,
  1,
  2,
  2,
  2;

  Material  {
   1.000000;1.000000;1.000000;1.000000;;
   0.000000;
   0.000000;0.000000;0.000000;;
   0.000000;0.000000;0.000000;;

   TextureFilename  {
    "FormSide1.png";
   }
  }

  Material  {
   1.000000;1.000000;1.000000;1.000000;;
   0.000000;
   0.000000;0.000000;0.000000;;
   0.000000;0.000000;0.000000;;

   TextureFilename  {
    "FormSide1.png";
   }
  }

  Material  {
   1.000000;1.000000;1.000000;1.000000;;
   0.000000;
   0.000000;0.000000;0.000000;;
   0.000000;0.000000;0.000000;;

   TextureFilename  {
    "FormC.png";
   }
  }
 }

 MeshTextureCoords  {
  18;
  0.000000;1.000000;,
  0.000000;0.350000;,
  0.000000;0.000000;,
  1.250000;1.000000;,
  1.250000;0.350000;,
  1.250000;0.000000;,
  2.500000;1.000000;,
  2.500000;0.350000;,
  2.500000;0.000000;,
  0.000000;0.350000;,
  0.000000;0.000000;,
  2.500000;0.350000;,
  2.500000;0.000000;,
  0.000000;0.000000;,
  0.986000;0.000000;,
  0.000000;0.500000;,
  1.000000;1.000000;,
  0.000000;1.000000;;
 }

 MeshNormals  {
  13;
  -0.999999;0.000000;-0.001264;,
  -0.999999;0.000000;-0.001264;,
  0.000000;1.000000;0.000000;,
  0.000000;1.000000;0.000000;,
  -0.999999;0.000000;0.001264;,
  -0.999999;0.000000;0.001264;,
  0.000000;1.000000;0.000000;,
  0.000000;1.000000;0.000000;,
  0.000000;1.000000;0.000000;,
  0.000000;1.000000;0.000000;,
  0.000000;1.000000;0.000000;,
  0.000000;1.000000;0.000000;,
  0.000000;1.000000;0.000000;;
  13;
  3;0,0,0;,
  3;1,1,1;,
  3;2,2,2;,
  3;3,3,3;,
  3;4,4,4;,
  3;5,5,5;,
  3;6,6,6;,
  3;7,7,7;,
  3;8,8,8;,
  3;9,9,9;,
  3;10,10,10;,
  3;11,11,11;,
  3;12,12,12;;
 }
}