xof 0303txt 0032
template Vector {
 <3d82ab5e-62da-11cf-ab39-0020af71e433>
 FLOAT x;
 FLOAT y;
 FLOAT z;
}

template MeshFace {
 <3d82ab5f-62da-11cf-ab39-0020af71e433>
 DWORD nFaceVertexIndices;
 array DWORD faceVertexIndices[nFaceVertexIndices];
}

template Mesh {
 <3d82ab44-62da-11cf-ab39-0020af71e433>
 DWORD nVertices;
 array Vector vertices[nVertices];
 DWORD nFaces;
 array MeshFace faces[nFaces];
 [...]
}

template ColorRGBA {
 <35ff44e0-6c7c-11cf-8f52-0040333594a3>
 FLOAT red;
 FLOAT green;
 FLOAT blue;
 FLOAT alpha;
}

template ColorRGB {
 <d3e16e81-7835-11cf-8f52-0040333594a3>
 FLOAT red;
 FLOAT green;
 FLOAT blue;
}

template Material {
 <3d82ab4d-62da-11cf-ab39-0020af71e433>
 ColorRGBA faceColor;
 FLOAT power;
 ColorRGB specularColor;
 ColorRGB emissiveColor;
 [...]
}

template MeshMaterialList {
 <f6f23f42-7686-11cf-8f52-0040333594a3>
 DWORD nMaterials;
 DWORD nFaceIndexes;
 array DWORD faceIndexes[nFaceIndexes];
 [Material <3d82ab4d-62da-11cf-ab39-0020af71e433>]
}

template TextureFilename {
 <a42790e1-7810-11cf-8f52-0040333594a3>
 STRING filename;
}

template Coords2d {
 <f6f23f44-7686-11cf-8f52-0040333594a3>
 FLOAT u;
 FLOAT v;
}

template MeshTextureCoords {
 <f6f23f40-7686-11cf-8f52-0040333594a3>
 DWORD nTextureCoords;
 array Coords2d textureCoords[nTextureCoords];
}

template MeshNormals {
 <f6f23f43-7686-11cf-8f52-0040333594a3>
 DWORD nNormals;
 array Vector normals[nNormals];
 DWORD nFaceNormals;
 array MeshFace faceNormals[nFaceNormals];
}


Mesh  {
 19;
 1.500000;1.103000;-0.050000;,
 1.484400;1.103000;12.500000;,
 1.500000;1.103000;25.049999;,
 3.225000;1.103000;-0.050000;,
 2.650000;1.103000;25.049999;,
 1.500000;0.850000;-0.050000;,
 1.500000;1.103000;-0.050000;,
 1.484400;0.850000;12.500000;,
 1.484400;1.103000;12.500000;,
 1.500000;0.850000;25.049999;,
 1.500000;1.103000;25.049999;,
 4.300000;-0.850000;-0.050000;,
 4.300000;0.860000;-0.050000;,
 1.500000;0.860000;-0.050000;,
 1.500000;-0.850000;-0.050000;,
 4.284400;-0.850000;12.500000;,
 4.284400;0.850000;12.500000;,
 1.484400;0.850000;12.500000;,
 1.484400;-0.850000;12.500000;;
 11;
 3;0,1,2;,
 3;0,2,4;,
 3;0,4,3;,
 3;7,8,6;,
 3;7,6,5;,
 3;9,10,8;,
 3;9,8,7;,
 3;14,13,12;,
 3;14,12,11;,
 3;18,17,16;,
 3;18,16,15;;

 MeshMaterialList  {
  4;
  11;
  0,
  0,
  0,
  1,
  1,
  1,
  1,
  2,
  2,
  3,
  3;

  Material  {
   0.686275;0.686275;0.686275;1.000000;;
   0.000000;
   0.000000;0.000000;0.000000;;
   0.000000;0.000000;0.000000;;

   TextureFilename  {
    "FormC2.png";
   }
  }

  Material  {
   0.686275;0.686275;0.686275;1.000000;;
   0.000000;
   0.000000;0.000000;0.000000;;
   0.000000;0.000000;0.000000;;

   TextureFilename  {
    "FormSide3.png";
   }
  }

  Material  {
   1.000000;1.000000;1.000000;1.000000;;
   0.000000;
   0.000000;0.000000;0.000000;;
   0.000000;0.000000;0.000000;;

   TextureFilename  {
    "Iron7.png";
   }
  }

  Material  {
   1.000000;1.000000;1.000000;1.000000;;
   0.000000;
   0.000000;0.000000;0.000000;;
   0.000000;0.000000;0.000000;;

   TextureFilename  {
    "Iron7.png";
   }
  }
 }

 MeshTextureCoords  {
  19;
  0.000000;0.000000;,
  0.625000;0.000000;,
  1.250000;0.000000;,
  0.000000;0.750000;,
  1.250000;0.500000;,
  0.000000;1.000000;,
  0.000000;0.000000;,
  0.250000;1.000000;,
  0.250000;0.000000;,
  1.000000;1.000000;,
  1.000000;0.000000;,
  1.000000;1.000000;,
  1.000000;0.000000;,
  0.000000;0.000000;,
  0.000000;1.000000;,
  1.000000;1.000000;,
  1.000000;0.000000;,
  0.000000;0.000000;,
  0.000000;1.000000;;
 }

 MeshNormals  {
  11;
  0.000000;1.000000;0.000000;,
  0.000000;1.000000;0.000000;,
  0.000000;1.000000;0.000000;,
  -0.999999;0.000000;-0.001243;,
  -0.999999;0.000000;-0.001243;,
  -0.999999;0.000000;0.001243;,
  -0.999999;0.000000;0.001243;,
  0.000000;0.000000;-1.000000;,
  0.000000;0.000000;-1.000000;,
  0.000000;0.000000;-1.000000;,
  0.000000;0.000000;-1.000000;;
  11;
  3;0,0,0;,
  3;1,1,1;,
  3;2,2,2;,
  3;3,3,3;,
  3;4,4,4;,
  3;5,5,5;,
  3;6,6,6;,
  3;7,7,7;,
  3;8,8,8;,
  3;9,9,9;,
  3;10,10,10;;
 }
}