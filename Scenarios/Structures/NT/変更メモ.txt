★2019/05/01 Ver 0.99 - rev.2
　予土線更新に絡む更新

●更新
　　BallastPC_K_5aD.x
　　PoleFenceXX（足の長さの調整）
　　road_045_X（明るさの修正）
　　橋　conc_5m・conc_10m・conc_25m（テクスチャ修正）
　　Pier.x（上蓋追加）
　　単線架線柱の透過色調整　※別の問題も出ているので様子見 再修正の可能性あり
　　トンネル用バラストの暗さを上げた（BallastPC_K_5xD.x）
　　2go_c1_escL.x・2go_c1_escR.xの隙間修正
　　ホーム　ちらつきを修正

○Defaultフォルダ
　Stop.x　明るさを調整

○Tunnelフォルダ
　トンネルの明るさを調整
　トンネル　xxx_c1_lp.x系（テクスチャconc01_wall.png小修整）

　他もたぶんいろいろある…（忘れた）


●追加
○Accessoryフォルダ
　　キロポスト垂直型（kilopost1e.x・kilopost2e.x）
　　トンネル内用キロポスト（kilopost〜_d.x） ※九州系除く
　　トンネル内用各種標識（Posts\Tunnel）
　　JR四国の歓呼標（Posts\JRS）

○Bridgeフォルダ
　　橋　concL_5m・concL_10m・concL_25m（幅6mの太めのコンクリ橋）

○Dikeフォルダ
　　より鋭角な擁壁（CrDike2Lx/CrDike2Rx）
　　短い足の土手（DikeGrs5mSLx/DikeGrs5mSRx）
　　コンクリ土手の蓋（DikeFace2.x・DikeFace3.x）

○Natureフォルダ
　　X_side_Grassの勾配対応版を仮追加（Ground\Lside_GrassDch.x・Rside_GrassDch.x）
　　トンネル入口の山L（mount\GrsWall_TunL01.x・GrsWall_TunL02.x）
　　畑らしきもの（Ground\fieldL_25.x・fieldR_25.x）
　　並行の地面（Ground\Grass100m.x）
　　並行の水面（River\river250mL_green.x）
　　並行の河原（River\kawara250m.x）
　　雑草（bushM・bushMnフォルダ）　※透過透けを起こすので注意、対策版もあるが副作用アリ

○Tunnelフォルダ
　　トンネル内ワイヤー（wireL.x・wireR.x）



★2018/07/15 Ver 0.99 - rev.1
　日豊線とともに新規公開