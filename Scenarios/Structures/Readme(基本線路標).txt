--------------------------------------------------
フリーストラクチャ 基本線路標セット（skp付/BVE5用）

かんたんのゆめ　NT/fiv
2016/7/18
http://kantanbay.org/
http://kty-bvememo.tumblr.com/
--------------------------------------------------

・とりあえず雑ですが時間ないので仕様

・SKPやPSDも同梱していますので好きにしてください
・BVE以外でも自由に使用可能です
・再配布はsourceフォルダ以外は自由に行って構いません

曲線標（curve）
・GradualDiminution_P.xは逓減標
・GradualDiminution_Ps.xは大きさを1.5倍で出力

距離標（kilo）
・甲型・乙型・丙型・九州西日本方面仕様丙型
・Repeaterで設置するとラクチンです

勾配標（gradient）
・D＝Down、L＝平坦、U＝Up
