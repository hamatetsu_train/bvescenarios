xof 0303txt 0032
template Vector {
 <3d82ab5e-62da-11cf-ab39-0020af71e433>
 FLOAT x;
 FLOAT y;
 FLOAT z;
}

template MeshFace {
 <3d82ab5f-62da-11cf-ab39-0020af71e433>
 DWORD nFaceVertexIndices;
 array DWORD faceVertexIndices[nFaceVertexIndices];
}

template Mesh {
 <3d82ab44-62da-11cf-ab39-0020af71e433>
 DWORD nVertices;
 array Vector vertices[nVertices];
 DWORD nFaces;
 array MeshFace faces[nFaces];
 [...]
}

template ColorRGBA {
 <35ff44e0-6c7c-11cf-8f52-0040333594a3>
 FLOAT red;
 FLOAT green;
 FLOAT blue;
 FLOAT alpha;
}

template ColorRGB {
 <d3e16e81-7835-11cf-8f52-0040333594a3>
 FLOAT red;
 FLOAT green;
 FLOAT blue;
}

template Material {
 <3d82ab4d-62da-11cf-ab39-0020af71e433>
 ColorRGBA faceColor;
 FLOAT power;
 ColorRGB specularColor;
 ColorRGB emissiveColor;
 [...]
}

template MeshMaterialList {
 <f6f23f42-7686-11cf-8f52-0040333594a3>
 DWORD nMaterials;
 DWORD nFaceIndexes;
 array DWORD faceIndexes[nFaceIndexes];
 [Material <3d82ab4d-62da-11cf-ab39-0020af71e433>]
}

template TextureFilename {
 <a42790e1-7810-11cf-8f52-0040333594a3>
 STRING filename;
}

template Coords2d {
 <f6f23f44-7686-11cf-8f52-0040333594a3>
 FLOAT u;
 FLOAT v;
}

template MeshTextureCoords {
 <f6f23f40-7686-11cf-8f52-0040333594a3>
 DWORD nTextureCoords;
 array Coords2d textureCoords[nTextureCoords];
}

template MeshNormals {
 <f6f23f43-7686-11cf-8f52-0040333594a3>
 DWORD nNormals;
 array Vector normals[nNormals];
 DWORD nFaceNormals;
 array MeshFace faceNormals[nFaceNormals];
}


Mesh  {
 18;
 -3.500000;0.000000;0.000000;,
 -3.500000;0.250000;0.000000;,
 -3.700000;0.250000;0.000000;,
 -3.500000;0.000000;25.000000;,
 -3.500000;0.250000;25.000000;,
 -3.700000;0.250000;25.000000;,
 -3.700000;0.250000;0.000000;,
 -3.700000;0.250000;25.000000;,
 -5.700000;0.250000;0.000000;,
 -5.700000;0.250000;25.000000;,
 -5.700000;-5.000000;0.000000;,
 -5.700000;0.250000;0.000000;,
 -5.700000;-5.000000;25.000000;,
 -5.700000;0.250000;25.000000;,
 -5.600000;0.250000;0.000000;,
 -5.600000;1.250000;0.000000;,
 -5.600000;0.250000;25.000000;,
 -5.600000;1.250000;25.000000;;
 12;
 3;0,1,4;,
 3;0,4,3;,
 3;1,2,5;,
 3;1,5,4;,
 3;8,9,7;,
 3;8,7,6;,
 3;12,13,11;,
 3;12,11,10;,
 3;14,15,17;,
 3;14,17,16;,
 3;16,17,15;,
 3;16,15,14;;

 MeshMaterialList  {
  4;
  12;
  0,
  0,
  0,
  0,
  1,
  1,
  2,
  2,
  3,
  3,
  3,
  3;

  Material  {
   1.000000;1.000000;1.000000;1.000000;;
   0.000000;
   0.000000;0.000000;0.000000;;
   0.000000;0.000000;0.000000;;

   TextureFilename  {
    "Center1.png";
   }
  }

  Material  {
   1.000000;1.000000;1.000000;1.000000;;
   0.000000;
   0.000000;0.000000;0.000000;;
   0.000000;0.000000;0.000000;;

   TextureFilename  {
    "Road1.png";
   }
  }

  Material  {
   0.768627;0.768627;0.768627;1.000000;;
   0.000000;
   0.000000;0.000000;0.000000;;
   0.000000;0.000000;0.000000;;

   TextureFilename  {
    "Concrete1.png";
   }
  }

  Material  {
   1.000000;1.000000;1.000000;1.000000;;
   0.000000;
   0.000000;0.000000;0.000000;;
   0.000000;0.000000;0.000000;;

   TextureFilename  {
    "Saku2.png";
   }
  }
 }

 MeshTextureCoords  {
  18;
  0.000000;1.000000;,
  0.000000;0.280000;,
  0.000000;0.000000;,
  4.000000;1.000000;,
  4.000000;0.280000;,
  4.000000;0.000000;,
  0.100000;0.000000;,
  0.100000;1.000000;,
  0.400000;0.000000;,
  0.400000;1.000000;,
  0.000000;1.000000;,
  0.000000;0.000000;,
  1.000000;1.000000;,
  1.000000;0.000000;,
  0.000000;1.000000;,
  0.000000;0.000000;,
  12.000000;1.000000;,
  12.000000;0.000000;;
 }

 MeshNormals  {
  12;
  1.000000;0.000000;0.000000;,
  1.000000;0.000000;0.000000;,
  0.000000;1.000000;0.000000;,
  0.000000;1.000000;0.000000;,
  0.000000;1.000000;0.000000;,
  0.000000;1.000000;0.000000;,
  -1.000000;0.000000;0.000000;,
  -1.000000;0.000000;0.000000;,
  1.000000;0.000000;0.000000;,
  1.000000;0.000000;0.000000;,
  -1.000000;0.000000;0.000000;,
  -1.000000;0.000000;0.000000;;
  12;
  3;0,0,0;,
  3;1,1,1;,
  3;2,2,2;,
  3;3,3,3;,
  3;4,4,4;,
  3;5,5,5;,
  3;6,6,6;,
  3;7,7,7;,
  3;8,8,8;,
  3;9,9,9;,
  3;10,10,10;,
  3;11,11,11;;
 }
}