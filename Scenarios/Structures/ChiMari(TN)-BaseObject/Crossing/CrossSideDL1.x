xof 0303txt 0032
template Vector {
 <3d82ab5e-62da-11cf-ab39-0020af71e433>
 FLOAT x;
 FLOAT y;
 FLOAT z;
}

template MeshFace {
 <3d82ab5f-62da-11cf-ab39-0020af71e433>
 DWORD nFaceVertexIndices;
 array DWORD faceVertexIndices[nFaceVertexIndices];
}

template Mesh {
 <3d82ab44-62da-11cf-ab39-0020af71e433>
 DWORD nVertices;
 array Vector vertices[nVertices];
 DWORD nFaces;
 array MeshFace faces[nFaces];
 [...]
}

template ColorRGBA {
 <35ff44e0-6c7c-11cf-8f52-0040333594a3>
 FLOAT red;
 FLOAT green;
 FLOAT blue;
 FLOAT alpha;
}

template ColorRGB {
 <d3e16e81-7835-11cf-8f52-0040333594a3>
 FLOAT red;
 FLOAT green;
 FLOAT blue;
}

template Material {
 <3d82ab4d-62da-11cf-ab39-0020af71e433>
 ColorRGBA faceColor;
 FLOAT power;
 ColorRGB specularColor;
 ColorRGB emissiveColor;
 [...]
}

template MeshMaterialList {
 <f6f23f42-7686-11cf-8f52-0040333594a3>
 DWORD nMaterials;
 DWORD nFaceIndexes;
 array DWORD faceIndexes[nFaceIndexes];
 [Material <3d82ab4d-62da-11cf-ab39-0020af71e433>]
}

template TextureFilename {
 <a42790e1-7810-11cf-8f52-0040333594a3>
 STRING filename;
}

template Coords2d {
 <f6f23f44-7686-11cf-8f52-0040333594a3>
 FLOAT u;
 FLOAT v;
}

template MeshTextureCoords {
 <f6f23f40-7686-11cf-8f52-0040333594a3>
 DWORD nTextureCoords;
 array Coords2d textureCoords[nTextureCoords];
}

template MeshNormals {
 <f6f23f43-7686-11cf-8f52-0040333594a3>
 DWORD nNormals;
 array Vector normals[nNormals];
 DWORD nFaceNormals;
 array MeshFace faceNormals[nFaceNormals];
}


Mesh  {
 15;
 -120.000000;-0.500000;-3.000000;,
 -120.000000;-0.500000;3.000000;,
 -10.000000;-0.500000;-3.000000;,
 -10.000000;-0.500000;3.000000;,
 -3.000000;-0.020000;-3.000000;,
 -3.000000;-0.020000;3.000000;,
 -120.000000;-0.500000;-3.000000;,
 -120.000000;-0.600000;-3.500000;,
 -120.000000;-15.000000;-17.900000;,
 -10.000000;-0.500000;-3.000000;,
 -10.000000;-0.600000;-3.500000;,
 -10.000000;-15.000000;-17.900000;,
 -3.000000;-0.020000;-3.000000;,
 -3.000000;-0.600000;-3.500000;,
 -3.000000;-15.000000;-17.900000;;
 12;
 3;0,1,3;,
 3;0,3,2;,
 3;2,3,5;,
 3;2,5,4;,
 3;7,6,9;,
 3;7,9,10;,
 3;8,7,10;,
 3;8,10,11;,
 3;10,9,12;,
 3;10,12,13;,
 3;11,10,13;,
 3;11,13,14;;

 MeshMaterialList  {
  2;
  12;
  0,
  0,
  0,
  0,
  1,
  1,
  1,
  1,
  1,
  1,
  1,
  1;

  Material  {
   1.000000;1.000000;1.000000;1.000000;;
   0.000000;
   0.000000;0.000000;0.000000;;
   0.000000;0.000000;0.000000;;

   TextureFilename  {
    "Road.png";
   }
  }

  Material  {
   1.000000;1.000000;1.000000;1.000000;;
   0.000000;
   0.000000;0.000000;0.000000;;
   0.000000;0.000000;0.000000;;

   TextureFilename  {
    "Grass2.png";
   }
  }
 }

 MeshTextureCoords  {
  15;
  1.000000;2.000000;,
  0.000000;2.000000;,
  1.000000;1.166667;,
  0.000000;1.166667;,
  1.000000;0.000000;,
  0.000000;0.000000;,
  0.032043;0.000000;,
  0.038718;0.000000;,
  1.000000;0.000000;,
  0.032043;2.820513;,
  0.038718;2.820513;,
  1.000000;2.820513;,
  0.000000;3.000000;,
  0.038718;3.000000;,
  1.000000;3.000000;;
 }

 MeshNormals  {
  12;
  0.000000;1.000000;0.000000;,
  0.000000;1.000000;0.000000;,
  -0.068411;0.997657;0.000000;,
  -0.068411;0.997657;0.000000;,
  0.000000;0.980581;-0.196116;,
  0.000000;0.980581;-0.196116;,
  0.000000;0.707107;-0.707107;,
  0.000000;0.707107;-0.707107;,
  -0.067088;0.978371;-0.195674;,
  0.000000;0.652939;-0.757410;,
  0.000000;0.707107;-0.707107;,
  0.000000;0.707107;-0.707107;;
  12;
  3;0,0,0;,
  3;1,1,1;,
  3;2,2,2;,
  3;3,3,3;,
  3;4,4,4;,
  3;5,5,5;,
  3;6,6,6;,
  3;7,7,7;,
  3;8,8,8;,
  3;9,9,9;,
  3;10,10,10;,
  3;11,11,11;;
 }
}