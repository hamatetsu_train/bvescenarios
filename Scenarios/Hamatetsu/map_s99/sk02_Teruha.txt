﻿BveTs Map 2.00

//////////////////////////////////////////////////////////////////////////
// map_s99/sk02_Teruha
// 清水港線 清水埠頭(SK02) テルハ
//////////////////////////////////////////////////////////////////////////

$OffsetX=21.0;

$simizu-1375;

$OffsetZ=-55;
//$OffsetZ-30;
	Structure['Teruha2'].Put(0, $OffsetX+10, 0, $OffsetZ+4.50, 0, 102, 0, 1, 1.00);
	Structure['Teruha2'].Put(0, $OffsetX+10, 0, $OffsetZ+4.50, 0, 114, 0, 1, 1.00);
	Structure['Teruha2'].Put(0, $OffsetX+10, 0, $OffsetZ+4.50, 0, 126, 0, 1, 1.00);
	Structure['Teruha2'].Put(0, $OffsetX+10, 0, $OffsetZ+4.50, 0, 138, 0, 1, 1.00);
	Structure['Teruha2'].Put(0, $OffsetX+10, 0, $OffsetZ+4.50, 0, 150, 0, 1, 1.00);
	Structure['Teruha2'].Put(0, $OffsetX+10, 0, $OffsetZ+4.50, 0, 162, 0, 1, 1.00);
	Structure['Teruha2'].Put(0, $OffsetX+10, 0, $OffsetZ+4.50, 0, 174, 0, 1, 1.00);
	Structure['Teruha2'].Put(0, $OffsetX+10, 0, $OffsetZ+4.50, 0, 186, 0, 1, 1.00);
	Structure['Teruha2'].Put(0, $OffsetX+10, 0, $OffsetZ+4.50, 0, 198, 0, 1, 1.00);
	Structure['Teruha2'].Put(0, $OffsetX+10, 0, $OffsetZ+4.50, 0, 210, 0, 1, 1.00);
	Structure['Teruha2'].Put(0, $OffsetX+10, 0, $OffsetZ+4.50, 0, 222, 0, 1, 1.00);
	Structure['Teruha2'].Put(0, $OffsetX+10, 0, $OffsetZ+4.50, 0, 234, 0, 1, 1.00);
	Structure['Teruha2'].Put(0, $OffsetX+10, 0, $OffsetZ+4.50, 0, 246, 0, 1, 1.00);
	Structure['Teruha2'].Put(0, $OffsetX+10, 0, $OffsetZ+4.50, 0, 258, 0, 1, 1.00);
	Structure['Teruha2'].Put(0, $OffsetX+10, 0, $OffsetZ+4.50, 0, 270, 0, 1, 1.00);

$OffsetZ=-50;
//$OffsetZ-25;
	Structure['Teruha1'].Put(0, $OffsetX+1, 0, $OffsetZ-5.0, 0, 90.000, 0, 1, 1.00);
	Structure['Teruha1'].Put(0, $OffsetX+2, 0, $OffsetZ-5.0, 0, 90.000, 0, 1, 1.00);
	Structure['Teruha1'].Put(0, $OffsetX+3, 0, $OffsetZ-5.0, 0, 90.000, 0, 1, 1.00);
	Structure['Teruha1'].Put(0, $OffsetX+4, 0, $OffsetZ-5.0, 0, 90.000, 0, 1, 1.00);
	Structure['Teruha1'].Put(0, $OffsetX+5, 0, $OffsetZ-5.0, 0, 90.000, 0, 1, 1.00);
	Structure['Teruha1'].Put(0, $OffsetX+6, 0, $OffsetZ-5.0, 0, 90.000, 0, 1, 1.00);
	Structure['Teruha1'].Put(0, $OffsetX+7, 0, $OffsetZ-5.0, 0, 90.000, 0, 1, 1.00);
	Structure['Teruha1'].Put(0, $OffsetX+8, 0, $OffsetZ-5.0, 0, 90.000, 0, 1, 1.00);
	Structure['Teruha1'].Put(0, $OffsetX+9, 0, $OffsetZ-5.0, 0, 90.000, 0, 1, 1.00);
	Structure['Teruha1'].Put(0, $OffsetX+10, 0, $OffsetZ-5.0, 0, 90.000, 0, 1, 1.00);

	Structure['Teruha1'].Put(0, $OffsetX+1, 0,  $OffsetZ+4.0, 0, 90.000, 0, 1, 1.00);
	Structure['Teruha1'].Put(0, $OffsetX+2, 0,  $OffsetZ+4.0, 0, 90.000, 0, 1, 1.00);
	Structure['Teruha1'].Put(0, $OffsetX+3, 0,  $OffsetZ+4.0, 0, 90.000, 0, 1, 1.00);
	Structure['Teruha1'].Put(0, $OffsetX+4, 0,  $OffsetZ+4.0, 0, 90.000, 0, 1, 1.00);
	Structure['Teruha1'].Put(0, $OffsetX+5, 0,  $OffsetZ+4.0, 0, 90.000, 0, 1, 1.00);
	Structure['Teruha1'].Put(0, $OffsetX+6, 0,  $OffsetZ+4.0, 0, 90.000, 0, 1, 1.00);
	Structure['Teruha1'].Put(0, $OffsetX+7, 0,  $OffsetZ+4.0, 0, 90.000, 0, 1, 1.00);
	Structure['Teruha1'].Put(0, $OffsetX+8, 0,  $OffsetZ+4.0, 0, 90.000, 0, 1, 1.00);
	Structure['Teruha1'].Put(0, $OffsetX+9, 0,  $OffsetZ+4.0, 0, 90.000, 0, 1, 1.00);
	Structure['Teruha1'].Put(0, $OffsetX+10, 0,  $OffsetZ+4.0, 0, 90.000, 0, 1, 1.00);

	Structure['Teruha4'].Put(0, $OffsetX+00, 0, $OffsetZ-0.5, 0, 0.000, 0, 1, 1.00);
	Structure['Teruha5'].Put(0, $OffsetX+10, 0, $OffsetZ-0.5, 0, 0.000, 0, 1, 1.00);
	Structure['Teruha6'].Put(0, $OffsetX+00, 0, $OffsetZ-0.5, 0, 0.000, 0, 1, 1.00);
	Structure['Teruha7'].Put(0, $OffsetX+00, 0, $OffsetZ-0.5, 0, 90.000, 0, 1, 1.00);
	Structure['Teruha7'].Put(0, $OffsetX+05, 0, $OffsetZ-0.5, 0, 90.000, 0, 1, 1.00);
	Structure['Teruha7'].Put(0, $OffsetX+10, 0, $OffsetZ-0.5, 0, 90.000, 0, 1, 1.00);

$OffsetZ=-41;
//$OffsetZ-16;
	Structure['Teruha3'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.47, 0,  -0, 0, 1, 0.94);
	Structure['Teruha3'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.47, 0,  -2, 0, 1, 0.94);
	Structure['Teruha3'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.47, 0,  -4, 0, 1, 0.94);
	Structure['Teruha3'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.47, 0,  -6, 0, 1, 0.94);
	Structure['Teruha3'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.47, 0,  -8, 0, 1, 0.94);
	Structure['Teruha3'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.47, 0, -10, 0, 1, 0.94);
	Structure['Teruha3'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.47, 0, -12, 0, 1, 0.94);
	Structure['Teruha3'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.47, 0, -14, 0, 1, 0.94);
	Structure['Teruha3'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.47, 0, -16, 0, 1, 0.94);
	Structure['Teruha3'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.47, 0, -18, 0, 1, 0.94);
	Structure['Teruha3'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.47, 0, -20, 0, 1, 0.94);
	Structure['Teruha3'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.47, 0, -22, 0, 1, 0.94);
	Structure['Teruha3'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.47, 0, -24, 0, 1, 0.94);
	Structure['Teruha3'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.47, 0, -26, 0, 1, 0.94);
	Structure['Teruha3'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.47, 0, -28, 0, 1, 0.94);
	Structure['Teruha3'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.47, 0, -30, 0, 1, 0.94);
	Structure['Teruha3'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.47, 0, -32, 0, 1, 0.94);
	Structure['Teruha3'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.47, 0, -34, 0, 1, 0.94);
	Structure['Teruha3'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.47, 0, -36, 0, 1, 0.94);
	Structure['Teruha3'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.47, 0, -38, 0, 1, 0.94);
	Structure['Teruha3'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.47, 0, -40, 0, 1, 0.94);
	Structure['Teruha3'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.47, 0, -42, 0, 1, 0.94);
	Structure['Teruha3'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.47, 0, -44, 0, 1, 0.94);
	Structure['Teruha3'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.47, 0, -46, 0, 1, 0.94);
	Structure['Teruha3'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.47, 0, -48, 0, 1, 0.94);
	Structure['Teruha3'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.47, 0, -50, 0, 1, 0.94);
	Structure['Teruha3'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.47, 0, -52, 0, 1, 0.94);
	Structure['Teruha3'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.47, 0, -54, 0, 1, 0.94);
	Structure['Teruha3'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.47, 0, -56, 0, 1, 0.94);
	Structure['Teruha3'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.47, 0, -58, 0, 1, 0.94);
	Structure['Teruha3'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.47, 0, -60, 0, 1, 0.94);
	Structure['Teruha3'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.47, 0, -62, 0, 1, 0.94);
	Structure['Teruha3'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.47, 0, -64, 0, 1, 0.94);
	Structure['Teruha3'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.47, 0, -66, 0, 1, 0.94);
	Structure['Teruha3'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.47, 0, -68, 0, 1, 0.94);
	Structure['Teruha3'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.47, 0, -70, 0, 1, 0.94);
	Structure['Teruha3'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.47, 0, -72, 0, 1, 0.94);
	Structure['Teruha3'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.47, 0, -74, 0, 1, 0.94);
	Structure['Teruha3'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.47, 0, -76, 0, 1, 0.94);
	Structure['Teruha3'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.47, 0, -78, 0, 1, 0.94);
	Structure['Teruha3'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.47, 0, -80, 0, 1, 0.94);
	Structure['Teruha3'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.47, 0, -82, 0, 1, 0.94);
	Structure['Teruha3'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.47, 0, -84, 0, 1, 0.94);
	Structure['Teruha3'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.47, 0, -86, 0, 1, 0.94);
	Structure['Teruha3'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.47, 0, -88, 0, 1, 0.94);
	Structure['Teruha3'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.47, 0, -90, 0, 1, 0.94);

	Structure['Teruha2'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.50, 0,  -0.00, 0, 1, 1.00);
	Structure['Teruha2'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.50, 0, -11.25, 0, 1, 1.00);
	Structure['Teruha2'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.50, 0, -22.50, 0, 1, 1.00);
	Structure['Teruha2'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.50, 0, -33.75, 0, 1, 1.00);
	Structure['Teruha2'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.50, 0, -45.00, 0, 1, 1.00);
	Structure['Teruha2'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.50, 0, -56.25, 0, 1, 1.00);
	Structure['Teruha2'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.50, 0, -67.50, 0, 1, 1.00);
	Structure['Teruha2'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.50, 0, -78.75, 0, 1, 1.00);
	Structure['Teruha2'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.50, 0, -90.00, 0, 1, 1.00);

	Structure['Teruha4'].Put(0, $OffsetX, 0,  $OffsetZ, 0,   0.000, 0, 1, 1.00);
	Structure['Teruha7'].Put(0, $OffsetX-6.36, 0, $OffsetZ-6.36, 0, -45.000, 0, 1, 1.00);
	Structure['Teruha8'].Put(0, $OffsetX, 0,  $OffsetZ, 0,   0.000, 0, 1, 1.00);
	Structure['Teruha9'].Put(0, $OffsetX, 0,  $OffsetZ, 0,   0.000, 0, 1, 1.00);
	Structure['Teruha9'].Put(0, $OffsetX, 0,  $OffsetZ, 0, -90.000, 0, 1, 1.00);

$OffsetZ=-41;
//$OffsetZ-16;
	Structure['Teruha1'].Put(0, $OffsetX-9.0-4.5, 0, $OffsetZ+0.50, 0,  0, 0, 1, 1.00);
	Structure['Teruha1'].Put(0, $OffsetX-9.0+4.5, 0, $OffsetZ+0.50, 0,  0, 0, 1, 1.00);
	Structure['Teruha4'].Put(0, $OffsetX-9.0, 0, $OffsetZ, 0, 0, 0, 3, 7.5);
	Structure['Teruha7'].Put(0, $OffsetX-9.0, 0, $OffsetZ, 0, 0, 0, 3, 7.5);
//	Repeater['Teruha1mL'].Begin(0, $OffsetX-9.0-4.5, 0, $OffsetZ+0.50, 0, 0, 0, 3, 1.0, 1.0, 'Teruha1');
//	Repeater['Teruha1mR'].Begin(0, $OffsetX-9.0+4.5, 0, $OffsetZ+0.50, 0, 0, 0, 3, 1.0, 1.0, 'Teruha1');
//	Repeater['TeruhaP'].Begin(0, $OffsetX-9.0, 0, $OffsetZ+0.50, 0, 0, 0, 3, 7.5, 7.5, 'Teruha4');
//	Repeater['TeruhaY'].Begin(0, $OffsetX-9.0, 0, $OffsetZ+0.50, 0, 0, 0, 3, 7.5, 7.5, 'Teruha7');

$OffsetZ=-40;
	Structure['Teruha1'].Put(0, $OffsetX-9.0-4.5, 0, $OffsetZ+0.50, 0,  0, 0, 1, 1.00);
	Structure['Teruha1'].Put(0, $OffsetX-9.0+4.5, 0, $OffsetZ+0.50, 0,  0, 0, 1, 1.00);

$OffsetZ=-39;
	Structure['Teruha1'].Put(0, $OffsetX-9.0-4.5, 0, $OffsetZ+0.50, 0,  0, 0, 1, 1.00);
	Structure['Teruha1'].Put(0, $OffsetX-9.0+4.5, 0, $OffsetZ+0.50, 0,  0, 0, 1, 1.00);

$OffsetZ=-38;
	Structure['Teruha1'].Put(0, $OffsetX-9.0-4.5, 0, $OffsetZ+0.50, 0,  0, 0, 1, 1.00);
	Structure['Teruha1'].Put(0, $OffsetX-9.0+4.5, 0, $OffsetZ+0.50, 0,  0, 0, 1, 1.00);

$OffsetZ=-37;
	Structure['Teruha1'].Put(0, $OffsetX-9.0-4.5, 0, $OffsetZ+0.50, 0,  0, 0, 1, 1.00);
	Structure['Teruha1'].Put(0, $OffsetX-9.0+4.5, 0, $OffsetZ+0.50, 0,  0, 0, 1, 1.00);

$OffsetZ=-36;
	Structure['Teruha1'].Put(0, $OffsetX-9.0-4.5, 0, $OffsetZ+0.50, 0,  0, 0, 1, 1.00);
	Structure['Teruha1'].Put(0, $OffsetX-9.0+4.5, 0, $OffsetZ+0.50, 0,  0, 0, 1, 1.00);

$OffsetZ=-35;
	Structure['Teruha1'].Put(0, $OffsetX-9.0-4.5, 0, $OffsetZ+0.50, 0,  0, 0, 1, 1.00);
	Structure['Teruha1'].Put(0, $OffsetX-9.0+4.5, 0, $OffsetZ+0.50, 0,  0, 0, 1, 1.00);

$OffsetZ=-34;
	Structure['Teruha1'].Put(0, $OffsetX-9.0-4.5, 0, $OffsetZ+0.50, 0,  0, 0, 1, 1.00);
	Structure['Teruha1'].Put(0, $OffsetX-9.0+4.5, 0, $OffsetZ+0.50, 0,  0, 0, 1, 1.00);

$OffsetZ=-33.5;
	Structure['Teruha4'].Put(0, $OffsetX-9.0, 0, $OffsetZ, 0, 0, 0, 3, 7.5);
	Structure['Teruha7'].Put(0, $OffsetX-9.0, 0, $OffsetZ, 0, 0, 0, 3, 7.5);

$OffsetZ=-33;
	Structure['Teruha1'].Put(0, $OffsetX-9.0-4.5, 0, $OffsetZ+0.50, 0,  0, 0, 1, 1.00);
	Structure['Teruha1'].Put(0, $OffsetX-9.0+4.5, 0, $OffsetZ+0.50, 0,  0, 0, 1, 1.00);

$OffsetZ=-32;
	Structure['Teruha1'].Put(0, $OffsetX-9.0-4.5, 0, $OffsetZ+0.50, 0,  0, 0, 1, 1.00);
	Structure['Teruha1'].Put(0, $OffsetX-9.0+4.5, 0, $OffsetZ+0.50, 0,  0, 0, 1, 1.00);

$OffsetZ=-31;
	Structure['Teruha1'].Put(0, $OffsetX-9.0-4.5, 0, $OffsetZ+0.50, 0,  0, 0, 1, 1.00);
	Structure['Teruha1'].Put(0, $OffsetX-9.0+4.5, 0, $OffsetZ+0.50, 0,  0, 0, 1, 1.00);

$OffsetZ=-30;
	Structure['Teruha1'].Put(0, $OffsetX-9.0-4.5, 0, $OffsetZ+0.50, 0,  0, 0, 1, 1.00);
	Structure['Teruha1'].Put(0, $OffsetX-9.0+4.5, 0, $OffsetZ+0.50, 0,  0, 0, 1, 1.00);

$OffsetZ=-29;
	Structure['Teruha1'].Put(0, $OffsetX-9.0-4.5, 0, $OffsetZ+0.50, 0,  0, 0, 1, 1.00);
	Structure['Teruha1'].Put(0, $OffsetX-9.0+4.5, 0, $OffsetZ+0.50, 0,  0, 0, 1, 1.00);

$OffsetZ=-28;
	Structure['Teruha1'].Put(0, $OffsetX-9.0-4.5, 0, $OffsetZ+0.50, 0,  0, 0, 1, 1.00);
	Structure['Teruha1'].Put(0, $OffsetX-9.0+4.5, 0, $OffsetZ+0.50, 0,  0, 0, 1, 1.00);

$OffsetZ=-27;
	Structure['Teruha1'].Put(0, $OffsetX-9.0-4.5, 0, $OffsetZ+0.50, 0,  0, 0, 1, 1.00);
	Structure['Teruha1'].Put(0, $OffsetX-9.0+4.5, 0, $OffsetZ+0.50, 0,  0, 0, 1, 1.00);

$OffsetZ=-26;
	Structure['Teruha1'].Put(0, $OffsetX-9.0-4.5, 0, $OffsetZ+0.50, 0,  0, 0, 1, 1.00);
	Structure['Teruha1'].Put(0, $OffsetX-9.0+4.5, 0, $OffsetZ+0.50, 0,  0, 0, 1, 1.00);
	Structure['Teruha4'].Put(0, $OffsetX-9.0, 0, $OffsetZ, 0, 0, 0, 3, 7.5);
	Structure['Teruha7'].Put(0, $OffsetX-9.0, 0, $OffsetZ, 0, 0, 0, 3, 7.5);

$OffsetZ=-25;
	Structure['Teruha1'].Put(0, $OffsetX-9.0-4.5, 0, $OffsetZ+0.50, 0,  0, 0, 1, 1.00);
	Structure['Teruha1'].Put(0, $OffsetX-9.0+4.5, 0, $OffsetZ+0.50, 0,  0, 0, 1, 1.00);

$OffsetZ=-24;
	Structure['Teruha1'].Put(0, $OffsetX-9.0-4.5, 0, $OffsetZ+0.50, 0,  0, 0, 1, 1.00);
	Structure['Teruha1'].Put(0, $OffsetX-9.0+4.5, 0, $OffsetZ+0.50, 0,  0, 0, 1, 1.00);

$OffsetZ=-23;
	Structure['Teruha1'].Put(0, $OffsetX-9.0-4.5, 0, $OffsetZ+0.50, 0,  0, 0, 1, 1.00);
	Structure['Teruha1'].Put(0, $OffsetX-9.0+4.5, 0, $OffsetZ+0.50, 0,  0, 0, 1, 1.00);

$OffsetZ=-22;
	Structure['Teruha1'].Put(0, $OffsetX-9.0-4.5, 0, $OffsetZ+0.50, 0,  0, 0, 1, 1.00);
	Structure['Teruha1'].Put(0, $OffsetX-9.0+4.5, 0, $OffsetZ+0.50, 0,  0, 0, 1, 1.00);

$OffsetZ=-21;
	Structure['Teruha1'].Put(0, $OffsetX-9.0-4.5, 0, $OffsetZ+0.50, 0,  0, 0, 1, 1.00);
	Structure['Teruha1'].Put(0, $OffsetX-9.0+4.5, 0, $OffsetZ+0.50, 0,  0, 0, 1, 1.00);

$OffsetZ=-20;
	Structure['Teruha1'].Put(0, $OffsetX-9.0-4.5, 0, $OffsetZ+0.50, 0,  0, 0, 1, 1.00);
	Structure['Teruha1'].Put(0, $OffsetX-9.0+4.5, 0, $OffsetZ+0.50, 0,  0, 0, 1, 1.00);

$OffsetZ=-19;
	Structure['Teruha1'].Put(0, $OffsetX-9.0-4.5, 0, $OffsetZ+0.50, 0,  0, 0, 1, 1.00);
	Structure['Teruha1'].Put(0, $OffsetX-9.0+4.5, 0, $OffsetZ+0.50, 0,  0, 0, 1, 1.00);

$OffsetZ=-18.5;
	Structure['Teruha4'].Put(0, $OffsetX-9.0, 0, $OffsetZ, 0, 0, 0, 3, 7.5);
	Structure['Teruha7'].Put(0, $OffsetX-9.0, 0, $OffsetZ, 0, 0, 0, 3, 7.5);

$OffsetZ=-18;
	Structure['Teruha1'].Put(0, $OffsetX-9.0-4.5, 0, $OffsetZ+0.50, 0,  0, 0, 1, 1.00);
	Structure['Teruha1'].Put(0, $OffsetX-9.0+4.5, 0, $OffsetZ+0.50, 0,  0, 0, 1, 1.00);

$OffsetZ=-17;
	Structure['Teruha1'].Put(0, $OffsetX-9.0-4.5, 0, $OffsetZ+0.50, 0,  0, 0, 1, 1.00);
	Structure['Teruha1'].Put(0, $OffsetX-9.0+4.5, 0, $OffsetZ+0.50, 0,  0, 0, 1, 1.00);

$OffsetZ=-16;
	Structure['Teruha1'].Put(0, $OffsetX-9.0-4.5, 0, $OffsetZ+0.50, 0,  0, 0, 1, 1.00);
	Structure['Teruha1'].Put(0, $OffsetX-9.0+4.5, 0, $OffsetZ+0.50, 0,  0, 0, 1, 1.00);

$OffsetZ=-15;
	Structure['Teruha1'].Put(0, $OffsetX-9.0-4.5, 0, $OffsetZ+0.50, 0,  0, 0, 1, 1.00);
	Structure['Teruha1'].Put(0, $OffsetX-9.0+4.5, 0, $OffsetZ+0.50, 0,  0, 0, 1, 1.00);

$OffsetZ=-14;
	Structure['Teruha1'].Put(0, $OffsetX-9.0-4.5, 0, $OffsetZ+0.50, 0,  0, 0, 1, 1.00);
	Structure['Teruha1'].Put(0, $OffsetX-9.0+4.5, 0, $OffsetZ+0.50, 0,  0, 0, 1, 1.00);

$OffsetZ=-13;
	Structure['Teruha1'].Put(0, $OffsetX-9.0-4.5, 0, $OffsetZ+0.50, 0,  0, 0, 1, 1.00);
	Structure['Teruha1'].Put(0, $OffsetX-9.0+4.5, 0, $OffsetZ+0.50, 0,  0, 0, 1, 1.00);

$OffsetZ=-12;
	Structure['Teruha1'].Put(0, $OffsetX-9.0-4.5, 0, $OffsetZ+0.50, 0,  0, 0, 1, 1.00);
	Structure['Teruha1'].Put(0, $OffsetX-9.0+4.5, 0, $OffsetZ+0.50, 0,  0, 0, 1, 1.00);

$OffsetZ=-11;
	Structure['Teruha1'].Put(0, $OffsetX-9.0-4.5, 0, $OffsetZ+0.50, 0,  0, 0, 1, 1.00);
	Structure['Teruha1'].Put(0, $OffsetX-9.0+4.5, 0, $OffsetZ+0.50, 0,  0, 0, 1, 1.00);
	Structure['Teruha4'].Put(0, $OffsetX-9.0, 0, $OffsetZ, 0, 0, 0, 3, 7.5);
	Structure['Teruha7'].Put(0, $OffsetX-9.0, 0, $OffsetZ, 0, 0, 0, 3, 7.5);

$OffsetZ=-10;
//$OffsetZ+15;
	Structure['Teruha1'].Put(0, $OffsetX-9.0-4.5, 0, $OffsetZ+0.50, 0,  0, 0, 1, 1.00);
	Structure['Teruha1'].Put(0, $OffsetX-9.0+4.5, 0, $OffsetZ+0.50, 0,  0, 0, 1, 1.00);
//	Repeater['Teruha1mL'].End();
//	Repeater['Teruha1mR'].End();
//	Repeater['TeruhaP'].End();
//	Repeater['TeruhaY'].End();

$OffsetZ=-9;
//$OffsetZ+16;
	Structure['Teruha3'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.47, 0,  2, 0, 1, 0.94);
	Structure['Teruha3'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.47, 0,  4, 0, 1, 0.94);
	Structure['Teruha3'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.47, 0,  6, 0, 1, 0.94);
	Structure['Teruha3'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.47, 0,  8, 0, 1, 0.94);
	Structure['Teruha3'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.47, 0, 10, 0, 1, 0.94);
	Structure['Teruha3'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.47, 0, 12, 0, 1, 0.94);
	Structure['Teruha3'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.47, 0, 14, 0, 1, 0.94);
	Structure['Teruha3'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.47, 0, 16, 0, 1, 0.94);
	Structure['Teruha3'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.47, 0, 18, 0, 1, 0.94);
	Structure['Teruha3'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.47, 0, 20, 0, 1, 0.94);
	Structure['Teruha3'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.47, 0, 22, 0, 1, 0.94);
	Structure['Teruha3'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.47, 0, 24, 0, 1, 0.94);
	Structure['Teruha3'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.47, 0, 26, 0, 1, 0.94);
	Structure['Teruha3'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.47, 0, 28, 0, 1, 0.94);
	Structure['Teruha3'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.47, 0, 30, 0, 1, 0.94);
	Structure['Teruha3'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.47, 0, 32, 0, 1, 0.94);
	Structure['Teruha3'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.47, 0, 34, 0, 1, 0.94);
	Structure['Teruha3'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.47, 0, 36, 0, 1, 0.94);
	Structure['Teruha3'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.47, 0, 38, 0, 1, 0.94);
	Structure['Teruha3'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.47, 0, 40, 0, 1, 0.94);
	Structure['Teruha3'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.47, 0, 42, 0, 1, 0.94);
	Structure['Teruha3'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.47, 0, 44, 0, 1, 0.94);
	Structure['Teruha3'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.47, 0, 46, 0, 1, 0.94);
	Structure['Teruha3'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.47, 0, 48, 0, 1, 0.94);
	Structure['Teruha3'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.47, 0, 50, 0, 1, 0.94);
	Structure['Teruha3'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.47, 0, 52, 0, 1, 0.94);
	Structure['Teruha3'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.47, 0, 54, 0, 1, 0.94);
	Structure['Teruha3'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.47, 0, 56, 0, 1, 0.94);
	Structure['Teruha3'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.47, 0, 58, 0, 1, 0.94);
	Structure['Teruha3'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.47, 0, 60, 0, 1, 0.94);
	Structure['Teruha3'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.47, 0, 62, 0, 1, 0.94);
	Structure['Teruha3'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.47, 0, 64, 0, 1, 0.94);
	Structure['Teruha3'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.47, 0, 66, 0, 1, 0.94);
	Structure['Teruha3'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.47, 0, 68, 0, 1, 0.94);
	Structure['Teruha3'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.47, 0, 70, 0, 1, 0.94);
	Structure['Teruha3'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.47, 0, 72, 0, 1, 0.94);
	Structure['Teruha3'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.47, 0, 74, 0, 1, 0.94);
	Structure['Teruha3'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.47, 0, 76, 0, 1, 0.94);
	Structure['Teruha3'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.47, 0, 78, 0, 1, 0.94);
	Structure['Teruha3'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.47, 0, 80, 0, 1, 0.94);
	Structure['Teruha3'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.47, 0, 82, 0, 1, 0.94);
	Structure['Teruha3'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.47, 0, 84, 0, 1, 0.94);
	Structure['Teruha3'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.47, 0, 86, 0, 1, 0.94);
	Structure['Teruha3'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.47, 0, 88, 0, 1, 0.94);
	Structure['Teruha3'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.47, 0, 90, 0, 1, 0.94);

	Structure['Teruha2'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.50, 0, 11.25, 0, 1, 1.00);
	Structure['Teruha2'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.50, 0, 22.50, 0, 1, 1.00);
	Structure['Teruha2'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.50, 0, 33.75, 0, 1, 1.00);
	Structure['Teruha2'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.50, 0, 45.00, 0, 1, 1.00);
	Structure['Teruha2'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.50, 0, 56.25, 0, 1, 1.00);
	Structure['Teruha2'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.50, 0, 67.50, 0, 1, 1.00);
	Structure['Teruha2'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.50, 0, 78.75, 0, 1, 1.00);
	Structure['Teruha2'].Put(0, $OffsetX-0.0, 0, $OffsetZ-0.50, 0, 90.00, 0, 1, 1.00);

	Structure['Teruha4'].Put(0, $OffsetX, 0,  $OffsetZ, 0, 90.000, 0, 1, 1.00);
	Structure['Teruha7'].Put(0, $OffsetX-6.36, 0, $OffsetZ+6.36, 0, 45.000, 0, 1, 1.00);
	Structure['Teruha8'].Put(0, $OffsetX, 0,  $OffsetZ, 0, 90.000, 0, 1, 1.00);
	Structure['Teruha9'].Put(0, $OffsetX, 0,  $OffsetZ, 0,  0.000, 0, 1, 1.00);
	Structure['Teruha9'].Put(0, $OffsetX, 0,  $OffsetZ, 0, 90.000, 0, 1, 1.00);

$OffsetZ=-5;
//$OffsetZ+20;
	Structure['Teruha2'].Put(0, $OffsetX+10, 0, $OffsetZ+4.50, 0, 102, 0, 1, 1.00);
	Structure['Teruha2'].Put(0, $OffsetX+10, 0, $OffsetZ+4.50, 0, 114, 0, 1, 1.00);
	Structure['Teruha2'].Put(0, $OffsetX+10, 0, $OffsetZ+4.50, 0, 126, 0, 1, 1.00);
	Structure['Teruha2'].Put(0, $OffsetX+10, 0, $OffsetZ+4.50, 0, 138, 0, 1, 1.00);
	Structure['Teruha2'].Put(0, $OffsetX+10, 0, $OffsetZ+4.50, 0, 150, 0, 1, 1.00);
	Structure['Teruha2'].Put(0, $OffsetX+10, 0, $OffsetZ+4.50, 0, 162, 0, 1, 1.00);
	Structure['Teruha2'].Put(0, $OffsetX+10, 0, $OffsetZ+4.50, 0, 174, 0, 1, 1.00);
	Structure['Teruha2'].Put(0, $OffsetX+10, 0, $OffsetZ+4.50, 0, 186, 0, 1, 1.00);
	Structure['Teruha2'].Put(0, $OffsetX+10, 0, $OffsetZ+4.50, 0, 198, 0, 1, 1.00);
	Structure['Teruha2'].Put(0, $OffsetX+10, 0, $OffsetZ+4.50, 0, 210, 0, 1, 1.00);
	Structure['Teruha2'].Put(0, $OffsetX+10, 0, $OffsetZ+4.50, 0, 222, 0, 1, 1.00);
	Structure['Teruha2'].Put(0, $OffsetX+10, 0, $OffsetZ+4.50, 0, 234, 0, 1, 1.00);
	Structure['Teruha2'].Put(0, $OffsetX+10, 0, $OffsetZ+4.50, 0, 246, 0, 1, 1.00);
	Structure['Teruha2'].Put(0, $OffsetX+10, 0, $OffsetZ+4.50, 0, 258, 0, 1, 1.00);
	Structure['Teruha2'].Put(0, $OffsetX+10, 0, $OffsetZ+4.50, 0, 270, 0, 1, 1.00);

$OffsetZ=0;
	Structure['Teruha1'].Put(0, $OffsetX+1, 0, -5.0, 0, 90.000, 0, 1, 1.00);
	Structure['Teruha1'].Put(0, $OffsetX+2, 0, -5.0, 0, 90.000, 0, 1, 1.00);
	Structure['Teruha1'].Put(0, $OffsetX+3, 0, -5.0, 0, 90.000, 0, 1, 1.00);
	Structure['Teruha1'].Put(0, $OffsetX+4, 0, -5.0, 0, 90.000, 0, 1, 1.00);
	Structure['Teruha1'].Put(0, $OffsetX+5, 0, -5.0, 0, 90.000, 0, 1, 1.00);
	Structure['Teruha1'].Put(0, $OffsetX+6, 0, -5.0, 0, 90.000, 0, 1, 1.00);
	Structure['Teruha1'].Put(0, $OffsetX+7, 0, -5.0, 0, 90.000, 0, 1, 1.00);
	Structure['Teruha1'].Put(0, $OffsetX+8, 0, -5.0, 0, 90.000, 0, 1, 1.00);
	Structure['Teruha1'].Put(0, $OffsetX+9, 0, -5.0, 0, 90.000, 0, 1, 1.00);
	Structure['Teruha1'].Put(0, $OffsetX+10, 0, -5.0, 0, 90.000, 0, 1, 1.00);

	Structure['Teruha1'].Put(0, $OffsetX+1, 0,  4.0, 0, 90.000, 0, 1, 1.00);
	Structure['Teruha1'].Put(0, $OffsetX+2, 0,  4.0, 0, 90.000, 0, 1, 1.00);
	Structure['Teruha1'].Put(0, $OffsetX+3, 0,  4.0, 0, 90.000, 0, 1, 1.00);
	Structure['Teruha1'].Put(0, $OffsetX+4, 0,  4.0, 0, 90.000, 0, 1, 1.00);
	Structure['Teruha1'].Put(0, $OffsetX+5, 0,  4.0, 0, 90.000, 0, 1, 1.00);
	Structure['Teruha1'].Put(0, $OffsetX+6, 0,  4.0, 0, 90.000, 0, 1, 1.00);
	Structure['Teruha1'].Put(0, $OffsetX+7, 0,  4.0, 0, 90.000, 0, 1, 1.00);
	Structure['Teruha1'].Put(0, $OffsetX+8, 0,  4.0, 0, 90.000, 0, 1, 1.00);
	Structure['Teruha1'].Put(0, $OffsetX+9, 0,  4.0, 0, 90.000, 0, 1, 1.00);
	Structure['Teruha1'].Put(0, $OffsetX+10, 0,  4.0, 0, 90.000, 0, 1, 1.00);

	Structure['Teruha4'].Put(0, $OffsetX+00, 0, -0.5, 0, 0.000, 0, 1, 1.00);
	Structure['Teruha5'].Put(0, $OffsetX+10, 0, -0.5, 0, 0.000, 0, 1, 1.00);
	Structure['Teruha6'].Put(0, $OffsetX+00, 0, -0.5, 0, 0.000, 0, 1, 1.00);
	Structure['Teruha7'].Put(0, $OffsetX+00, 0, -0.5, 0, 90.000, 0, 1, 1.00);
	Structure['Teruha7'].Put(0, $OffsetX+05, 0, -0.5, 0, 90.000, 0, 1, 1.00);
	Structure['Teruha7'].Put(0, $OffsetX+10, 0, -0.5, 0, 90.000, 0, 1, 1.00);


//--------------------------------------------------------------------------



// (C)Harupi
