﻿BveTs Map 2.00

//////////////////////////////////////////////////////////////////////////
// S03_K01
// 身延線 普通 富士(CC00)1 ～ 身延(CC16)
//////////////////////////////////////////////////////////////////////////

	//Sound.Load('map_s03\Sub_S03_sound.txt');
	Sound.Load('map_s03\Sub_S03K01_Sound.txt');
	Sound3D.Load('map_s03\Sub_S03_sounds3d.txt');
// 路線基本ファイル
	include 'map_s03\map_s03.txt';

// ATS-S

//////////////////////////////////////////////////////////////////////////
// 閉塞初期値代入
$SecA = 3996;
$SecB = 3996;
//////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////
$Fuji=2000;//-400
	include 'map_s03\cc00_K01.txt';
	include 'map_s03\cc00_cc02.txt';
	include 'map_s03\cc02_cc03.txt';

$Iriyamase= $Fuji+5650;//5250
	include 'map_s03\cc03_cc06.txt';

$Fujinomiya= $Iriyamase+5120;//10370
	include 'map_s03\cc06_K01.txt';
//	include 'map_s03\cc06_cc07_S.txt';
	include 'map_s03\cc06_cc07.txt';

$NishiFujinomiya = $Fujinomiya+1210;//11580
	include 'map_s03\cc07_K01.txt';
	include 'map_s03\cc07_cc09.txt';

$Shibakawa= $NishiFujinomiya+7300;//18880;
	include 'map_s03\cc09_K01.txt';
	include 'map_s03\cc09_cc11.txt';

$Toshima= $Shibakawa+7020;//25900
	include 'map_s03\cc11_K01.txt';
	include 'map_s03\cc11_cc14.txt';

$Utsubuna= $Toshima+7850;//33750
	include 'map_s03\cc14_K01.txt';
	include 'map_s03\cc14_cc15.txt';

$Oshima= $Utsubuna+5640;//39390
	include 'map_s03\cc15_K01.txt';
	include 'map_s03\cc15_cc16.txt';

$Minobu= $Oshima+3770;//43160
	include 'map_s03\cc16_K01.txt';
//	include 'map_s03\cc16_cc18.txt';

//////////////////////////////////////////////////////////////////////////
// 沿線
	include 'map_s03\Sub_S03_Nature01.txt';
// 駅ファイル
	Station.Load('map_s03\Sub_S03K00_stations.txt');
// 停止目標
	include 'map_s03\Sub_S03K01_stops.txt';
// 停車駅接近警報
// -- ATS-PTプラグイン用
	include 'map_s03\Sub_S03K01_cast.txt';
// 閉塞
// -- ATS-PTプラグイン用
	include 'map_s03\Sub_S03K01_Beacons.txt';
// -- snp/swp2用
	//include 'map_s03\Sub_S03K03_sections.txt';
// 曲線
// -- ATS-PTプラグイン用
	include 'map_s03\Sub_S03K01_curve.txt';
// 他列車
	include 'map_s03\Sub_S03K01_trains.txt';
// 音声
	include 'map_s03\Plus_S03K01_Sound.txt';


// (C)Harupi
