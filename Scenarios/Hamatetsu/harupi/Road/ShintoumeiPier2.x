xof 0303txt 0032
template Vector {
 <3d82ab5e-62da-11cf-ab39-0020af71e433>
 FLOAT x;
 FLOAT y;
 FLOAT z;
}

template MeshFace {
 <3d82ab5f-62da-11cf-ab39-0020af71e433>
 DWORD nFaceVertexIndices;
 array DWORD faceVertexIndices[nFaceVertexIndices];
}

template Mesh {
 <3d82ab44-62da-11cf-ab39-0020af71e433>
 DWORD nVertices;
 array Vector vertices[nVertices];
 DWORD nFaces;
 array MeshFace faces[nFaces];
 [...]
}

template ColorRGBA {
 <35ff44e0-6c7c-11cf-8f52-0040333594a3>
 FLOAT red;
 FLOAT green;
 FLOAT blue;
 FLOAT alpha;
}

template ColorRGB {
 <d3e16e81-7835-11cf-8f52-0040333594a3>
 FLOAT red;
 FLOAT green;
 FLOAT blue;
}

template Material {
 <3d82ab4d-62da-11cf-ab39-0020af71e433>
 ColorRGBA faceColor;
 FLOAT power;
 ColorRGB specularColor;
 ColorRGB emissiveColor;
 [...]
}

template MeshMaterialList {
 <f6f23f42-7686-11cf-8f52-0040333594a3>
 DWORD nMaterials;
 DWORD nFaceIndexes;
 array DWORD faceIndexes[nFaceIndexes];
 [Material <3d82ab4d-62da-11cf-ab39-0020af71e433>]
}

template TextureFilename {
 <a42790e1-7810-11cf-8f52-0040333594a3>
 STRING filename;
}

template Coords2d {
 <f6f23f44-7686-11cf-8f52-0040333594a3>
 FLOAT u;
 FLOAT v;
}

template MeshTextureCoords {
 <f6f23f40-7686-11cf-8f52-0040333594a3>
 DWORD nTextureCoords;
 array Coords2d textureCoords[nTextureCoords];
}

template MeshNormals {
 <f6f23f43-7686-11cf-8f52-0040333594a3>
 DWORD nNormals;
 array Vector normals[nNormals];
 DWORD nFaceNormals;
 array MeshFace faceNormals[nFaceNormals];
}


Mesh  {
 16;
 -3.150000;20.000000;-1.000000;,
 -2.350000;20.000000;-1.800000;,
 2.350000;20.000000;-1.800000;,
 3.150000;20.000000;-1.000000;,
 -3.150000;0.000000;-1.000000;,
 -2.350000;0.000000;-1.800000;,
 2.350000;0.000000;-1.800000;,
 3.150000;0.000000;-1.000000;,
 -3.150000;20.000000;1.000000;,
 -2.350000;20.000000;1.800000;,
 2.350000;20.000000;1.800000;,
 3.150000;20.000000;1.000000;,
 -3.150000;0.000000;1.000000;,
 -2.350000;0.000000;1.800000;,
 2.350000;0.000000;1.800000;,
 3.150000;0.000000;1.000000;;
 16;
 3;0,1,5;,
 3;0,5,4;,
 3;1,2,6;,
 3;1,6,5;,
 3;2,3,7;,
 3;2,7,6;,
 3;3,11,15;,
 3;3,15,7;,
 3;11,10,14;,
 3;11,14,15;,
 3;10,9,13;,
 3;10,13,14;,
 3;9,8,12;,
 3;9,12,13;,
 3;8,0,4;,
 3;8,4,12;;

 MeshMaterialList  {
  1;
  16;
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0;

  Material  {
   0.549020;0.568627;0.549020;1.000000;;
   0.000000;
   0.000000;0.000000;0.000000;;
   0.000000;0.000000;0.000000;;

   TextureFilename  {
    "conL_1.dds";
   }
  }
 }

 MeshTextureCoords  {
  16;
  0.000000;0.000000;,
  1.000000;0.000000;,
  5.000000;0.000000;,
  6.000000;0.000000;,
  0.000000;10.000000;,
  1.000000;10.000000;,
  5.000000;10.000000;,
  6.000000;10.000000;,
  7.000000;0.000000;,
  6.000000;0.000000;,
  2.000000;0.000000;,
  1.000000;0.000000;,
  7.000000;10.000000;,
  6.000000;10.000000;,
  2.000000;10.000000;,
  1.000000;10.000000;;
 }

 MeshNormals  {
  16;
  -0.707107;0.000000;-0.707107;,
  -0.707107;0.000000;-0.707107;,
  0.000000;0.000000;-1.000000;,
  0.000000;0.000000;-1.000000;,
  0.707107;0.000000;-0.707107;,
  0.707107;0.000000;-0.707107;,
  1.000000;0.000000;0.000000;,
  1.000000;0.000000;0.000000;,
  0.707107;0.000000;0.707107;,
  0.707107;0.000000;0.707107;,
  0.000000;0.000000;1.000000;,
  0.000000;0.000000;1.000000;,
  -0.707107;0.000000;0.707107;,
  -0.707107;0.000000;0.707107;,
  -1.000000;0.000000;0.000000;,
  -1.000000;0.000000;0.000000;;
  16;
  3;0,0,0;,
  3;1,1,1;,
  3;2,2,2;,
  3;3,3,3;,
  3;4,4,4;,
  3;5,5,5;,
  3;6,6,6;,
  3;7,7,7;,
  3;8,8,8;,
  3;9,9,9;,
  3;10,10,10;,
  3;11,11,11;,
  3;12,12,12;,
  3;13,13,13;,
  3;14,14,14;,
  3;15,15,15;;
 }
}