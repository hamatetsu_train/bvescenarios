xof 0303txt 0032
template Vector {
 <3d82ab5e-62da-11cf-ab39-0020af71e433>
 FLOAT x;
 FLOAT y;
 FLOAT z;
}

template MeshFace {
 <3d82ab5f-62da-11cf-ab39-0020af71e433>
 DWORD nFaceVertexIndices;
 array DWORD faceVertexIndices[nFaceVertexIndices];
}

template Mesh {
 <3d82ab44-62da-11cf-ab39-0020af71e433>
 DWORD nVertices;
 array Vector vertices[nVertices];
 DWORD nFaces;
 array MeshFace faces[nFaces];
 [...]
}

template ColorRGBA {
 <35ff44e0-6c7c-11cf-8f52-0040333594a3>
 FLOAT red;
 FLOAT green;
 FLOAT blue;
 FLOAT alpha;
}

template ColorRGB {
 <d3e16e81-7835-11cf-8f52-0040333594a3>
 FLOAT red;
 FLOAT green;
 FLOAT blue;
}

template Material {
 <3d82ab4d-62da-11cf-ab39-0020af71e433>
 ColorRGBA faceColor;
 FLOAT power;
 ColorRGB specularColor;
 ColorRGB emissiveColor;
 [...]
}

template MeshMaterialList {
 <f6f23f42-7686-11cf-8f52-0040333594a3>
 DWORD nMaterials;
 DWORD nFaceIndexes;
 array DWORD faceIndexes[nFaceIndexes];
 [Material <3d82ab4d-62da-11cf-ab39-0020af71e433>]
}

template TextureFilename {
 <a42790e1-7810-11cf-8f52-0040333594a3>
 STRING filename;
}

template Coords2d {
 <f6f23f44-7686-11cf-8f52-0040333594a3>
 FLOAT u;
 FLOAT v;
}

template MeshTextureCoords {
 <f6f23f40-7686-11cf-8f52-0040333594a3>
 DWORD nTextureCoords;
 array Coords2d textureCoords[nTextureCoords];
}

template MeshNormals {
 <f6f23f43-7686-11cf-8f52-0040333594a3>
 DWORD nNormals;
 array Vector normals[nNormals];
 DWORD nFaceNormals;
 array MeshFace faceNormals[nFaceNormals];
}


Mesh  {
 20;
 -1.250000;7.100000;-1.250000;,
 1.250000;7.100000;-1.250000;,
 -1.250000;7.100000;1.250000;,
 1.250000;7.100000;1.250000;,
 -1.250000;9.600000;-1.250000;,
 1.250000;9.600000;-1.250000;,
 -1.250000;9.600000;1.250000;,
 1.250000;9.600000;1.250000;,
 -1.250000;10.850000;0.000000;,
 1.250000;10.850000;0.000000;,
 -1.250000;7.100000;-1.250000;,
 1.250000;7.100000;-1.250000;,
 -1.250000;7.100000;1.250000;,
 1.250000;7.100000;1.250000;,
 -1.250000;9.450000;-1.400000;,
 1.250000;9.450000;-1.400000;,
 -1.250000;10.850000;0.000000;,
 1.250000;10.850000;0.000000;,
 -1.250000;9.450000;1.400000;,
 1.250000;9.450000;1.400000;;
 32;
 3;0,1,5;,
 3;0,5,4;,
 3;4,5,1;,
 3;4,1,0;,
 3;1,3,7;,
 3;1,7,5;,
 3;5,7,3;,
 3;5,3,1;,
 3;3,2,6;,
 3;3,6,7;,
 3;7,6,2;,
 3;7,2,3;,
 3;2,0,4;,
 3;2,4,6;,
 3;6,4,0;,
 3;6,0,2;,
 3;4,6,8;,
 3;8,6,4;,
 3;5,7,9;,
 3;9,7,5;,
 3;10,11,13;,
 3;10,13,12;,
 3;12,13,11;,
 3;12,11,10;,
 3;14,15,17;,
 3;14,17,16;,
 3;16,17,15;,
 3;16,15,14;,
 3;16,17,19;,
 3;16,19,18;,
 3;18,19,17;,
 3;18,17,16;;

 MeshMaterialList  {
  2;
  32;
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  1,
  1,
  1,
  1,
  1,
  1,
  1,
  1,
  1,
  1,
  1,
  1;

  Material  {
   1.000000;1.000000;1.000000;1.000000;;
   0.000000;
   0.000000;0.000000;0.000000;;
   0.000000;0.000000;0.000000;;

   TextureFilename  {
    "numazu_mokuzai.dds";
   }
  }

  Material  {
   0.392157;0.392157;0.392157;1.000000;;
   0.000000;
   0.000000;0.000000;0.000000;;
   0.000000;0.000000;0.000000;;

   TextureFilename  {
    "numazu_mokuzai.dds";
   }
  }
 }

 MeshTextureCoords  {
  20;
  1.000000;3.000000;,
  1.000000;0.000000;,
  1.000000;0.000000;,
  1.000000;3.000000;,
  0.000000;3.000000;,
  0.000000;0.000000;,
  0.000000;0.000000;,
  0.000000;3.000000;,
  0.500000;0.300000;,
  0.500000;0.300000;,
  0.000000;0.000000;,
  1.000000;0.000000;,
  0.000000;3.000000;,
  1.000000;3.000000;,
  0.000000;0.000000;,
  1.000000;0.000000;,
  0.000000;6.000000;,
  1.000000;6.000000;,
  0.000000;12.000000;,
  1.000000;12.000000;;
 }

 MeshNormals  {
  32;
  0.000000;0.000000;1.000000;,
  0.000000;0.000000;1.000000;,
  0.000000;0.000000;-1.000000;,
  0.000000;0.000000;-1.000000;,
  -1.000000;0.000000;0.000000;,
  -1.000000;0.000000;0.000000;,
  1.000000;0.000000;0.000000;,
  1.000000;0.000000;0.000000;,
  0.000000;0.000000;-1.000000;,
  0.000000;0.000000;-1.000000;,
  0.000000;0.000000;1.000000;,
  0.000000;0.000000;1.000000;,
  1.000000;0.000000;0.000000;,
  1.000000;0.000000;0.000000;,
  -1.000000;0.000000;0.000000;,
  -1.000000;0.000000;0.000000;,
  -1.000000;0.000000;0.000000;,
  1.000000;0.000000;0.000000;,
  -1.000000;0.000000;0.000000;,
  1.000000;0.000000;0.000000;,
  0.000000;-1.000000;0.000000;,
  0.000000;-1.000000;0.000000;,
  0.000000;1.000000;0.000000;,
  0.000000;1.000000;0.000000;,
  0.000000;-0.707107;0.707107;,
  0.000000;-0.707107;0.707107;,
  0.000000;0.707107;-0.707107;,
  0.000000;0.707107;-0.707107;,
  0.000000;-0.707107;-0.707107;,
  0.000000;-0.707107;-0.707107;,
  0.000000;0.707107;0.707107;,
  0.000000;0.707107;0.707107;;
  32;
  3;0,0,0;,
  3;1,1,1;,
  3;2,2,2;,
  3;3,3,3;,
  3;4,4,4;,
  3;5,5,5;,
  3;6,6,6;,
  3;7,7,7;,
  3;8,8,8;,
  3;9,9,9;,
  3;10,10,10;,
  3;11,11,11;,
  3;12,12,12;,
  3;13,13,13;,
  3;14,14,14;,
  3;15,15,15;,
  3;16,16,16;,
  3;17,17,17;,
  3;18,18,18;,
  3;19,19,19;,
  3;20,20,20;,
  3;21,21,21;,
  3;22,22,22;,
  3;23,23,23;,
  3;24,24,24;,
  3;25,25,25;,
  3;26,26,26;,
  3;27,27,27;,
  3;28,28,28;,
  3;29,29,29;,
  3;30,30,30;,
  3;31,31,31;;
 }
}