xof 0303txt 0032
template Vector {
 <3d82ab5e-62da-11cf-ab39-0020af71e433>
 FLOAT x;
 FLOAT y;
 FLOAT z;
}

template MeshFace {
 <3d82ab5f-62da-11cf-ab39-0020af71e433>
 DWORD nFaceVertexIndices;
 array DWORD faceVertexIndices[nFaceVertexIndices];
}

template Mesh {
 <3d82ab44-62da-11cf-ab39-0020af71e433>
 DWORD nVertices;
 array Vector vertices[nVertices];
 DWORD nFaces;
 array MeshFace faces[nFaces];
 [...]
}

template ColorRGBA {
 <35ff44e0-6c7c-11cf-8f52-0040333594a3>
 FLOAT red;
 FLOAT green;
 FLOAT blue;
 FLOAT alpha;
}

template ColorRGB {
 <d3e16e81-7835-11cf-8f52-0040333594a3>
 FLOAT red;
 FLOAT green;
 FLOAT blue;
}

template Material {
 <3d82ab4d-62da-11cf-ab39-0020af71e433>
 ColorRGBA faceColor;
 FLOAT power;
 ColorRGB specularColor;
 ColorRGB emissiveColor;
 [...]
}

template MeshMaterialList {
 <f6f23f42-7686-11cf-8f52-0040333594a3>
 DWORD nMaterials;
 DWORD nFaceIndexes;
 array DWORD faceIndexes[nFaceIndexes];
 [Material <3d82ab4d-62da-11cf-ab39-0020af71e433>]
}

template TextureFilename {
 <a42790e1-7810-11cf-8f52-0040333594a3>
 STRING filename;
}

template Coords2d {
 <f6f23f44-7686-11cf-8f52-0040333594a3>
 FLOAT u;
 FLOAT v;
}

template MeshTextureCoords {
 <f6f23f40-7686-11cf-8f52-0040333594a3>
 DWORD nTextureCoords;
 array Coords2d textureCoords[nTextureCoords];
}

template MeshNormals {
 <f6f23f43-7686-11cf-8f52-0040333594a3>
 DWORD nNormals;
 array Vector normals[nNormals];
 DWORD nFaceNormals;
 array MeshFace faceNormals[nFaceNormals];
}


Mesh  {
 22;
 0.000000;4.500000;11.000000;,
 0.000000;0.000000;11.000000;,
 0.000000;4.500000;0.000000;,
 0.000000;0.000000;0.000000;,
 0.000000;2.500000;0.900000;,
 0.000000;0.000000;0.900000;,
 5.000000;2.500000;0.900000;,
 5.000000;0.000000;0.900000;,
 0.000000;2.600000;0.000000;,
 5.000000;2.600000;0.000000;,
 0.000000;3.200000;1.000000;,
 5.000000;3.200000;1.000000;,
 0.000000;3.600000;2.000000;,
 5.000000;3.600000;2.000000;,
 0.000000;4.000000;3.500000;,
 5.000000;4.000000;3.500000;,
 0.000000;4.250000;3.450000;,
 5.000000;4.250000;3.450000;,
 0.000000;4.400000;4.500000;,
 5.000000;4.400000;4.500000;,
 0.000000;4.500000;5.500000;,
 5.000000;4.500000;5.500000;;
 16;
 3;0,2,3;,
 3;0,3,1;,
 3;4,6,7;,
 3;4,7,5;,
 3;8,10,11;,
 3;8,11,9;,
 3;10,12,13;,
 3;10,13,11;,
 3;12,14,15;,
 3;12,15,13;,
 3;14,16,17;,
 3;14,17,15;,
 3;16,18,19;,
 3;16,19,17;,
 3;18,20,21;,
 3;18,21,19;;

 MeshMaterialList  {
  2;
  16;
  0,
  0,
  0,
  0,
  1,
  1,
  1,
  1,
  1,
  1,
  1,
  1,
  1,
  1,
  1,
  1;

  Material  {
   1.000000;1.000000;1.000000;1.000000;;
   0.000000;
   0.000000;0.000000;0.000000;;
   0.000000;0.000000;0.000000;;

   TextureFilename  {
    "Sta_CC20_House1a.dds";
   }
  }

  Material  {
   0.784314;0.784314;0.745098;1.000000;;
   0.000000;
   0.000000;0.000000;0.000000;;
   0.000000;0.000000;0.000000;;
  }
 }

 MeshTextureCoords  {
  22;
  0.000000;0.000000;,
  0.000000;1.000000;,
  1.000000;0.000000;,
  1.000000;1.000000;,
  0.910000;0.500000;,
  0.910000;1.000000;,
  0.850000;0.500000;,
  0.850000;1.000000;,
  0.000000;0.000000;,
  0.000000;0.000000;,
  0.000000;0.000000;,
  0.000000;0.000000;,
  0.000000;0.000000;,
  0.000000;0.000000;,
  0.000000;0.000000;,
  0.000000;0.000000;,
  0.000000;0.000000;,
  0.000000;0.000000;,
  0.000000;0.000000;,
  0.000000;0.000000;,
  0.000000;0.000000;,
  0.000000;0.000000;;
 }

 MeshNormals  {
  16;
  -1.000000;0.000000;0.000000;,
  -1.000000;0.000000;0.000000;,
  0.000000;0.000000;-1.000000;,
  0.000000;0.000000;-1.000000;,
  0.000000;0.857493;-0.514496;,
  0.000000;0.857493;-0.514496;,
  0.000000;0.928477;-0.371391;,
  0.000000;0.928477;-0.371391;,
  0.000000;0.966235;-0.257663;,
  0.000000;0.966235;-0.257663;,
  0.000000;-0.196116;-0.980581;,
  0.000000;-0.196116;-0.980581;,
  0.000000;0.989949;-0.141421;,
  0.000000;0.989949;-0.141421;,
  0.000000;0.995037;-0.099504;,
  0.000000;0.995037;-0.099504;;
  16;
  3;0,0,0;,
  3;1,1,1;,
  3;2,2,2;,
  3;3,3,3;,
  3;4,4,4;,
  3;5,5,5;,
  3;6,6,6;,
  3;7,7,7;,
  3;8,8,8;,
  3;9,9,9;,
  3;10,10,10;,
  3;11,11,11;,
  3;12,12,12;,
  3;13,13,13;,
  3;14,14,14;,
  3;15,15,15;;
 }
}