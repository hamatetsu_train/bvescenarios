xof 0303txt 0032
template Vector {
 <3d82ab5e-62da-11cf-ab39-0020af71e433>
 FLOAT x;
 FLOAT y;
 FLOAT z;
}

template MeshFace {
 <3d82ab5f-62da-11cf-ab39-0020af71e433>
 DWORD nFaceVertexIndices;
 array DWORD faceVertexIndices[nFaceVertexIndices];
}

template Mesh {
 <3d82ab44-62da-11cf-ab39-0020af71e433>
 DWORD nVertices;
 array Vector vertices[nVertices];
 DWORD nFaces;
 array MeshFace faces[nFaces];
 [...]
}

template ColorRGBA {
 <35ff44e0-6c7c-11cf-8f52-0040333594a3>
 FLOAT red;
 FLOAT green;
 FLOAT blue;
 FLOAT alpha;
}

template ColorRGB {
 <d3e16e81-7835-11cf-8f52-0040333594a3>
 FLOAT red;
 FLOAT green;
 FLOAT blue;
}

template Material {
 <3d82ab4d-62da-11cf-ab39-0020af71e433>
 ColorRGBA faceColor;
 FLOAT power;
 ColorRGB specularColor;
 ColorRGB emissiveColor;
 [...]
}

template MeshMaterialList {
 <f6f23f42-7686-11cf-8f52-0040333594a3>
 DWORD nMaterials;
 DWORD nFaceIndexes;
 array DWORD faceIndexes[nFaceIndexes];
 [Material <3d82ab4d-62da-11cf-ab39-0020af71e433>]
}

template TextureFilename {
 <a42790e1-7810-11cf-8f52-0040333594a3>
 STRING filename;
}

template Coords2d {
 <f6f23f44-7686-11cf-8f52-0040333594a3>
 FLOAT u;
 FLOAT v;
}

template MeshTextureCoords {
 <f6f23f40-7686-11cf-8f52-0040333594a3>
 DWORD nTextureCoords;
 array Coords2d textureCoords[nTextureCoords];
}

template MeshNormals {
 <f6f23f43-7686-11cf-8f52-0040333594a3>
 DWORD nNormals;
 array Vector normals[nNormals];
 DWORD nFaceNormals;
 array MeshFace faceNormals[nFaceNormals];
}


Mesh  {
 12;
 -1.000000;0.000000;-0.100000;,
 0.000000;0.000000;-0.100000;,
 -1.000000;-20.000000;-0.100000;,
 5.000000;-20.000000;-0.100000;,
 -1.000000;0.000000;10.100000;,
 0.000000;0.000000;10.100000;,
 -1.000000;-20.000000;10.100000;,
 5.000000;-20.000000;10.100000;,
 -1.000000;0.000000;-0.100000;,
 0.000000;0.000000;-0.100000;,
 -1.000000;0.000000;10.100000;,
 0.000000;0.000000;10.100000;;
 16;
 3;0,1,3;,
 3;0,3,2;,
 3;2,3,1;,
 3;2,1,0;,
 3;1,3,7;,
 3;1,7,5;,
 3;5,7,3;,
 3;5,3,1;,
 3;5,4,6;,
 3;5,6,7;,
 3;7,6,4;,
 3;7,4,5;,
 3;9,8,10;,
 3;9,10,11;,
 3;11,10,8;,
 3;11,8,9;;

 MeshMaterialList  {
  1;
  16;
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0;

  Material  {
   1.000000;1.000000;1.000000;1.000000;;
   0.000000;
   0.000000;0.000000;0.000000;;
   0.000000;0.000000;0.000000;;

   TextureFilename  {
    "rock.dds";
   }
  }
 }

 MeshTextureCoords  {
  12;
  0.000000;0.000000;,
  1.000000;0.000000;,
  0.000000;9.000000;,
  1.000000;9.000000;,
  5.000000;0.000000;,
  4.000000;0.000000;,
  5.000000;9.000000;,
  4.000000;9.000000;,
  0.000000;0.000000;,
  1.000000;0.000000;,
  0.000000;3.000000;,
  1.000000;3.000000;;
 }

 MeshNormals  {
  16;
  0.000000;0.000000;-1.000000;,
  0.000000;0.000000;-1.000000;,
  0.000000;0.000000;1.000000;,
  0.000000;0.000000;1.000000;,
  -0.970142;-0.242536;0.000000;,
  -0.970142;-0.242536;0.000000;,
  0.970142;0.242536;0.000000;,
  0.970142;0.242536;0.000000;,
  0.000000;0.000000;1.000000;,
  0.000000;0.000000;1.000000;,
  0.000000;0.000000;-1.000000;,
  0.000000;0.000000;-1.000000;,
  0.000000;1.000000;0.000000;,
  0.000000;1.000000;0.000000;,
  0.000000;-1.000000;0.000000;,
  0.000000;-1.000000;0.000000;;
  16;
  3;0,0,0;,
  3;1,1,1;,
  3;2,2,2;,
  3;3,3,3;,
  3;4,4,4;,
  3;5,5,5;,
  3;6,6,6;,
  3;7,7,7;,
  3;8,8,8;,
  3;9,9,9;,
  3;10,10,10;,
  3;11,11,11;,
  3;12,12,12;,
  3;13,13,13;,
  3;14,14,14;,
  3;15,15,15;;
 }
}