﻿BveTs Map 2.00

// 熱海→三島 ATS-SWP閉塞
//////////////////////////////////////////////////////////////////////////

///////////////////////////////////////
// 熱海駅 (CA00)
$Atami-570 + 770;
;//●●○ 出発
		$dist = distance;
		$LastBcn=500;
	include 'utility\BeaconSWP.txt';



$Atami+430 + 180;
;//●●○ １
		$dist = distance;
		$LastBcn=500;
	include 'utility\BeaconSWP.txt';


$Atami+430 + 340;
;//●●○ 場内
		$dist = distance;
		$LastBcn=500;
	include 'utility\BeaconSWP.txt';

///////////////////////////////////////
// 来宮駅 (JT22)
$Atami+430 + 1070;
;//●●○ 出発
		$dist = distance;
		$LastBcn=500;
	include 'utility\BeaconSWP.txt';

$Atami+430 + 1760;
;//●●○ 6
		$dist = distance;
		$LastBcn=500;
	include 'utility\BeaconSWP.txt';

$Atami+430 + 2860;
;//●●○ 5
		$dist = distance;
		$LastBcn=500;
	include 'utility\BeaconSWP.txt';

$Atami+430 + 4160;
;//●●○ 4
		$dist = distance;
		$LastBcn=500;
	include 'utility\BeaconSWP.txt';

$Atami+430 + 5560;
;//●●○ 3
		$dist = distance;
		$LastBcn=500;
	include 'utility\BeaconSWP.txt';

$Atami+430 + 6960;
;//●●○ 2
		$dist = distance;
		$LastBcn=500;
	include 'utility\BeaconSWP.txt';

$Atami+430 + 8160;
;//●●○ 1
		$dist = distance;
		$LastBcn=500;
	include 'utility\BeaconSWP.txt';


$Atami+430 + 8965;
;//●●○ 場内
		$dist = distance;
		$LastBcn=500;
	include 'utility\BeaconSWP.txt';


///////////////////////////////////////
// 函南駅 (CA01)
$Atami+430 + 9742;
;//●●○ 出発
		$dist = distance;
		$LastBcn=500;
	include 'utility\BeaconSWP.txt';

$Atami+430 + 10210;
;//●●○ 4
		$dist = distance;
		$LastBcn=500;
	include 'utility\BeaconSWP.txt';

$Atami+430 + 11710;
;//●●○ 3
		$dist = distance;
		$LastBcn=500;
	include 'utility\BeaconSWP.txt';

$Atami+430 + 13380;
;//●●○ 2
		$dist = distance;
		$LastBcn=500;
	include 'utility\BeaconSWP.txt';

$Atami+430 + 14270;
;//●●○ 1
		$dist = distance;
		$LastBcn=500;
	include 'utility\BeaconSWP.txt';


$Mishima-740 + 40;
;//●●○ 場内
		$dist = distance;
		$LastBcn=500;
	include 'utility\BeaconSWP.txt';

///////////////////////////////////////
// 三島駅 (CA02)
$Mishima-740 + 1030;
;//●●○ 出発
		$dist = distance;
		$LastBcn=500;
	include 'utility\BeaconSWP.txt';



