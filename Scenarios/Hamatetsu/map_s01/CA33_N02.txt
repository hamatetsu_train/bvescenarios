﻿BveTs Map 2.00

//////////////////////////////////////////////////////////////////////////
// 東海道本線（しぞーか本線）CA33 天竜川駅 上り本線
// +3800(-253.300) ～ +4830()
//////////////////////////////////////////////////////////////////////////


// -------------------------------------------------------------------------------------------------------------


$hamamatsu + 3800;
// ◯●●(天竜川場内) (253.300)
	Section.BeginNew(0, 2, 4);
	Structure['PoleSL1'].Put(0, -2.5, 0, 0.3, 0, 0, 0, 1, 25);
	Signal['type3'].Put(0, 0, -2.2, 4.3, 0, 0, 0, 0, 1, 25);


$hamamatsu + 3830;
	Track['11'].Position(7.60, 0, 0, 0);	// 下１
	Repeater['Rail11L'].Begin('11', -0.5335, 0, 0, 0, 0, 0, 3, 5.0, 5.0, 'RailL0', 'RailL1', 'RailL2', 'RailL3', 'RailL4');
	Repeater['Rail11R'].Begin('11',  0.5335, 0, 0, 0, 0, 0, 3, 5.0, 5.0, 'RailR0', 'RailR1', 'RailR2', 'RailR3', 'RailR4');
	Repeater['Rail11u'].Begin('11', 0, 0, 0, 0, 0, 0, 3, 5, 5, 'Ballast0', 'Ballast1', 'Ballast2', 'Ballast3', 'Ballast4');


$hamamatsu+3850;
	Repeater['EnePole00'].Begin('00', 0.00, 0.00, 0.00, 0, 0, 0, 0, 1.0, 50.0, 'Pole2_0');


$hamamatsu + 4000;
	$RailNo='01';
	Track[$RailNo].Position(0.00, 0, -238, 0);	// 上１
	Repeater['Rail'+$RailNo+'u'].Begin($RailNo, 0, 0, 0, 0, 0, 0, 3, 5, 5, 'Ballast0', 'Ballast1', 'Ballast2', 'Ballast3', 'Ballast4');
		$dist = distance;
	include 'utility\TongStartL.txt';

	JointNoise.Play(0);
	Repeater['EnePole00'].Begin('00', 0.00, 0.00, 0.00, 0, 0, 0, 0, 1.0, 25.0, 'Pole2_0');


$hamamatsu+4025;
	Repeater['EnePole00'].Begin('00', -1.90, 0.00, 0.00, 0, 0, 0, 0, 1.0, 50.0, 'Pole3_0');


$hamamatsu + 4030;
	Track['01'].X.Interpolate(-1.90, 238);	// 上１


$hamamatsu + 4050;
	$RailNo='20';
	Track[$RailNo].Position(3.80, 0, 238, 0);	// 下本→下１
	Repeater['Rail'+$RailNo+'u'].Begin($RailNo, 0, 0, 0, 0, 0, 0, 3, 5, 5, 'Ballast0', 'Ballast1', 'Ballast2', 'Ballast3', 'Ballast4');
		$dist = distance;
	include 'utility\TongStartR.txt';

	Repeater['EnePole00'].Begin('00', -3.80, 0.00, 0.00, 0, 0, 0, 0, 1.0, 25.0, 'Pole3_0');


$hamamatsu + 4060; // 300mほど並走
	Track['01'].Position(-3.80, 0, 0, 0);	// 上１

	Repeater['Rail01u'].Begin('01', 0, 0, 0, 0, 0, 0, 3, 2.5, 2.5, 'Ballast0T');


$hamamatsu + 4070;
	Track['20'].Position(5.70, 0, -238, 0);	// 下本→下１


$hamamatsu + 4075;
	//※◯●●下り出発 (253.024)
$hamamatsu+4075;
	Repeater['EnePole00'].Begin('00', -3.80, 0.00, 0.00, 0, 0, 0, 0, 1.0, 50.0, 'Pole3_0');


$hamamatsu + 4090;
	$RailNo='20';
	Track[$RailNo].Position(7.60, 0, 0, 0);	// 下本→下１
	Repeater['Rail'+$RailNo+'u'].End();
	Repeater['Kasen'+$RailNo].End();
		$dist = distance;
	include 'utility\TongEndL.txt';


//■ +4100 = 253km
$hamamatsu + 4100;
	Track['10'].X.Interpolate(3.80, 0);	// 下本
	Structure['Kilo_C0'].Put(0, -2.3, 0, 0, 0, 0, 0, 1, 1);
	Structure['Kilo_C0'].Put(1,  2.3, 0, 0, 0, 0, 0, 1, 1);


$hamamatsu + 4180;
	Track['11'].X.Interpolate(7.60, 0);	// 下１


$hamamatsu + 4200;
	Structure['CrackR0'].PutBetween('01', '11');


$hamamatsu + 4220;
// 歩道橋
	Structure['hodokyo01'].Put(0, 1.9, 0, 0.3, 0, 0, 0, 1, 25);


$hamamatsu + 4225; // ？
	Track['01'].X.Interpolate(-3.80, -329);	// 上１
	Structure['CrackR0'].PutBetween('01', '11');


$hamamatsu + 4250;
	Track['01'].X.Interpolate(-4.75, 0);	// 上１
	Track['11'].X.Interpolate(10.45, 0);	// 下１

	Track['下2'].Position(10.45, 0);
	Repeater['RailK2L'].Begin('下2', -0.5335, 0, 0, 0, 0, 0, 3, 5.0, 5.0, 'RailL0', 'RailL1', 'RailL2', 'RailL3', 'RailL4');
	Repeater['RailK2R'].Begin('下2',  0.5335, 0, 0, 0, 0, 0, 3, 5.0, 5.0, 'RailR0', 'RailR1', 'RailR2', 'RailR3', 'RailR4');
	Repeater['RailK2u'].Begin('下2', 0, 0, 0, 0, 0, 0, 3, 5, 5, 'Ballast0', 'Ballast1', 'Ballast2', 'Ballast3', 'Ballast4');

	Repeater['EnePole00'].Begin('00', -5.70, 0.00, 0.00, 0, 0, 0, 0, 1.0, 50.0, 'Pole4_0');
	Structure['CrackR0'].PutBetween('01', '11');


$hamamatsu + 4275;
	Track['01'].X.Interpolate(-6.65, 0);	// 上１
	Track['下2'].X.Interpolate(13.30, 0);

	Track['下3'].Position(13.30, 0);
	Repeater['RailK3L'].Begin('下3', -0.5335, 0, 0, 0, 0, 0, 3, 5.0, 5.0, 'RailL0', 'RailL1', 'RailL2', 'RailL3', 'RailL4');
	Repeater['RailK3R'].Begin('下3',  0.5335, 0, 0, 0, 0, 0, 3, 5.0, 5.0, 'RailR0', 'RailR1', 'RailR2', 'RailR3', 'RailR4');
	Repeater['RailK3u'].Begin('下3', 0, 0, 0, 0, 0, 0, 3, 5, 5, 'Ballast0', 'Ballast1', 'Ballast2', 'Ballast3', 'Ballast4');
	Structure['CrackR0'].PutBetween('01', '11');


//▼▼▼ 天竜川 (CA33)
$hamamatsu + 4300;
	Track['01'].X.Interpolate(-8.55, 329);	// 上１
	Track['11'].X.Interpolate(11.40, 0);	// 下１
	Track['下2'].X.Interpolate(15.20, 0);
	Track['下3'].X.Interpolate(17.10, 0);

	Track['下4'].Position(17.10, 0);
	Repeater['RailK4L'].Begin('下4', -0.5335, 0, 0, 0, 0, 0, 3, 5.0, 5.0, 'RailL0', 'RailL1', 'RailL2', 'RailL3', 'RailL4');
	Repeater['RailK4R'].Begin('下4',  0.5335, 0, 0, 0, 0, 0, 3, 5.0, 5.0, 'RailR0', 'RailR1', 'RailR2', 'RailR3', 'RailR4');
	Repeater['RailK4u'].Begin('下4', 0, 0, 0, 0, 0, 0, 3, 5, 5, 'Ballast0', 'Ballast1', 'Ballast2', 'Ballast3', 'Ballast4');

	Repeater['Form01SR'].Begin('01', 0, 0, 0, 0, 0, 0, 3, 25.0, 25.0, 'FormR1');
	//Repeater['Form00CL'].Begin(0, 0, 0, 0, 0, 0, 0, 3, 25.0, 25.0, 'FormCL1');
	Repeater['Form00SL'].Begin(0, 0, 0, 0, 0, 0, 0, 3, 25.0, 25.0, 'FormL1');
	Structure['FormCL1'].PutBetween(0, '01');
	Repeater['Form10SR'].Begin('10', 0, 0, 0, 0, 0, 0, 3, 25.0, 25.0, 'FormR1');
	Repeater['Form11SL'].Begin('11', 0, 0, 0, 0, 0, 0, 3, 25.0, 25.0, 'FormL1');
	Structure['FormCR1'].PutBetween('10', '11');

	Repeater['EnePole00'].Begin('00', 0.00, 0.00, 0.00, 0, 0, 0, 0, 1.0, 50.0, 'Pole1_0');


$hamamatsu + 4325;
	Track['01'].X.Interpolate(-9.50, 0);	// 上１
	Track['下3'].X.Interpolate(19.00, 0);
	Track['下4'].X.Interpolate(20.90, 0);

	Track['下5'].Position(20.90, 0);
	Repeater['RailK5L'].Begin('下5', -0.5335, 0, 0, 0, 0, 0, 3, 5.0, 5.0, 'RailL0', 'RailL1', 'RailL2', 'RailL3', 'RailL4');
	Repeater['RailK5R'].Begin('下5',  0.5335, 0, 0, 0, 0, 0, 3, 5.0, 5.0, 'RailR0', 'RailR1', 'RailR2', 'RailR3', 'RailR4');
	Repeater['RailK5u'].Begin('下5', 0, 0, 0, 0, 0, 0, 3, 5, 5, 'Ballast0', 'Ballast1', 'Ballast2', 'Ballast3', 'Ballast4');

	Structure['FormCL1'].PutBetween(0, '01');
	Structure['FormCR1'].PutBetween('10', '11');


$hamamatsu + 4350;
	Track['下4'].X.Interpolate(22.80, 0);
	Track['下5'].X.Interpolate(24.70, 0);

	Track['下6'].Position(24.70, 0);
	Repeater['RailK6L'].Begin('下6', -0.5335, 0, 0, 0, 0, 0, 3, 5.0, 5.0, 'RailL0', 'RailL1', 'RailL2', 'RailL3', 'RailL4');
	Repeater['RailK6R'].Begin('下6',  0.5335, 0, 0, 0, 0, 0, 3, 5.0, 5.0, 'RailR0', 'RailR1', 'RailR2', 'RailR3', 'RailR4');
	Repeater['RailK6u'].Begin('下6', 0, 0, 0, 0, 0, 0, 3, 5, 5, 'Ballast0', 'Ballast1', 'Ballast2', 'Ballast3', 'Ballast4');

	Structure['FormCL1'].PutBetween(0, '01');
	Structure['FormCR1'].PutBetween('10', '11');


$hamamatsu + 4375;
	Track['下5'].X.Interpolate(26.60, 0);
	Track['下6'].X.Interpolate(28.50, 0);
	Structure['FormCL1'].PutBetween(0, '01');
	Structure['FormCR1'].PutBetween('10', '11');


$hamamatsu + 4400;
	Track['下6'].X.Interpolate(30.40, 0);
	Structure['FormCL1'].PutBetween(0, '01');
	Structure['FormCR1'].PutBetween('10', '11');


$hamamatsu + 4425;
	Structure['FormCL1'].PutBetween(0, '01');
	Structure['FormCR1'].PutBetween('10', '11');


$hamamatsu + 4440;
// [○B]
	Structure['StaPole00'].Put(0, -2.3, 3.3, 0, 0, 0, 0, 1, 1.0);
	Structure['StaStop06'].Put(0, -2.3, 3.3, 0, 0, 0, 0, 1, 1.0);


$hamamatsu + 4450;
	Track['下3'].X.Interpolate(19.00, -129);

	Track['下4'].X.Interpolate(22.80, 0);
	Repeater['RailK4L'].End();
	Repeater['RailK4R'].End();
	Repeater['RailK4u'].End();

	Track['下5'].X.Interpolate(26.60, 0);
	Repeater['RailK5L'].End();
	Repeater['RailK5R'].End();
	Repeater['RailK5u'].End();

	Track['下6'].X.Interpolate(30.40, -129);

	Structure['FormCL1'].PutBetween(0, '01');
	Structure['FormCR1'].PutBetween('10', '11');


$hamamatsu + 4454;
	// 跨線橋末端
	Structure['Tenryu_sta'].Put(0, 1.9, 0, 0, 0, 0, 0, 1, 25);


$hamamatsu + 4475;
	Track['下3'].X.Interpolate(17.10, 129);
	Track['下6'].X.Interpolate(28.50, 0);
	Structure['FormCL1'].PutBetween(0, '01');
	Structure['FormCR1'].PutBetween('10', '11');


$hamamatsu + 4500;
	Track['上3'].Position(-17.10, 0);
	Repeater['RailN3L'].Begin('上3', -0.5335, 0, 0, 0, 0, 0, 3, 5.0, 5.0, 'RailL0', 'RailL1', 'RailL2', 'RailL3', 'RailL4');
	Repeater['RailN3R'].Begin('上3',  0.5335, 0, 0, 0, 0, 0, 3, 5.0, 5.0, 'RailR0', 'RailR1', 'RailR2', 'RailR3', 'RailR4');
	Repeater['RailN3u'].Begin('上3', 0, 0, 0, 0, 0, 0, 3, 5, 5, 'Ballast0', 'Ballast1', 'Ballast2', 'Ballast3', 'Ballast4');

	Track['上2'].Position(-13.30, 0);
	Repeater['RailN2L'].Begin('上2', -0.5335, 0, 0, 0, 0, 0, 3, 5.0, 5.0, 'RailL0', 'RailL1', 'RailL2', 'RailL3', 'RailL4');
	Repeater['RailN2R'].Begin('上2',  0.5335, 0, 0, 0, 0, 0, 3, 5.0, 5.0, 'RailR0', 'RailR1', 'RailR2', 'RailR3', 'RailR4');
	Repeater['RailN2u'].Begin('上2', 0, 0, 0, 0, 0, 0, 3, 5, 5, 'Ballast0', 'Ballast1', 'Ballast2', 'Ballast3', 'Ballast4');

	Track['下3'].X.Interpolate(15.20, 0);
	Repeater['RailK3L'].End();
	Repeater['RailK3R'].End();
	Repeater['RailK3u'].End();

	Track['下6'].X.Interpolate(24.70, 0);

	Structure['FormCL1'].PutBetween(0, '01');
	Structure['FormCR1'].PutBetween('10', '11');


$hamamatsu + 4525;
	$RailNo='22';
	Track[$RailNo].Position(15.20, 0, -129, 0);	// 下２→下１
	Repeater['Rail'+$RailNo+'u'].Begin($RailNo, 0, 0, 0, 0, 0, 0, 3, 5, 5, 'Ballast0', 'Ballast1', 'Ballast2', 'Ballast3', 'Ballast4');
		$dist = distance;
	include 'utility\TongStartL.txt';

	Track['下6'].X.Interpolate(20.90, 0);

	Structure['FormCL1'].PutBetween(0, '01');
	Structure['FormCR1'].PutBetween('10', '11');


$hamamatsu + 4550;
	Track['22'].X.Interpolate(13.30, 129);	// 下２→下１
	Track['下6'].X.Interpolate(17.10, 129);
	Structure['FormCL1'].PutBetween(0, '01');
	Structure['FormCR1'].PutBetween('10', '11');


$hamamatsu + 4575;
	Track['01'].X.Interpolate(-9.50, 0);	// 上１
	Track['11'].X.Interpolate(11.40, 0);	// 下１

	$RailNo='22';
	Track[$RailNo].Position(11.40, 0, 0, 0);	// 下２→下１
	Repeater['Rail'+$RailNo+'u'].End();
	Repeater['Kasen'+$RailNo].End();
		$dist = distance;
	include 'utility\TongEndR.txt';

	Track['下6'].X.Interpolate(15.20, 0);
	Repeater['RailK6L'].End();
	Repeater['RailK6R'].End();
	Repeater['RailK6u'].End();

	Repeater['Form01SR'].End();
	//Repeater['Form00CL'].End();
	Repeater['Form00SL'].End();
	Repeater['Form10SR'].End();
	//Repeater['Form10CR'].End();
	Repeater['Form11SL'].End();

	Structure['CrackR0'].PutBetween('01', 0);
	Structure['CrackR0'].PutBetween('10', '11');
//▲▲▲ 天竜川 (CA33)


$hamamatsu + 4590;
	Track['上3'].Position(-17.1, 0);
	Track['上2'].Position(-13.3, 0);
	Track['01'].Position(-9.5, 0, 0, 0);	// 上１
	Track['11'].Position(11.4, 0, 0, 0);	// 下１
	Track['下2'].X.Interpolate(15.20, 0);


$hamamatsu + 4600;
	Track['上3'].Position(-16.9, 0);
	Structure['Kilo_C5'].Put(0, -2.3, 0, 0, 0, 0, 0, 1, 1);
	Structure['Kilo_C5'].Put(1,  2.3, 0, 0, 0, 0, 0, 1, 1);
	Structure['CrackR0'].PutBetween('01', 0);
	Structure['CrackR0'].PutBetween('10', '11');
	Repeater['EnePole00'].Begin('00', 0.00, 0.00, 0.00, 0, 0, 0, 0, 1.0, 50.0, 'Pole4_0');


$hamamatsu + 4610;
	// Rr1000 - TCLx - CCLx - Cx
		$dist = distance;
		$radius=1000;
		$cant=25;
	include 'utility\CurveStart.txt';


$hamamatsu + 4625;
	Track['上3'].Position(-13.7, 0);
	Track['上2'].Position(-11.8, 0);

	Structure['CrackR0'].PutBetween('01', 0);
	Structure['CrackR0'].PutBetween('10', '11');

	Repeater['EnePole00'].Begin('00', 0.00, 0.00, 0.00, 0, 0, 0, 0, 1.0, 50.0, 'Pole3_0');


$hamamatsu + 4650;
	Track['上3'].Position(-8.4, 0);
	Repeater['RailN3L'].End();
	Repeater['RailN3R'].End();
	Repeater['RailN3u'].End();
	Track['上2'].Position(-8.4, 0);

	Structure['CrackR0'].PutBetween('01', 0);
	Structure['CrackR0'].PutBetween('10', '11');

	Repeater['EnePole00'].Begin('00', 0.00, 0.00, 0.00, 0, 0, 0, 0, 1.0, 50.0, 'Pole3_0');


$hamamatsu + 4662;
	//◯●●出発 (252.438)
	Section.BeginNew(0, 2, 4);
	Structure['PoleSL1'].Put(0, -2.5, 0, 0.3, 0, 0, 0, 1, 25);
	Signal['type3'].Put(0, 0, -2.5, 4.3, 0, 0, 0, 0, 1, 25);


$hamamatsu + 4675;
	Track['上2'].Position(-5.0, 0);
	Repeater['RailN2L'].End();
	Repeater['RailN2R'].End();
	Repeater['RailN2u'].End();
	Track['01'].Position(-5.0, 0);	// 上１
	Structure['CrackR0'].PutBetween('01', 0);
	Structure['CrackR0'].PutBetween('10', '11');


$hamamatsu + 4685;
	Track['上引上'].Position(-3.80, 0);
	Repeater['RailN4L'].Begin('上引上', -0.5335, 0, 0, 0, 0, 0, 3, 5.0, 5.0, 'RailL0', 'RailL1', 'RailL2', 'RailL3', 'RailL4');
	Repeater['RailN4R'].Begin('上引上',  0.5335, 0, 0, 0, 0, 0, 3, 5.0, 5.0, 'RailR0', 'RailR1', 'RailR2', 'RailR3', 'RailR4');
	Repeater['RailN4u'].Begin('上引上', 0, 0, 0, 0, 0, 0, 3, 5, 5, 'Ballast0', 'Ballast1', 'Ballast2', 'Ballast3', 'Ballast4');

	Track['01'].Position(-3.8, 0, 238, 0);	// 上１


$hamamatsu+4700;
	Repeater['EnePole00'].Begin('00', -3.80, 0.00, 0.00, 0, 0, 0, 0, 1.0, 50.0, 'Pole3_0');


$hamamatsu + 4705;
	Track['01'].Position(-1.9, 0, -238, 0);

	$RailNo='21';
	Track[$RailNo].Position(3.80, 0, -238, 0);	// 下本→上本
	Repeater['Rail'+$RailNo+'u'].Begin($RailNo, 0, 0, 0, 0, 0, 0, 3, 5, 5, 'Ballast0', 'Ballast1', 'Ballast2', 'Ballast3', 'Ballast4');
		$dist = distance;
	include 'utility\TongStartL.txt';

	$RailNo='11';
	Track[$RailNo].Position(3.80, 0, 0, 0);	// 下１
	Repeater['Rail'+$RailNo+'u'].End();
	Repeater['Kasen'+$RailNo].End();
		$dist = distance;
	include 'utility\TongEndR.txt';

	Track['下2'].X.Interpolate(7.60, 0);
	JointNoise.Play(0);


$hamamatsu + 4725;
	$RailNo='01';
	Track[$RailNo].Position(0.00, 0, 0, 0);	// 上１
	Repeater['Rail'+$RailNo+'u'].End();
	Repeater['Kasen'+$RailNo].End();
		$dist = distance;
	include 'utility\TongEndL.txt';

	Track['21'].Position(2.75, 0, 0, 0);	// 下本→上本


$hamamatsu + 4745;
	Track['21'].Position(0.95, 0, 0, 0);	// 下本→上本
	JointNoise.Play(0);


$hamamatsu + 4765;
	$RailNo='21';
	Track[$RailNo].Position(0.00, 0, 0, 0);	// 下本→上本
	Repeater['Rail'+$RailNo+'u'].End();
	Repeater['Kasen'+$RailNo].End();
		$dist = distance;
	include 'utility\TongEndR.txt';


$hamamatsu + 4780;
	//┛+10.0‰
		$dist = distance;
		$slope=10.0;
	include 'utility\GraUpStart.txt';


$hamamatsu + 4825;
	Track['上引上'].Position(-5.7, 0);
	Repeater['RailN4L'].End();
	Repeater['RailN4R'].End();
	Repeater['RailN4u'].End();



// -------------------------------------------------------------------------------------------------------------


// (C)Harupi
