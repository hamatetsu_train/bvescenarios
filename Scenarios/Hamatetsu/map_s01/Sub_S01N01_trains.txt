﻿BveTs Map 2.00

// 他列車読み込み ----------------------------------------------------------------------------------------------
0;
	//Train['3626G'].Load('harupi\Train\313-YURU.txt', '10', -1);
	//Train['4004M'].Load('..\Structures\N209\373\373.txt', '10', -1);

	Train['917M'].Load('map_s01\Train_917M1.txt', '10', -1); // GG + W + LL
	Train['5091'].Load('map_s01\Train_JRF1.txt', '10', -1); // 
	Train['729M'].Load('map_s01\Train_729M1.txt', '10', -1); // T + LL
	Train['421M'].Load('map_s01\Train_421M1.txt', '10', -1); // GG + T
	Train['731M'].Load('map_s01\Train_731M1.txt', '10', -1); // T + GG
	Train['733M'].Load('map_s01\Train_733M1.txt', '10', -1); // LL + W


///////////////////////////////////////
// 917M
0; // <0>
	Train['917M'].Enable('07:45:00');

$hamamatsu+100 - 100;
	Train['917M'].Stop(2.0, 30, 2.0, 100);

$hamamatsu + 4320;
	Train['917M'].Stop(2.0, 30, 2.0, 100);


///////////////////////////////////////
// 5091
0; // <0>
	Train['5091'].Enable('07:46:00');

$hamamatsu+100 - 100;
	Train['5091'].Stop(2.0, 30, 2.0, 90);

$hamamatsu + 14295;
	Train['5091'].Stop(2.0, 30, 2.0, 100);


///////////////////////////////////////
// 729M
0; // <0>
	Train['729M'].Enable('07:48:00');

$hamamatsu+100 - 100;
	Train['729M'].Stop(2.0, 30, 2.0, 100);

$hamamatsu + 4320;
	Train['729M'].Stop(2.0, 30, 2.0, 100);

$hamamatsu + 8260;
	Train['729M'].Stop(2.0, 30, 2.0, 100);

$hamamatsu + 11190;
	Train['729M'].Stop(2.0, 30, 2.0, 100);

$hamamatsu + 14295;
	Train['729M'].Stop(2.0, 30, 2.0, 100);

///////////////////////////////////////
// 421M
0; // <0>
	Train['421M'].Enable('07:46:00');

$hamamatsu+100 - 100;
	Train['421M'].Stop(2.0, 30, 2.0, 100);

$hamamatsu + 4320;
	Train['421M'].Stop(2.0, 30, 2.0, 100);

$hamamatsu + 8260;
	Train['421M'].Stop(2.0, 30, 2.0, 100);

$hamamatsu + 11190;
	Train['421M'].Stop(2.0, 30, 2.0, 100);

$hamamatsu + 14295;
	Train['421M'].Stop(2.0, 30, 2.0, 100);

$hamamatsu + 18905;
	Train['421M'].Stop(2.0, 30, 2.0, 100);

$hamamatsu + 22350;
	Train['421M'].Stop(2.0, 30, 2.0, 100);

$hamamatsu + 27710;
	Train['421M'].Stop(2.0, 30, 2.0, 100);


///////////////////////////////////////
// 731M
0; // <0>
	Train['731M'].Enable('07:55:00');

$hamamatsu+100 - 100;
	Train['731M'].Stop(2.0, 30, 2.0, 100);

$hamamatsu + 4320;
	Train['731M'].Stop(2.0, 30, 2.0, 100);

$hamamatsu + 8260;
	Train['731M'].Stop(2.0, 30, 2.0, 100);

$hamamatsu + 11190;
	Train['731M'].Stop(2.0, 30, 2.0, 100);

$hamamatsu + 14295;
	Train['731M'].Stop(2.0, 30, 2.0, 100);

$hamamatsu + 18905;
	Train['731M'].Stop(2.0, 30, 2.0, 100);

$hamamatsu + 22350;
	Train['731M'].Stop(2.0, 30, 2.0, 100);

$hamamatsu + 27710;
	Train['731M'].Stop(2.0, 30, 2.0, 100);


///////////////////////////////////////
// 733M
0; // <0>
	Train['733M'].Enable('08:16:00');

$hamamatsu+100 - 100;
	Train['733M'].Stop(2.0, 30, 2.0, 100);

$hamamatsu + 4320;
	Train['733M'].Stop(2.0, 30, 2.0, 100);

$hamamatsu + 8260;
	Train['733M'].Stop(2.0, 30, 2.0, 100);

$hamamatsu + 11190;
	Train['733M'].Stop(2.0, 30, 2.0, 100);

$hamamatsu + 14295;
	Train['733M'].Stop(2.0, 30, 2.0, 100);

$hamamatsu + 18905;
	Train['733M'].Stop(2.0, 30, 2.0, 100);

$hamamatsu + 22350;
	Train['733M'].Stop(2.0, 30, 2.0, 100);

$hamamatsu + 27710;
	Train['733M'].Stop(2.0, 30, 2.0, 100);

