BveTs Structure List 2.00


# Hamatetsu objects
# - nΚ
Ground08La	, ..\harupi\Ground\Ground_08La.x
Ground08Lb	, ..\harupi\Ground\Ground_08Lb.x
# - OΉ
Rail_GrDark01	, ..\harupi\Rail\Rail_GrDark01.x
# - wΦW
Tenryu_sta	, ..\harupi\Form\CA\Tenryu_sta.x
CA02_StaHouse	, ..\harupi\Building\CA\CA02_StaHouse.x
CA02_Roof01	, ..\harupi\Form\CA\CA02_FormRoof1.x
CA02_Roof02	, ..\harupi\Form\CA\CA02_FormRoof2.x
CA02_Roof03	, ..\harupi\Form\CA\CA02_FormRoof3.x
CA02_Roof04	, ..\harupi\Form\CA\CA02_FormRoof4.x
Form_CA0201R	, ..\harupi\Form\CA\Form_CA0201R.x
Form_CA0202R	, ..\harupi\Form\CA\Form_CA0202R.x
CA03_RoofR1	, ..\harupi\Form\CA\CA03_FormRoofR1.x
CA03_RoofR2	, ..\harupi\Form\CA\CA03_FormRoofR2.x
CA03_Shop	, ..\harupi\Form\CA\CA03_FormShop.x
CA03_Tsuro	, ..\harupi\Form\CA\CA03_Tsuro.x
CA30_FormL1	, ..\harupi\Form\CA\FormL_CA30a.x
CA30_FormL2	, ..\harupi\Form\CA\FormL_CA30b.x
CA30_FormL3	, ..\harupi\Form\CA\FormL_CA30c.x
CA30_FormR1	, ..\harupi\Form\CA\FormR_CA30a.x
CA30_FormR2	, ..\harupi\Form\CA\FormR_CA30b.x
CA30_FormR3	, ..\harupi\Form\CA\FormR_CA30c.x
CA30_RoofL1	, ..\harupi\Form\CA\RoofL_CA30a.x
CA30_RoofL2	, ..\harupi\Form\CA\RoofL_CA30b.x
CA30_RoofR1	, ..\harupi\Form\CA\RoofR_CA30a.x
CA30_RoofR2	, ..\harupi\Form\CA\RoofR_CA30b.x
CA30_UsagiL	, ..\harupi\Form\CA\FormL_CA30u.x
# # βΤW
StaPole00	, ..\harupi\Pole\StaPole00.x
StaStop00	, ..\harupi\Kamban\StaStop\StaStop00.x
# StaStop01	, ..\harupi\Kamban\StaStop\StaStop01.x
StaStop02	, ..\harupi\Kamban\StaStop\StaStop02.x
StaStop03	, ..\harupi\Kamban\StaStop\StaStop03.x
StaStop04	, ..\harupi\Kamban\StaStop\StaStop04.x
StaStop05	, ..\harupi\Kamban\StaStop\StaStop05.x
StaStop06	, ..\harupi\Kamban\StaStop\StaStop06.x
StaStop07	, ..\harupi\Kamban\StaStop\StaStop07.x
StaStop08	, ..\harupi\Kamban\StaStop\StaStop08.x
StaStop09	, ..\harupi\Kamban\StaStop\StaStop09.x
StaStop10	, ..\harupi\Kamban\StaStop\StaStop10.x
StaStop11	, ..\harupi\Kamban\StaStop\StaStop11.x
StaStop12	, ..\harupi\Kamban\StaStop\StaStop12.x
StaStop14	, ..\harupi\Kamban\StaStop\StaStop14.x
# StaStop15	, ..\harupi\Kamban\StaStop\StaStop15.x
StaStop16	, ..\harupi\Kamban\StaStop\StaStop16.x
StaStop17	, ..\harupi\Kamban\StaStop\StaStop17.x
StaStop18	, ..\harupi\Kamban\StaStop\StaStop18.x
# StaStop19	, ..\harupi\Kamban\StaStop\StaStop19.x
# StaStop60	, ..\harupi\Kamban\StaStop\StaStop60.x
StaStop99	, ..\harupi\Kamban\StaStop\StaStop99.x
# - M@
LimpSigC	, ..\harupi\Kamban\SlowlyGo.x
LimpSigS	, ..\harupi\Kamban\LimpSignal0.x
LimpSigE	, ..\harupi\Kamban\LimpSignal1.x
# - W―
#SlowlyGo	, ..\harupi\Kamban\SlowlyGo.x
SigCall00	, ..\harupi\Kamban\SigCall\SigCall00.x
SigCall01	, ..\harupi\Kamban\SigCall\SigCall01.x
SigCall02	, ..\harupi\Kamban\SigCall\SigCall02.x
SigCall03	, ..\harupi\Kamban\SigCall\SigCall03.x
SigCall04	, ..\harupi\Kamban\SigCall\SigCall04.x
SigCall05	, ..\harupi\Kamban\SigCall\SigCall05.x
SigCall06	, ..\harupi\Kamban\SigCall\SigCall06.x
SigCall07	, ..\harupi\Kamban\SigCall\SigCall07.x
SigCall08	, ..\harupi\Kamban\SigCall\SigCall08.x
SigCall09	, ..\harupi\Kamban\SigCall\SigCall09.x
SigCall20	, ..\harupi\Kamban\SigCall\SigCall20.x
SigCall21	, ..\harupi\Kamban\SigCall\SigCall21.x
SigCall22	, ..\harupi\Kamban\SigCall\SigCall22.x
# - ©Μ@
Vending001	, ..\harupi\Building\VM\VendingMachine001.x
Vending002	, ..\harupi\Building\VM\VendingMachine002.x
Vending003	, ..\harupi\Building\VM\VendingMachine003.x
Vending004	, ..\harupi\Building\VM\VendingMachine004.x
Vending005	, ..\harupi\Building\VM\VendingMachine005.x
Vending006	, ..\harupi\Building\VM\VendingMachine006.x
Vending007	, ..\harupi\Building\VM\VendingMachine007.x
Vending008	, ..\harupi\Building\VM\VendingMachine008.x
Vending009	, ..\harupi\Building\VM\VendingMachine009.x
Vending010	, ..\harupi\Building\VM\VendingMachine010.x
# - ½ΔΰΜ
S_Hotel		, ..\harupi\Building\S_Hotel.x
CA02_BuildingL1	, ..\harupi\Building\CA\CA02_BuildingL1.x
CA02_BuildingR1	, ..\harupi\Building\CA\CA02_BuildingR1.x
CA02_Hotel1	, ..\harupi\Building\CA\CA02_Hotel1.x
CA02_Hotel2	, ..\harupi\Building\CA\CA02_Hotel2.x
StaHouse_CA30	, ..\harupi\Building\CA\StaHouse_CA30.x
# - zη
DikeCR		, ..\harupi\Dike\DikeConR4.x
DikeCL		, ..\harupi\Dike\DikeConL4.x
DikeWR		, ..\harupi\Dike\DikeConWR4.x
DikeWL		, ..\harupi\Dike\DikeConWL4.x
DikeRR		, ..\harupi\Dike\DikeRockR4.x
DikeRL		, ..\harupi\Dike\DikeRockL4.x
# - ©Χ
CA02_WallR1	, ..\harupi\Building\CA\CA02_WallR1.x
CA02_WallR2	, ..\harupi\Building\CA\CA02_WallR2.x
CA12_CT1	, ..\harupi\Wall\CA\Ca12ConTun1.x
CA12_CT2	, ..\harupi\Wall\CA\Ca12ConTun2.x
CA30_WallCon	, ..\harupi\Wall\CC\StaWall_CC30_Con.x
AmiW		, ..\harupi\Wall\AmiW.x
# - ΕΒ
MARS		, ..\harupi\Kamban\MARS.x
CA03_Kamotsu	, ..\harupi\Kamban\CA03_NewKamotsu.x
CA03_Kichi	, ..\harupi\Kamban\CA03_NewKichi.x
CA13_288	, ..\harupi\Kamban\CA13_288m.x
CA30_Name	, ..\harupi\Kamban\StaName\StaName_CA30.x
# - ΉH
ShintoumeiBridge1	, ..\harupi\Road\ShintoumeiBridge1.x
ShintoumeiBridge2	, ..\harupi\Road\ShintoumeiBridge2.x
ShintoumeiPier1		, ..\harupi\Road\ShintoumeiPier1.x
ShintoumeiPier2		, ..\harupi\Road\ShintoumeiPier2.x


# ΔpXgN`
# - wi
Bg		, ..\..\Structures\strpack1\Bg.x
Grass		, ..\..\Structures\strpack1\Grass.x
# - Dike
DikeL0		, ..\harupi\Old\Dike\DikeL.x
DikeR0		, ..\harupi\Old\Dike\DikeR.x
DikeL1		, ..\harupi\Old\Wall\WallPole.x
DikeR1		, ..\harupi\Old\Wall\WallPole.x
# - Crack
CrackL0		, ..\harupi\Old\Crack\CraBalL.x
CrackR0		, ..\harupi\Old\Crack\CraBalR.x
CrackL1		, ..\harupi\Old\Crack\CraSlbL.x
CrackR1		, ..\harupi\Old\Crack\CraSlbR.x
# - ¬x§ΐW
SpeedlimBeginL	, ..\..\structures\default\SpeedlimL.x
SpeedlimBegin	, ..\..\structures\default\Speedlim.x
SpeedlimBeginR	, ..\..\structures\default\SpeedlimR.x
SpeedlimEnd	, ..\..\structures\default\SpeedlimEnd.x
SpeedlimPole	, ..\..\structures\default\SpeedlimPole.x
Speedlim0	, ..\..\structures\default\Speedlim0.x
Speedlim1	, ..\..\structures\default\Speedlim1.x
Speedlim2	, ..\..\structures\default\Speedlim2.x
Speedlim3	, ..\..\structures\default\Speedlim3.x
Speedlim4	, ..\..\structures\default\Speedlim4.x
Speedlim5	, ..\..\structures\default\Speedlim5.x
Speedlim6	, ..\..\structures\default\Speedlim6.x
Speedlim7	, ..\..\structures\default\Speedlim7.x
Speedlim8	, ..\..\structures\default\Speedlim8.x
Speedlim9	, ..\..\structures\default\Speedlim9.x
# - M@
Signal0L0	, ..\..\structures\default\signal20.x
Signal0L4	, ..\..\structures\default\signal24.x
Signal0G0	, ..\..\structures\default\glare20.x
Signal0G4	, ..\..\structures\default\glare24.x
Signal1L0	, ..\..\structures\default\signal4yg0.x
Signal1L2	, ..\..\structures\default\signal4yg2.x
Signal1L3	, ..\..\structures\default\signal4yg3.x
Signal1L4	, ..\..\structures\default\signal4yg4.x
Signal1G0	, ..\..\structures\default\glare4yg0.x
Signal1G2	, ..\..\structures\default\glare4yg2.x
Signal1G3	, ..\..\structures\default\glare4yg3.x
Signal1G4	, ..\..\structures\default\glare4yg4.x
Signal2L0	, ..\..\structures\default\signal20.x
Signal2L2	, ..\..\structures\default\signal22.x
Signal2G0	, ..\..\structures\default\glare20.x
Signal2G2	, ..\..\structures\default\glare22.x
Signal3L0	, ..\..\structures\default\signal30.x
Signal3L2	, ..\..\structures\default\signal32.x
Signal3L4	, ..\..\structures\default\signal34.x
Signal3G0	, ..\..\structures\default\glare30.x
Signal3G2	, ..\..\structures\default\glare32.x
Signal3G4	, ..\..\structures\default\glare34.x
Signal4L0	, ..\..\structures\default\signal4yy0.x
Signal4L1	, ..\..\structures\default\signal4yy1.x
Signal4L2	, ..\..\structures\default\signal4yy2.x
Signal4L4	, ..\..\structures\default\signal4yy4.x
Signal4G0	, ..\..\structures\default\glare4yy0.x
Signal4G1	, ..\..\structures\default\glare4yy1.x
Signal4G2	, ..\..\structures\default\glare4yy2.x
Signal4G4	, ..\..\structures\default\glare4yy4.x
Signal5L0	, ..\..\structures\default\signal50.x
Signal5L1	, ..\..\structures\default\signal51.x
Signal5L2	, ..\..\structures\default\signal52.x
Signal5L3	, ..\..\structures\default\signal53.x
Signal5L4	, ..\..\structures\default\signal54.x
Signal5G0	, ..\..\structures\default\glare50.x
Signal5G1	, ..\..\structures\default\glare51.x
Signal5G2	, ..\..\structures\default\glare52.x
Signal5G3	, ..\..\structures\default\glare53.x
Signal5G4	, ..\..\structures\default\glare54.x
SignalRL0	, ..\..\structures\default\signalrep0.x
SignalRL1	, ..\..\structures\default\signalrep1.x
SignalRL4	, ..\..\structures\default\signalrep4.x
# - OΉ
Ballast0T	, ..\..\Structures\strpack1\Ballast0T.x
Ballast0	, ..\..\Structures\strpack1\Ballast0.x
Ballast1	, ..\..\Structures\strpack1\Ballast1.x
Ballast2	, ..\..\Structures\strpack1\Ballast2.x
Ballast3	, ..\..\Structures\strpack1\Ballast3.x
Ballast4	, ..\..\Structures\strpack1\Ballast4.x
RailL0		, ..\..\Structures\strpack1\RailL0.x
RailL1		, ..\..\Structures\strpack1\RailL1.x
RailL2		, ..\..\Structures\strpack1\RailL2.x
RailL3		, ..\..\Structures\strpack1\RailL3.x
RailL4		, ..\..\Structures\strpack1\RailL4.x
RailR0		, ..\..\Structures\strpack1\RailR0.x
RailR1		, ..\..\Structures\strpack1\RailR1.x
RailR2		, ..\..\Structures\strpack1\RailR2.x
RailR3		, ..\..\Structures\strpack1\RailR3.x
RailR4		, ..\..\Structures\strpack1\RailR4.x

FormL1		, ..\harupi\Old\Form\FormL1.x
FormR1		, ..\harupi\Old\Form\FormR1.x
FormCL1		, ..\harupi\Old\Form\FormCL1.x
FormCR1		, ..\harupi\Old\Form\FormCR1.x
RoofL1		, ..\harupi\Old\Form\RoofL.x
RoofR1		, ..\harupi\Old\Form\RoofR.x
# RoofCL1		, ..\..\2-Hamatetsu\structures\RoofCL.x
RoofCR1		, ..\harupi\Old\Form\RoofCR.x

Beacon_p	, ..\harupi\Rail\Beacon_pt.x
KasenS0		, ..\harupi\Pole\kasen25_01S.x
# KasenS1		, ..\harupi\Pole\kasen50_01S.x
# KasenL1		, ..\harupi\Pole\kasen50_01L.x
# KasenR1		, ..\harupi\Pole\kasen50_01R.x
# Kasen50N	, ..\harupi\Pole\kasen50N_01S.x
PoleSL1		, ..\harupi\Pole\spole_et01L.x
PoleSR1		, ..\harupi\Pole\spole_et01R.x
Wall_HamaCon	, ..\harupi\Wall\Wall_HamaConL.x
C96_Wall1	, ..\harupi\Wall\C96_Wall1.x
Mesh02		, ..\harupi\Wall\Wiremesh02.x
# # vbgz[
FormL2		, ..\harupi\Form\FormL2.x
FormR2		, ..\harupi\Form\FormR2.x
FormCL2		, ..\harupi\Form\FormCL2.x
FormCR2		, ..\harupi\Form\FormCR2.x
FormLS2		, ..\harupi\Form\FormLS2.x
FormRS2		, ..\harupi\Form\FormRS2.x
FormCLS2	, ..\harupi\Form\FormCLS2.x
FormCRS2	, ..\harupi\Form\FormCRS2.x
# # ΞΚ
Tea1		, ..\harupi\Dike\Tea.x
Tea2		, ..\harupi\Dike\Tea2.x
# # ΄
Bridge_Amma	, ..\harupi\Rail\bridge_amma_1m.x
Bridge_Makuragi	, ..\harupi\Rail\bridge_Makuragi_2m.x
Bridge_TenryuN	, ..\harupi\Wall\bridge_tenryu_N62m.x
Bridge_TenryuK	, ..\harupi\Wall\bridge_tenryu_K62m.x
Bridge01	, ..\harupi\Rail\bridge_gata01.x
#; Bridge02, harupi\Rail\bridge_gata02.x
BridgeSGL0	, ..\harupi\Rail\bridge_AmiG0L.x
BridgeSGR0	, ..\harupi\Rail\bridge_AmiG0R.x
#; BridgeSGL1, harupi\Rail\bridge_AmiG1L.x
#; BridgeSGR1, harupi\Rail\bridge_AmiG1R.x
BridgeSRL0	, ..\harupi\Rail\bridge_AmiR0L.x
BridgeSRR0	, ..\harupi\Rail\bridge_AmiR0R.x
#; BridgePillar0, harupi\Pole\bridge_pillar0.x

TongBL1		, ..\..\Structures\strpack1\TongBL0.x
TongBL2		, ..\..\Structures\strpack1\TongBL1.x
TongBR1		, ..\..\Structures\strpack1\TongBR0.x
TongBR2		, ..\..\Structures\strpack1\TongBR1.x
TongFL1		, ..\..\Structures\strpack1\TongFL0.x
TongFL2		, ..\..\Structures\strpack1\TongFL1.x
TongFR1		, ..\..\Structures\strpack1\TongFR0.x
TongFR2		, ..\..\Structures\strpack1\TongFR1.x

# Tc312-1NA	, ..\harupi\Train\JRC313\Tc312-1NA.x
# Tc312-1NB	, ..\harupi\Train\JRC313\Tc312-1NB.x
# Tc312-1RA	, ..\harupi\Train\JRC313\Tc312-1RA.x
# Tc312-1RB	, ..\harupi\Train\JRC313\Tc312-1RB.x
# Tc312-3NA	, ..\harupi\Train\JRC313\Tc312-3NA.x
# Tc312-3NB	, ..\harupi\Train\JRC313\Tc312-3NB.x
# Tc312-3TA	, ..\harupi\Train\JRC313\Tc312-3TA.x
# Tc312-3TB	, ..\harupi\Train\JRC313\Tc312-3TB.x
# Tc312-3WA	, ..\harupi\Train\JRC313\Tc312-3WA.x
# Tc312-3WB	, ..\harupi\Train\JRC313\Tc312-3WB.x
# Tc312-4CA	, ..\harupi\Train\JRC313\Tc312-4CA.x
# Tc312-4CB	, ..\harupi\Train\JRC313\Tc312-4CB.x
# Tc312-5CA	, ..\harupi\Train\JRC313\Tc312-5CC.x
# Tc312-5CB	, ..\harupi\Train\JRC313\Tc312-5CB.x
Tc312-3NB	, ..\harupi\Train\New313\Tc312-3NB.x

# Mc313-1NA	, ..\harupi\Train\JRC313\Mc313-1NA.x
# Mc313-1RA	, ..\harupi\Train\JRC313\Mc313-1RA.x
# Mc313-3NA	, ..\harupi\Train\JRC313\Mc313-3NA.x
# Mc313-3TA	, ..\harupi\Train\JRC313\Mc313-3TA.x
# Mc313-3WA	, ..\harupi\Train\JRC313\Mc313-3WA.x
Mc313-3IB	, ..\harupi\Train\New313\Mc313-3IB.x
Mc313-3NB	, ..\harupi\Train\New313\Mc313-3NB.x

# T313-1NA	, ..\harupi\Train\JRC313\T313-1NA.x
# T313-1NB	, ..\harupi\Train\JRC313\T313-1NB.x
T313-3NA	, ..\harupi\Train\New313\T313-3NA.x

# M313-1NA	, ..\harupi\Train\JRC313\M313-1NA.x

# Mc313-3IB	, ..\harupi\Train\JRC313\Mc313-3IA.x
# M313-3IB	, ..\harupi\Train\JRC313\M313-3IA.x
# Tc312-3IB	, ..\harupi\Train\JRC313\Tc312-3IA.x

# DumpA		, ..\harupi\Train\JRC313\dump_A.x
# DumpB		, ..\harupi\Train\JRC313\dump_B.x

# # ’ΈΑΟ±7001
Ihs7101		,  ..\harupi\Train\IHS7000\IHS7102.x
Ihs7301		,  ..\harupi\Train\IHS7000\IHS7302.x
Ihs7501		,  ..\harupi\Train\IHS7000\IHS7502.x
# # ’ΈΑΟ±3101(Λσ)
Ihs3101		,  ..\harupi\Train\IHS7000\IHS3101.x
Ihs3102		,  ..\harupi\Train\IHS7000\IHS3102.x
Ihs3601		,  ..\harupi\Train\IHS7000\IHS3601.x
# # xθqHM
odorikohm	,  ..\harupi\Train\185Head\Ikusan.x


act		, ..\harupi\Building\act_tower.x

kam_toyoda01	, ..\harupi\Kamban\kamban_toyoda01.x
hodokyo01	, ..\harupi\obj\bridge_tenryu_hodokyo.x

# ΉHn
#ShintoumeiBridge1	, ..\harupi\Road\ShintoumeiBridge1.x
ShintoumeiBridge2	, ..\harupi\Road\ShintoumeiBridge2.x
ShintoumeiPier1		, ..\harupi\Road\ShintoumeiPier1.x
ShintoumeiPier2		, ..\harupi\Road\ShintoumeiPier2.x


// DLi _/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/
//glΦA(NT/fiv)
1go_c1_Potal1	, ..\..\Structures\NT\tunnel\1go_c1_Potal1.x
1go_c1_Potal2	, ..\..\Structures\NT\tunnel\1go_c1_Potal2.x
1go_c1_ent	, ..\..\Structures\NT\tunnel\1go_c1_ent.x
1go_c1_lp	, ..\..\Structures\NT\tunnel\1go_c1_lp.x
1go_c1_escL	, ..\..\Structures\NT\tunnel\1go_c1_escL.x
1go_c1_escR	, ..\..\Structures\NT\tunnel\1go_c1_escR.x
1go_c1_ext	, ..\..\Structures\NT\tunnel\1go_c1_ext.x
sdw_et_m	, ..\..\Structures\NT\tunnel\shadow\sdw_et_m.x
sdw_lp_m	, ..\..\Structures\NT\tunnel\shadow\sdw_lp_m.x
huku_c1_Potal1	, ..\..\Structures\NT\tunnel\huku_c1_Potal1.x
huku_c1_ent	, ..\..\Structures\NT\tunnel\huku_c1_ent.x
huku_c1_lp	, ..\..\Structures\NT\tunnel\huku_c1_lp.x
huku_c1_ext	, ..\..\Structures\NT\tunnel\huku_c1_ext.x

//kihonsenrohyou(NT/fiv)
Gp_DL		, ..\..\Structures\NT\kihonsenrohyou\gradient\Gp_DL.x
Gp_LD		, ..\..\Structures\NT\kihonsenrohyou\gradient\Gp_LD.x
Gp_UL		, ..\..\Structures\NT\kihonsenrohyou\gradient\Gp_UL.x
Gp_LU		, ..\..\Structures\NT\kihonsenrohyou\gradient\Gp_LU.x
Gp_DU		, ..\..\Structures\NT\kihonsenrohyou\gradient\Gp_DU.x
Gp_UD		, ..\..\Structures\NT\kihonsenrohyou\gradient\Gp_UD.x
Kilo_C0		, ..\..\Structures\NT\kihonsenrohyou\kilo\KP_A.x
Kilo_C1		, ..\..\Structures\NT\kihonsenrohyou\kilo\KP_C1.x
Kilo_C2		, ..\..\Structures\NT\kihonsenrohyou\kilo\KP_C2.x
Kilo_C3		, ..\..\Structures\NT\kihonsenrohyou\kilo\KP_C3.x
Kilo_C4		, ..\..\Structures\NT\kihonsenrohyou\kilo\KP_C4.x
Kilo_C5		, ..\..\Structures\NT\kihonsenrohyou\kilo\KP_B.x
Kilo_C6		, ..\..\Structures\NT\kihonsenrohyou\kilo\KP_C6.x
Kilo_C7		, ..\..\Structures\NT\kihonsenrohyou\kilo\KP_C7.x
Kilo_C8		, ..\..\Structures\NT\kihonsenrohyou\kilo\KP_C8.x
Kilo_C9		, ..\..\Structures\NT\kihonsenrohyou\kilo\KP_C9.x
Curve_P		, ..\..\Structures\NT\kihonsenrohyou\curve\Curve_P.x


//PPRn(μ©Έ³)
# 113TcA	, ..\..\Structures\113\113Tc01shizuoka.x
# 112M	, ..\..\Structures\113\112Mshizuoka.x
# 113M	, ..\..\Structures\113\113Mshizuoka.x
# 113TcB	, ..\..\Structures\113\113Tc02shizuoka.x
# 113M	, ..\..\Structures\113\112Mshizuoka.x


// 185n (InitG)
185_00		, ..\..\Structures\initg_ecset\185\185_00.x
185_01		, ..\..\Structures\initg_ecset\185\185_01.x
185_02		, ..\..\Structures\initg_ecset\185\185_02.x
185_03		, ..\..\Structures\initg_ecset\185\185_03.x
185_04		, ..\..\Structures\initg_ecset\185\185_04.x
185_05		, ..\..\Structures\initg_ecset\185\185_05.x
185_06		, ..\..\Structures\initg_ecset\185\185_06.x
185_07		, ..\..\Structures\initg_ecset\185\185_07.x
185_08		, ..\..\Structures\initg_ecset\185\185_08.x
shadow_e	, ..\..\Structures\initg_ecset\185\shadow_e.x


// E231{Γ bθ (aki)
E231_tcF	, ..\..\Structures\aki.e231.syonan\syonan.tcF.x
E231_tcFB	, ..\..\Structures\aki.e231.syonan\syonan.tcFB.x
E231_tcR	, ..\..\Structures\aki.e231.syonan\syonan.tcR.x
E231_tcRB	, ..\..\Structures\aki.e231.syonan\syonan.tcRB.x
E231_midA	, ..\..\Structures\aki.e231.syonan\syonan.midA.x
E231_midB	, ..\..\Structures\aki.e231.syonan\syonan.midB.x
E231_midMA	, ..\..\Structures\aki.e231.syonan\syonan.midMA.x
E231_midMB	, ..\..\Structures\aki.e231.syonan\syonan.midMB.x


// 373n (N209)
Mc373	, ..\..\Structures\N209\373\Mc373.x
T373	, ..\..\Structures\N209\373\T373.x
Tc372	, ..\..\Structures\N209\373\Tc372.x

// 211n (saha209 (ns[ό))
Mc211	, ..\..\Structures\saha\211\sh\211_t3.x
M211	, ..\..\Structures\saha\211\sh\211_t2.x
Tc210	, ..\..\Structures\saha\211\sh\211_t1.x

// έ¨ (RON)
EF210	, ..\..\Structures\RON\EF210\EF210-1.x
koki106	, ..\..\Structures\RON\koki106\koki106_a.x


// nagoya_common
# OΉn
BridgePillar	, ..\..\Nagoya_Common\Railway_Obj\Dike\Pier.x
# Λό
Pole0_2		, ..\..\Nagoya_Common\Railway_Obj\Pole\Pole0_2.x
Pole0_4		, ..\..\Nagoya_Common\Railway_Obj\Pole\Pole0_1.x
Pole1_0		, ..\..\Nagoya_Common\Railway_Obj\Pole\Pole1_0.x
Pole1_3		, ..\..\Nagoya_Common\Railway_Obj\Pole\Pole1_3.x
Pole2_0		, ..\..\Nagoya_Common\Railway_Obj\Pole\Pole2_0.x
Pole2_3		, ..\..\Nagoya_Common\Railway_Obj\Pole\Pole2_3.x
Pole3_0		, ..\..\Nagoya_Common\Railway_Obj\Pole\Pole3_0.x
Pole4_0		, ..\..\Nagoya_Common\Railway_Obj\Pole\Pole4_0.x
Pole5_0		, ..\..\Nagoya_Common\Railway_Obj\Pole\Pole5_0.x
CusPole0L	, ..\..\Nagoya_Common\Railway_Obj\Pole\PoleL_0.x
CusPole0R	, ..\..\Nagoya_Common\Railway_Obj\Pole\PoleR_0.x
CusPole0C	, ..\..\Nagoya_Common\Railway_Obj\Pole\PoleC_1.x
# ΉH
Road01		, ..\..\Nagoya_Common\Build\Road\Road1_nonblock.x
Road02		, ..\..\Nagoya_Common\Build\Road\Road2_nonblock.x
TomeiExpwy	, ..\..\Nagoya_Common\Build\Over\mei\R21.x
OverRoad01	, ..\..\Nagoya_Common\Build\Over\01.x
Fumikiri_AL	, ..\..\Nagoya_Common\Railway_Obj\Cross\Normal\DL.x
Fumikiri_AR	, ..\..\Nagoya_Common\Railway_Obj\Cross\Normal\DR.x
Fumikiri_AC	, ..\..\Nagoya_Common\Railway_Obj\Cross\Normal\DC.x
Fumikiri_AH	, ..\..\Nagoya_Common\Railway_Obj\Cross\Normal\DCL.x
Fumikiri_AM	, ..\..\Nagoya_Common\Railway_Obj\Cross\Normal\DCR.x
Fumikiri_BL	, ..\..\Nagoya_Common\Railway_Obj\Cross\Normal\SL.x
Fumikiri_BR	, ..\..\Nagoya_Common\Railway_Obj\Cross\Normal\SR.x
Fumikiri_BC	, ..\..\Nagoya_Common\Railway_Obj\Cross\Normal\SC.x
Fumikiri_BH	, ..\..\Nagoya_Common\Railway_Obj\Cross\Normal\SCL.x
Fumikiri_BM	, ..\..\Nagoya_Common\Railway_Obj\Cross\Normal\SCR.x
HodokyoSL	, ..\..\Nagoya_Common\Build\Footbridge\StairsL.x
HodokyoSR	, ..\..\Nagoya_Common\Build\Footbridge\StairsR.x
HodokyoCL	, ..\..\Nagoya_Common\Build\Footbridge\L.x
HodokyoCR	, ..\..\Nagoya_Common\Build\Footbridge\R.x
Fumikiri_GL	, ..\..\Nagoya_Common\Railway_Obj\Cross\General\gate\GateL.x
Fumikiri_GR	, ..\..\Nagoya_Common\Railway_Obj\Cross\General\gate\GateR.x
Fumikiri_450L	, ..\..\Nagoya_Common\Railway_Obj\Cross\General\gate\Gate450L.x
Fumikiri_450R	, ..\..\Nagoya_Common\Railway_Obj\Cross\General\gate\Gate450R.x
# nΚ
Levee		, ..\..\Nagoya_Common\Nature\Ground\Levee.x
River		, ..\..\Nagoya_Common\Nature\Ground\River.x
Con10UNL	, ..\..\Nagoya_Common\Nature\Ground\Concrete_10UNL.x
Con10UNR	, ..\..\Nagoya_Common\Nature\Ground\Concrete_10UNR.x
GrassUNL	, ..\..\Nagoya_Common\Nature\Ground\Grass_10UNL.x
GrassUNR	, ..\..\Nagoya_Common\Nature\Ground\Grass_10UNR.x
#
DikeL2		, ..\..\Nagoya_Common\Railway_Obj\Dike\DikeL.x
DikeR2		, ..\..\Nagoya_Common\Railway_Obj\Dike\DikeR.x
DikeL3		, ..\..\Nagoya_Common\Railway_Obj\Dike\DikeCoL.x
DikeR3		, ..\..\Nagoya_Common\Railway_Obj\Dike\DikeCoR.x
# ΰθΰθX¨O
#; Grove00	, ..\Nagoya_Common\Nature\Wood\Grove00.x
Grove01		, ..\..\Nagoya_Common\Nature\Wood\Grove01.x
Grove02		, ..\..\Nagoya_Common\Nature\Wood\Grove02.x
Grove03		, ..\..\Nagoya_Common\Nature\Wood\Grove03.x
Grove04		, ..\..\Nagoya_Common\Nature\Wood\Grove04.x
Wood00		, ..\..\Nagoya_Common\Nature\Wood\Wood00.x
Wood01		, ..\..\Nagoya_Common\Nature\Wood\Wood01.x
Wood02		, ..\..\Nagoya_Common\Nature\Wood\Wood02.x
Wood03		, ..\..\Nagoya_Common\Nature\Wood\Wood03.x
Wood04		, ..\..\Nagoya_Common\Nature\Wood\Wood04.x
Wood05		, ..\..\Nagoya_Common\Nature\Wood\Wood05.x
Wood06		, ..\..\Nagoya_Common\Nature\Wood\Wood06.x
# M
SignalB		, ..\..\Nagoya_Common\Railway_Obj\Signal\Back\SignalB_1.x
# NρΏΑΔΗρΘΖH
HouseN028	, ..\..\Nagoya_Common\Build\house\28.x
House0R34	, ..\..\Nagoya_Common\Build\house\Model\House0_R_34.x
House1L04	, ..\..\Nagoya_Common\Build\house\Model\House1_04.x
House2L02	, ..\..\Nagoya_Common\Build\house\Model\House2_L_02.x
House3L03	, ..\..\Nagoya_Common\Build\house\Model\House3_L_03.x
House4L00	, ..\..\Nagoya_Common\Build\house\Model\House4_00.x
Mansion03	, ..\..\Nagoya_Common\Build\Mansion\03.x
Mansion07	, ..\..\Nagoya_Common\Build\Mansion\07.x
# R
Mt		, ..\..\Nagoya_Common\Nature\Mt\Mt.x
Mt_L		, ..\..\Nagoya_Common\Nature\Mt\Mt_L.x
Mt_R		, ..\..\Nagoya_Common\Nature\Mt\Mt_R.x
Mt_Wall10a	, ..\..\Nagoya_Common\Nature\Mt\Mt_wall_10a.x


#// κqΨiγ5 xμdΤj
GNR_BR0			, ..\..\Structures\root5GNR\0-1\BR0.x
GNR_numakawa0		, ..\..\Structures\root5GNR\0-1\numakawa0.x
GNR_DikeR0		, ..\..\Structures\root5GNR\Scenery\DikeR0.x
GNR_Dikeend-numakawa	, ..\..\Structures\root5GNR\Scenery\Dikeend-numakawa.x
GNR_Parking,		, ..\..\Structures\root5GNR\Scenery\parking.x
GNR_Pier		, ..\..\Structures\root5GNR\Scenery\Pier.x
GNR_Bank0		, ..\..\Structures\root5GNR\Scenery\Bank0.x
GNR_Bank0F		, ..\..\Structures\root5GNR\Scenery\Bank0F.x
GNR_T00			, ..\..\Structures\root5GNR\Factory\T00.x
GNR_T1			, ..\..\Structures\root5GNR\Scenery\T1.x
GNR_T2			, ..\..\Structures\root5GNR\Scenery\T2.x
GNR_Cement		, ..\..\Structures\root5GNR\0-1\Cement.x
GNR_F00			, ..\..\Structures\root5GNR\Factory\F00.x
GNR_p0			, ..\..\Structures\root5GNR\0-1\p0.x
GNR_p1			, ..\..\Structures\root5GNR\0-1\p1.x
GNR_p2			, ..\..\Structures\root5GNR\0-1\p2.x
GNR_Chip		, ..\..\Structures\root5GNR\0-1\Chip.x
GNR_Food		, ..\..\Structures\root5GNR\0-1\Food.x
GNR_Food00		, ..\..\Structures\root5GNR\Factory\Food00.x
GNR_HT2			, ..\..\Structures\root5GNR\Factory\HT2.x

# κqΨ
CMTNWallL11	, ..\..\Structures\ChiMari(TN)-BaseObject\Wall\WallL2.x
CMTNWallR11	, ..\..\Structures\ChiMari(TN)-BaseObject\Wall\WallR2.x


#CA03		, ..\harupi\Ground\CA03.x
#CA03b		, ..\harupi\Ground\CA03b.x
#CA03c		, ..\harupi\Ground\CA03c.x
