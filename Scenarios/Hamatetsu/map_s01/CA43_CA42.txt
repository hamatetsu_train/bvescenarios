﻿BveTs Map 2.00

//////////////////////////////////////////////////////////////////////////
// 東海道本線（東海道線）CA43 西小坂井 ～ CA42 豊橋
// (297.900) ～ (297.200)
//////////////////////////////////////////////////////////////////////////


// -------------------------------------------------------------------------------------------------------------

$CAWay = $kozakai + 420; // 298km

$CAWay + 100;
	Track['10'].X.Interpolate(3.80, 0); // 下本
	Repeater['Rail10L'].Begin('10', -0.5335, 0, 0, 0, 0, 0, 3, 5.0, 5.0, 'RailL0', 'RailL1', 'RailL2', 'RailL3', 'RailL4');
	Repeater['Rail10R'].Begin('10',  0.5335, 0, 0, 0, 0, 0, 3, 5.0, 5.0, 'RailR0', 'RailR1', 'RailR2', 'RailR3', 'RailR4');
	Repeater['Rail10u'].Begin('10', 0, 0, 0, 0, 0, 0, 3, 5, 5, 'Ballast0', 'Ballast1', 'Ballast2', 'Ballast3', 'Ballast4');
	Repeater['Kasen10'].Begin('10', 0, 0, 0, 0, 0, 0, 1, 25, 25, 'KasenS0');



$CAWay + 160;
	// Rrx - TCLx - CCLx - Cx
		$dist = distance;
		$radius=1800;
		$cant=50;
	include 'utility\CurveStart.txt';


$CAWay + 200;
	//━ 0.0‰
		$dist = distance;
	include 'utility\GraDownEnd.txt';


$CAWay + 320;
	//┛ 8.0‰
		$dist = distance;
		$Slope = 8.0;
	include 'utility\GraUpStart.txt';


$CAWay + 330;
	//※◯●●下場 (297.670)


$CAWay + 342;
	//＠ 坂田踏切 (297.658)
		$dist = distance;
	include 'utility\CrossA2.txt';


$CAWay + 350;
	Track['Height'].Y.Interpolate(-1);
	Repeater['Dike00L'].Begin('00', 0, 0, 0, 0, 0, 0, 3, 10.0, 10.0, 'DikeL0');
	Repeater['Dike10R'].Begin('10', 0, 0, 0, 0, 0, 0, 3, 10.0, 10.0, 'DikeR0');
	Repeater['Dike11R'].Begin('10', 3.7, -3.0, 0, 0, 0, 0, 3, 10.0, 10.0, 'DikeR0');


$CAWay + 600;
	$RailNo='42';
	Track[$RailNo].Position(-91.50, 0, -900, 0); // 飯上(2)
	Repeater['Rail'+$RailNo+'u'].Begin($RailNo, 0, 0, 0, 0, 0, 0, 3, 5, 5, 'Ballast0', 'Ballast1', 'Ballast2', 'Ballast3', 'Ballast4');
	Repeater['Kasen'+$RailNo].Begin($RailNo, 0, 0, 0, 0, 0, 0, 1, 25, 25, 'KasenS0');
		$dist = distance;
	include 'utility\TongStartR.txt';

	Repeater['Dike42R'].Begin('42', 0, 0, 0, 0, 0, 0, 3, 10.0, 10.0, 'DikeR0');

	$RailNo='43';
	Track[$RailNo].Position(-87.50, -3.00, -450, 0); // 飯下(3)
	Repeater['Rail'+$RailNo+'u'].Begin($RailNo, 0, 0, 0, 0, 0, 0, 3, 5, 5, 'Ballast0', 'Ballast1', 'Ballast2', 'Ballast3', 'Ballast4');
	Repeater['Kasen'+$RailNo].Begin($RailNo, 0, 0, 0, 0, 0, 0, 1, 25, 25, 'KasenS0');
		$dist = distance;
	include 'utility\TongStartR.txt';

	Repeater['Dike43L'].Begin('43', 0, 0, 0, 0, 0, 0, 3, 10.0, 10.0, 'DikeL0');
	Repeater['Dike43R'].Begin('43', 0, 0, 0, 0, 0, 0, 3, 10.0, 10.0, 'DikeR0');


$CAWay + 650;
	// R23小坂井BP
	Structure['CMTN_KozakaiBP'].Put('00', -115.0, 0, 0, 0, 0, 0, 3, 1.0);


$CAWay + 700;
	// Rrx - TCLx - CCLx - Cx
		$dist = distance;
	include 'utility\CurveEnd.txt';

	Track['42'].Position(-85.90, 0, 0, 0); // 飯上(2)
	Track['43'].Position(-9.10, 0, -600, 0); // 飯下(3)

	Repeater['Dike43R'].End();
	Repeater['Dike00L'].End();


$CAWay + 750;
	Repeater['EnePole00'].Begin('43', 0.00, 0, 0, 0, 0, 0, 0, 1.0, 50.0, 'Pole2_0');


$CAWay + 770;
	//━ 0.0‰
		$dist = distance;
	include 'utility\GraUpEnd.txt';


$CAWay + 780;
	Track['43'].X.Interpolate(-3.80, 0); // 飯下(3)


$CAWay + 800;
	Track['Height'].Y.Interpolate(-6);



// -------------------------------------------------------------------------------------------------------------



// 豊川放水路 (152)

$CAWay + 825; // 15025
	Repeater['Ground'].Begin('Height',  0, 0, 0, 0, 0, 0, 1, 10, 10, 'River');
	Structure['CMTN_Teibou'].Put('00', 0.00, 0.00, 0.00, 0, 0, 0, 3, 1.0);
	//Repeater['豊川放水路'].Begin('10', 0.00, 0.00, 0.00, 0, 0, 0, 3, 50.0, 50.0, 'Toyo_Bridge0');

	Track['42'].X.Interpolate(-73.20, 0); // 飯上(2)

	Repeater['Rail00u'].End();
	Repeater['Rail10u'].End();
	Repeater['Rail43u'].End();
	Repeater['Bridge00'].Begin('00', 0, 0, 0, 0, 0, 0, 3, 25, 25, 'CMTN_BridgeL1');
	Repeater['Bridge10'].Begin('10', 0, 0, 0, 0, 0, 0, 3, 25, 25, 'CMTN_BridgeL1');
	Repeater['Bridge42'].Begin('42', 0, 0, 0, 0, 0, 0, 3, 25, 25, 'CMTN_BridgeL1');
	Repeater['Bridge43'].Begin('43', 0, 0, 0, 0, 0, 0, 3, 25, 25, 'CMTN_BridgeL1');
	Repeater['EnePole00'].End();

	Repeater['Dike42R'].End();
	Repeater['Dike43L'].End();
	Repeater['Dike10R'].End();
	Repeater['Dike11R'].End();


$CAWay + 850;
	Repeater['Toyokawa1'].Begin('00', 0, 0, 0, 0, 0, 0, 3, 25, 50, 'CMTN_Toyokawa1');
	Repeater['Toyokawa2'].Begin('42', 0, 0, 0, 0, 0, 0, 3, 25, 50, 'CMTN_Toyokawa2');
	//Repeater['Toyokawa3'].Begin('42', 0, 0, 0, 0, 0, 0, 3, 25, 25, 'CMTN_Toyokawa3');


$CAWay + 975;
	Repeater['Ground'].Begin('Height',  0, 0, 0, 0, 0, 0, 1, 25, 25, 'Grass');
	Structure['CMTN_Teibou'].Put('00', 0.00, 0.00, 0.00, 0, 0, 0, 3, 1.0);

	Repeater['Rail00u'].Begin('00', 0, 0, 0, 0, 0, 0, 3, 5, 5, 'Ballast0', 'Ballast1', 'Ballast2', 'Ballast3', 'Ballast4');
	Repeater['Rail10u'].Begin('10', 0, 0, 0, 0, 0, 0, 3, 5, 5, 'Ballast0', 'Ballast1', 'Ballast2', 'Ballast3', 'Ballast4');
	Repeater['Rail43u'].Begin('43', 0, 0, 0, 0, 0, 0, 3, 5, 5, 'Ballast0', 'Ballast1', 'Ballast2', 'Ballast3', 'Ballast4');

	//Repeater['豊川放水路'].End();
	Repeater['Bridge00'].End();
	Repeater['Bridge10'].End();
	Repeater['Bridge42'].End();
	Repeater['Bridge43'].End();
	Repeater['Toyokawa1'].End();
	Repeater['Toyokawa2'].End();
	//Repeater['Toyokawa3'].End();


$CAWay + 980;
	Repeater['Dike42R'].Begin('42', 0, 0, 0, 0, 0, 0, 3, 10.0, 10.0, 'DikeR0');


$CAWay + 1000;
	Track['Height'].Y.Interpolate(-6);
	//┓-8.0‰
		$dist = distance;
		$slope=-8.0;
	include 'utility\GraDownStart.txt';

	Repeater['EnePole00'].Begin('43', 0.00, 0, 0, 0, 0, 0, 0, 1.0, 50.0, 'Pole2_0');


$CAWay + 1130;
	// Rrx - TCLx - CCLx - Cx
		$dist = distance;
		$radius=2000;
		$cant=50;
	include 'utility\CurveStart.txt';

	Track['42'].X.Interpolate(-42.90, 2000); // 飯上(2)


$CAWay + 1230;
	Track['42'].X.Interpolate(-30.20, -2000); // 飯上(2)


$CAWay + 1245;
	//◯●●上2 (296.755)
	Section.BeginNew(0, 2, 4);
	//Structure['PoleSL1'].Put(0, -2.6, 0, 0.3, 0, 0, 0, 1, 1.0);
	Signal['type3'].Put(0, 0, -0.6, 6.8, 0.0, 0, 0, 0, 1, 1.0);


$CAWay + 1500;
	Repeater['EnePole00'].Begin('42', 0.00, 0, 0, 0, 0, 0, 0, 1.0, 50.0, 'Pole3_0');
	Repeater['Dike42R'].End();


$CAWay + 1510;
	Track['Height'].Y.Interpolate(-1.0);
	//━ 0.0‰
		$dist = distance;
	include 'utility\GraDownEnd.txt';


$CAWay + 1530;
	// Rrx - TCLx - CCLx - Cx
		$dist = distance;
	include 'utility\CurveEnd.txt';

	Track['42'].X.Interpolate(-7.60, 0); // 飯上(2)


$CAWay + 1600;
	Repeater['EnePole00'].Begin('43', 0.00, 0, 0, 0, 0, 0, 0, 1.0, 50.0, 'Pole2_0');


$CAWay + 1780;
	Track['Height'].Y.Interpolate(-1.0);
	//┛ 9.0‰
		$dist = distance;
		$Slope = 9.0;
	include 'utility\GraUpStart.txt';


$CAWay + 1870;
	// Rrx - TCLx - CCLx - Cx
		$dist = distance;
		$radius=1600;
		$cant=50;
	include 'utility\CurveStart.txt';

	Track['42'].X.Interpolate(-7.60, -1600); // 飯上(2)
	Track['43'].X.Interpolate(-3.80, -1600); // 飯下(3)


$CAWay + 1900;
	Structure['CrackR0'].PutBetween('43', '00');


$CAWay + 1925;
	Structure['CrackR0'].PutBetween('43', '00');


$CAWay + 1950;
	Repeater['EnePole00'].Begin('00', 0.00, 0, 0, 0, 0, 0, 0, 1.0, 50.0, 'Pole1_0');
	Structure['CrackR0'].PutBetween('43', '00');


$CAWay + 1975;
	Structure['CrackR0'].PutBetween('43', '00');


$CAWay + 2000;
	Structure['CrackR0'].PutBetween('43', '00');


$CAWay + 2020;
	Track['42'].X.Interpolate(-7.60-6.04, -651); // 飯上(2)
	Track['43'].X.Interpolate(-3.80-6.04, 3520); // 飯下(3)


$CAWay + 2025;
	Structure['CrackR0'].PutBetween('43', '00');


$CAWay + 2030;
	// Rrx - TCLx - CCLx - Cx
		$dist = distance;
		$radius=-1600;
		$cant=-50;
	include 'utility\CurveStart.txt';

	Track['42'].X.Interpolate(-7.60-0.18-7.05, -3520); // 飯上(2)
	Track['43'].X.Interpolate(-3.80+0.18-7.05,   651); // 飯下(3)


$CAWay + 2050;
	Structure['CrackR0'].PutBetween('42', '43');
	Structure['CrackR0'].PutBetween('43', '00');


$CAWay + 2075;
	Structure['CrackR0'].PutBetween('42', '43');
	Structure['CrackR0'].PutBetween('43', '00');


$CAWay + 2085;
	Track['42'].X.Interpolate(-7.60-1.90-12.20, 1600); // 飯上(2)
	Track['43'].X.Interpolate(-3.80+1.90-12.20, 1600); // 飯下(3)


$CAWay + 2100;
	Structure['CrackR0'].PutBetween('42', '43');
	Structure['CrackR0'].PutBetween('43', '00');


$CAWay + 2125;
	Structure['CrackR0'].PutBetween('42', '43');
	Structure['CrackR0'].PutBetween('43', '00');


$CAWay + 2145;
	Track['42'].X.Interpolate(-7.60-5.70-15.80,   651); // 飯上(2)
	Track['43'].X.Interpolate(-3.80+5.70-15.80, -3520); // 飯下(3)


$CAWay + 2150;
	Structure['CrackR0'].PutBetween('42', '43');
	Structure['CrackR0'].PutBetween('43', '00');


$CAWay + 2175;
	Structure['CrackR0'].PutBetween('42', '43');
	Structure['CrackR0'].PutBetween('43', '00');


$CAWay + 2200;
	Structure['CrackR0'].PutBetween('42', '43');
	Structure['CrackR0'].PutBetween('43', '00');


$CAWay + 2210;
	// Rrx - TCLx - CCLx - Cx
		$dist = distance;
	include 'utility\CurveEnd.txt';

	Track['42'].X.Interpolate(-7.60-7.60-17.10, 0); // 飯上(2)
	Track['43'].X.Interpolate(-3.80+7.60-17.10, 0); // 飯下(3)


$CAWay + 2220;
	//━ 0.0‰
		$dist = distance;
	include 'utility\GraDownEnd.txt';


// 豊川 (249)
$CAWay + 2225;
	Track['Height'].Y.Interpolate(-6.2);
	Repeater['Ground'].Begin('Height',  0, 0, 0, 0, 0, 0, 1, 10, 10, 'River');
	//Structure['CMTN_Teibou'].Put('00', 0.00, 0.00, 0.00, 0, 0, 0, 3, 1.0);

	Repeater['Rail00u'].End();
	Repeater['Rail10u'].End();
	Repeater['Rail43u'].End();
	Repeater['Rail42u'].End();
	Repeater['Bridge00'].Begin('00', 0, 0, 0, 0, 0, 0, 3, 25, 25, 'CMTN_BridgeL1');
	Repeater['Bridge10'].Begin('10', 0, 0, 0, 0, 0, 0, 3, 25, 25, 'CMTN_BridgeL1');
	Repeater['Bridge42'].Begin('42', 0, 0, 0, 0, 0, 0, 3, 25, 25, 'CMTN_BridgeL1');
	Repeater['Bridge43'].Begin('43', 0, 0, 0, 0, 0, 0, 3, 25, 25, 'CMTN_BridgeL1');
	//Repeater['EnePole00'].End();
	Repeater['Toyokawa1'].Begin('00', 0, -0.2, 0, 0, 0, 0, 3, 25, 25, 'CMTN_PassageGLL');
	Repeater['Toyokawa2'].Begin('10', 0, -0.2, 0, 0, 0, 0, 3, 25, 25, 'CMTN_PassageGLR');
	Repeater['Toyokawa3'].Begin('42', 0, -0.2, 0, 0, 0, 0, 3, 25, 25, 'CMTN_PassageGLR');
	Repeater['Toyokawa4'].Begin('43', 0, -0.2, 0, 0, 0, 0, 3, 25, 25, 'CMTN_PassageGLR');
	Repeater['Toyokawa5'].Begin('00', 0, 0, 0, 0, 0, 0, 3, 25, 25, 'CMTN_Toyokawa3');


$CAWay + 2475;
	Track['Height'].Y.Interpolate(-6.2);
	Repeater['Ground'].Begin('Height',  0, 0, 0, 0, 0, 0, 1, 25, 25, 'Grass');
	Structure['CMTN_Teibou'].Put('00', 0.00, 0.00, 0.00, 0, 0, 0, 3, 1.0);

	Repeater['Rail00u'].Begin('00', 0, 0, 0, 0, 0, 0, 3, 5, 5, 'Ballast0', 'Ballast1', 'Ballast2', 'Ballast3', 'Ballast4');
	Repeater['Rail10u'].Begin('10', 0, 0, 0, 0, 0, 0, 3, 5, 5, 'Ballast0', 'Ballast1', 'Ballast2', 'Ballast3', 'Ballast4');
	Repeater['Rail42u'].Begin('42', 0, 0, 0, 0, 0, 0, 3, 5, 5, 'Ballast0', 'Ballast1', 'Ballast2', 'Ballast3', 'Ballast4');
	Repeater['Rail43u'].Begin('43', 0, 0, 0, 0, 0, 0, 3, 5, 5, 'Ballast0', 'Ballast1', 'Ballast2', 'Ballast3', 'Ballast4');

	Repeater['Bridge00'].End();
	Repeater['Bridge10'].End();
	Repeater['Bridge42'].End();
	Repeater['Bridge43'].End();
	Repeater['Toyokawa1'].End();
	Repeater['Toyokawa2'].End();
	Repeater['Toyokawa3'].End();
	Repeater['Toyokawa4'].End();
	Repeater['Toyokawa5'].End();


$CAWay + 2500;
	//┓-9.1‰
		$dist = distance;
		$slope=-9.1;
	include 'utility\GraDownStart.txt';

	Track['42'].X.Interpolate(-7.60-7.60-17.10, 0); // 飯上(2)
	Track['43'].X.Interpolate(-3.80+7.60-17.10, 0); // 飯下(3)


$CAWay + 2508;
	//◯●●上1 (295.492)
	Section.BeginNew(0, 2, 4);
	Structure['PoleSL1'].Put(0, -2.6, 0, 0.3, 0, 0, 0, 1, 1.0);
	Signal['type3'].Put(0, 0, -2.6, 4.3, 0.0, 0, 0, 0, 1, 1.0);


$CAWay + 2510;
	// Rrx - TCLx - CCLx - Cx
		$dist = distance;
		$radius=-1600;
		$cant=-50;
	include 'utility\CurveStart.txt';

	Track['42'].X.Interpolate(-7.60-7.60-17.10,  1600); // 飯上(2)
	Track['43'].X.Interpolate(-3.80+7.60-17.10, -3520); // 飯下(3)


$CAWay + 2630;
	Track['43'].X.Interpolate(-3.80+1.60-13.60, 651); // 飯下(3)


$CAWay + 2700;
	// Rrx - TCLx - CCLx - Cx
		$dist = distance;
		$radius=1600;
		$cant=50;
	include 'utility\CurveStart.txt';

	Track['42'].X.Interpolate(-7.60-7.60-8.55, -1600); // 飯上(2)
	Track['43'].X.Interpolate(-3.80+0.72-12.70, 3520); // 飯下(3)


$CAWay + 2760;
// 船町


$CAWay + 2760;
	Track['43'].X.Interpolate(-3.80-6.60, -1600); // 飯下(3)


$CAWay + 2920;
	// Rrx - TCLx - CCLx - Cx
		$dist = distance;
	include 'utility\CurveEnd.txt';

	Track['42'].X.Interpolate(-11.40, 950); // 飯上(2)
	Track['43'].X.Interpolate( -3.80, 0); // 飯下(3)


$CAWay + 2980;
	Track['42'].X.Interpolate(-9.50, -950); // 飯上(2)


$CAWay + 3040;
	Track['42'].X.Interpolate(-7.60, 0); // 飯上(2)


$CAWay + 3140;
	//━ 0.0‰
		$dist = distance;
	include 'utility\GraDownEnd.txt';


$CAWay + 3250;
	Track['Height'].Y.Interpolate(-1.0);


$CAWay + 3254;
	//＠ 宮本踏切 (294.746)
		$dist = distance;
	include 'utility\CrossA2.txt';


$CAWay + 3360;
	//┛ 10.0‰
		$dist = distance;
		$Slope = 10.0;
	include 'utility\GraUpStart.txt';



// -------------------------------------------------------------------------------------------------------------


// (C)Harupi
