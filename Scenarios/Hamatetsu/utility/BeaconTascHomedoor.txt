﻿BveTs Map 2.00

//////////////////////////////////////////////////////////////////////////
// utility/BeaconTascHomedoor
// TASC用地上子 / ホームドア
//////////////////////////////////////////////////////////////////////////

// 引数 //////
// ($location : 停止位置[m])
// $LastBcn : 最遠地上子との距離[m]
// $DoorLen : 停止位置とホームドア許容誤差[m]
/////////////

//$location = distance;
$location = $dist;

// [P1][HD]
$location - $LastBcn;
	Beacon.Put(120, 1, $LastBcn);
	Beacon.Put(122, 1, 1);
	Structure['Beacon_p'].Put(0, 0, 0, 0, 0, 0, 0, 3, 0);


// [P2]
$location - 200;
	Beacon.Put(120, 1, 200);
	Structure['Beacon_p'].Put(0, 0, 0, 0, 0, 0, 0, 3, 0);


// [P3]
$location - 50;
	Beacon.Put(120, 1, 50);
	Structure['Beacon_p'].Put(0, 0, 0, 0, 0, 0, 0, 3, 0);


// [P4]
$location - 4;
	Beacon.Put(121, 1, 400);
	Structure['Beacon_p'].Put(0, 0, 0, 0, 0, 0, 0, 3, 0);


// [HD]
$location - $DoorLen;
	Beacon.Put(122, 1, 2);


// [HD]
$location + $DoorLen;
	Beacon.Put(122, 1, 0);



////////////////////////////////////////////////////////////////

