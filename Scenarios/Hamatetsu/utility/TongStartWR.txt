﻿BveTs Map 2.00

//////////////////////////////////////////////////////////////////////////
// utility/TongStartWR
// 自身が右に分岐していく
//////////////////////////////////////////////////////////////////////////

// 引数 //////
// ($dist : 開始点[m])
// $RailNo : 軌道名
/////////////

//$location = distance;
$location = $dist;

$location + 0;
	Structure['TongFR1'].Put($RailNo, -0.7175, 0, 0, 0, 0, 0, 3, 5.0);
	Repeater['Rail'+$RailNo+'R'].Begin($RailNo,  0.7175, 0, 0, 0, 0, 0, 3, 5.0, 5.0, 'RailR0', 'RailR1', 'RailR2', 'RailR3', 'RailR4');

$location + 5.0;
	Structure['TongFR2'].Put($RailNo, -0.7175, 0, 0, 0, 0, 0, 3, 5.0);

$location + 10.0;
	Repeater['Rail'+$RailNo+'L'].Begin($RailNo, -0.7175, 0, 0, 0, 0, 0, 3, 5.0, 5.0, 'RailL2', 'RailL3', 'RailL4', 'RailL0', 'RailL1');

////////////////////////////////////////////////////////////////
