﻿BveTs Map 2.00

//////////////////////////////////////////////////////////////////////////
// utility/TongEndR
// 自身が右から合流していく
//////////////////////////////////////////////////////////////////////////

// 引数 //////
// ($dist : 終了点[m])
// $RailNo : 軌道名
/////////////

//$location = distance;
$location = $dist;

$location - 10.0;
	Repeater['Rail'+$RailNo+'L'].End();
	Structure['TongBR1'].Put($RailNo, -0.5335, 0, 0, 0, 0, 0, 3, 5.0);

$location - 5.0;
	Structure['TongBR2'].Put($RailNo, -0.5335, 0, 0, 0, 0, 0, 3, 5.0);

$location + 0;
	Repeater['Rail'+$RailNo+'R'].End();

////////////////////////////////////////////////////////////////
