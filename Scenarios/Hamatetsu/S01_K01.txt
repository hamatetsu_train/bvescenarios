﻿BveTs Map 2.00

//////////////////////////////////////////////////////////////////////////
// S01_K01
// 東海道線 普通 熱海(CA00)[2] ～ 掛川(CA27)
//////////////////////////////////////////////////////////////////////////

// 路線基本ファイル
	include 'map_s01\Map_S01.txt';

// ATS-S

//////////////////////////////////////////////////////////////////////////
// 閉塞初期値代入
$SecA = 3996;
$SecB = 3996;
$Slope2=0;
//////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////
$Atami=2000; // 104570
	include 'map_s01\CA00_K02.txt';
	include 'map_s01\CA00_CA02.txt';

$Mishima=$Atami+16170; // 120740
	include 'map_s01\CA02_K02.txt';
	include 'map_s01\CA02_CA03.txt';

$Numazu=$Mishima+5490; // 126230
	include 'map_s01\CA03_K02.txt';
	include 'map_s01\CA03_CA08.txt';
	include 'map_s01\Plus_S01K01_Scenery_CA03_CA08.txt';

$Fuji=$Numazu+19970; // 146160
	include 'map_s01\CA08_K02.txt';
	include 'map_s01\CA08_CA13.txt';
	include 'map_s01\Plus_S01K01_Scenery_CA08_CA13.txt';

$Okitsu=$Fuji+18090; // 164270
	include 'map_s01\CA13_K01.txt';
	include 'map_s01\CA13_CA14.txt';
	include 'map_s01\CA14_K01.txt';


//////////////////////////////////////////////////////////////////////////
// 駅ファイル
	Station.Load('map_s01\Sub_S01K01_Stations.txt');
// 停止目標
	include 'map_s01\Sub_S01K01_Stops.txt';
// 閉塞
	include 'map_s01\Sub_S01K01_BeaconsPT.txt';
// 曲線
	//include 'map_s02\Sub_S01K01_Curve.txt';
// 他列車
	//include 'map_s02\Sub_S01K01_Trains.txt';



// (C)Harupi
