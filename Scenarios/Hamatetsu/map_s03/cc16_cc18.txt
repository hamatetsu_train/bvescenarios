﻿BveTs Map 2.00

//////////////////////////////////////////////////////////////////////////
// 42515 - 49740  map_s03/cc16_cc18
// 身延線 身延(CC16) 下本出 ～ 波高島(CC18) 場内
//////////////////////////////////////////////////////////////////////////

// ※駅中心　43K160
$CCWay = $Minobu+840;


//////////////////////////////////////////////////////////////////////////
$CCWay-485;



$CCWay-480;
	Track['r10'].Position(-40.00, -1.00, 0, 0); // r10
	Repeater['Roadr10'].End();
	Track['c02'].Position(-40.00, -1.00, 0, 0); // 市道
	Repeater['Roadc02'].Begin('c02', 0.00, 0.00, 0.00, 0, 0, 0, 3, 10.0, 10.0, 'Road01');
	Track['c01'].Position(8.00, 1.00, 0, 0); // 市道


$CCWay-450;
	Repeater['EnePole00'].Begin(0, 0, 0, 0, 0, 180, 0, 0, 25.0, 25.0, 'Pole0_4');
//
		$dist = distance;
		$radius=300;
		$cant=40;
	include 'utility\CurveStart.txt';


$CCWay-400;
	Track['Mt_R'].Position(130.0, 0, 0, 0);
	Repeater['Mt_R'].Begin('Mt_R', 3.0, -5.0, 0, 0, 0, 0, 0, 200.0, 25.0, 'Mt_R');


$CCWay-370;
//
	Track['c02'].Position(-8.00, -0.50, 0, 0); // 市道
	Track['c01'].Position(8.00, 1.00, 0, 0); // 市道


$CCWay-321;
		$dist = distance;
	include 'utility\CurveEnd.txt';


$CCWay-320;
// [ □ 】
	Structure['SpeedlimEnd'].Put(0, -1.9, 0, 0, 0, 0, 0, 1, 1.0);
	Structure['SpeedlimPole'].Put(0, -1.9, 0, 0, 0, 0, 0, 1, 1.0);


$CCWay-320;
		$dist = distance;
		$radius=-300;
		$cant=-50;
	include 'utility\CurveStart.txt';


$CCWay-300;
	Track['Mt_R'].Position(65.0, 0, 0, 0);
	Repeater['EnePole00'].Begin(0, 0, 0, 0, 0, 0, 0, 0, 25.0, 25.0, 'Pole0_2');


$CCWay-290;
;// ●●○ (上本場内・上１場内・下本場内)
	//Structure['PoleSL1'].Put(0, 2.6, -1.0, 0, 0, 180, 0, 1, 1.0);
	Structure['SignalB'].Put(0, 2.6,  0.0, 0, 0,  0, 0, 1, 1.0);


$CCWay-280;
;// ｣｢ 
// 道路
	Track['c02'].Position(-8.00, 0.00, 0, 0); // 市道
	Track['c01'].Position(8.00, 0.00, 0, 0); // 市道
	Repeater['Roadc01'].End();
	Repeater['Dikec01L'].End();


$CCWay-280;
//◎ 東側踏切　43.720
	Structure['Fumikiri_AL'].Put0(0, 0, 5);
	Structure['Fumikiri_AC'].Put0(0, 0, 5);
	Structure['Fumikiri_AR'].Put0(0, 0, 5);
	Sound3D['Fumikiri'].Put(-2.5, 3.0);


$CCWay-275;
	Repeater['EnePole00'].Begin(0, 0, 0, 0, 0, 180, 0, 0, 25.0, 25.0, 'Pole0_4');


$CCWay-180;
// 道路
	Track['c02'].Position(-70.00, 0.00, 0, 0); // 市道
	Repeater['Roadc02'].End();


$CCWay-87;
;// ■■□ (上本場内　遠方)
	//Structure['PoleSL1'].Put(0, -2.6, -1.0, 0, 0, 180, 0, 1, 1.0);
	Structure['SignalB'].Put(0, -2.6,  0.0, 0, 0,  0, 0, 1, 1.0);


$CCWay+125;
	Track['Mt_R'].Position(0.0, 0, 0, 0);
	Repeater['Mt_R'].Begin(0, 3.0, -5.0, 0, 0, 0, 0, 0, 200.0, 25.0, 'Mt_R');


$CCWay+150;
		$dist = distance;
	include 'utility\CurveEnd.txt';


$CCWay+175;
	Repeater['EnePole00'].Begin(0, 0, 0, 0, 0, 0, 0, 0, 25.0, 25.0, 'Pole0_2');


$CCWay+182;
	Structure['Grove03'].Put(0, -9.00, -3.00, 0.00, 0, 0, 0, 1, 1.0);
	Structure['Grove01'].Put(0, 12.00, -2.00, 0.00, 0, 0, 0, 1, 1.0);


$CCWay+184;
	Structure['Grove03'].Put(0, -6.00, -2.00, 0.00, 0, 0, 0, 1, 1.0);
	Structure['Grove03'].Put(0,  6.00, -1.00, 0.00, 0, 0, 0, 1, 1.0);
	Structure['Grove01'].Put(0, 10.00, 0.00, 0.00, 0, 0, 0, 1, 1.0);


$CCWay+185;
;//●□● [19] 丸滝 13
		$dist = distance;
	include 'utility\SingleTunnelStart.txt';


$CCWay+187;
	Structure['Grove02'].Put(0, -10.00, -2.00, 0.00, 0, 0, 0, 1, 1.0);
	Structure['Grove01'].Put(0,   8.00, -1.00, 0.00, 0, 0, 0, 1, 1.0);


$CCWay+190;
	Structure['Grove01'].Put(0, -10.00, -1.00, 0.00, 0, 0, 0, 1, 1.0);
	Structure['Mt_Wall10a'].Put(0, 0.00, 6.00, 0.00, 0, 0, 0, 1, 10.0);


$CCWay+192;
	Structure['Mt_Wall10a'].Put(0, 12.00, 5.00, 0.00, 0, 0, 0, 1, 10.0);


$CCWay+193;
	Structure['Grove02'].Put(0, -8.50, 3.00, 0.00, 0, 0, 0, 1, 1.0);
	Structure['Mt_Wall10a'].Put(0, 18.00, 8.00, 0.00, 0, 0, 0, 1, 10.0);


$CCWay+195;
	Structure['Grove01'].Put(0, -8.00, 2.00, 0.00, 0, 0, 0, 1, 1.0);


$CCWay+200;
;//○□○ [19] 丸滝
		$dist = distance;
	include 'utility\SingleTunnelEnd.txt';
// 道路
	Track['r10'].Position(-90.00, 0.00, 0, 0); // r10
	Repeater['Roadr10'].Begin('r10', 0.00, 0.00, 0.00, 0, 0, 0, 0, 10.0, 10.0, 'Road02');
	Track['c02'].Position(-50.00, 0.00, 0, 0); // 市道
	Repeater['Roadc02'].Begin('c02', 0.00, 0.00, 0.00, 0, 0, 0, 0, 10.0, 10.0, 'Road01');


$CCWay+225;
	Repeater['EnePole00'].Begin(0, 0, 0, 0, 0, 180, 0, 0, 25.0, 25.0, 'Pole0_4');


$CCWay+250;
	Repeater['Mt_R'].Begin(0, 45.0, -5.0, 0, 0, 0, 0, 0, 200.0, 25.0, 'Mt_R');


$CCWay+290;
//◎ 沈砂池踏切　44.290
	Structure['Fumikiri_AL'].Put0(0, 0, 5);
	Structure['Fumikiri_AC'].Put0(0, 0, 5);
	Structure['Fumikiri_AR'].Put0(0, 0, 5);
	Sound3D['Fumikiri'].Put(-2.5, 3.0);


$CCWay+310;
// 道路
	Track['c02'].Position(-8.00, 0.00, 0, 0); // 市道


$CCWay+400;
		$dist = distance;
		$slope=9.1;
	include 'utility\GraUpStart.txt';


$CCWay+475;
	Repeater['Mt_R'].Begin(0, 5.0, -5.0, 0, 0, 0, 0, 0, 200.0, 25.0, 'Mt_R');


$CCWay+570;
// 道路
	Track['c02'].Position(-8.00, 0.00, 0, 0); // 市道


$CCWay+750;
// 道路
	Track['r10'].Position(-30.00, 0.00, 0, 0); // r10
	Track['c02'].Position(-30.00, -0.10, 0, 0); // 市道
	Repeater['Roadc02'].End();


$CCWay+760;
		$dist = distance;
		$radius=400;
		$cant=40;
	include 'utility\CurveStart.txt';


$CCWay+920;
	Repeater['Mt_R'].End();
$CCWay+920;
// 道路
	Track['r10'].Position(-10.00, 0.00, 0, 0); // r10
//
		$dist = distance;
	include 'utility\CurveEnd.txt';


$CCWay+928;
//◎ 子之神踏切　44.928
	Structure['Fumikiri_AL'].Put0(0, 0, 5);
	Structure['Fumikiri_AC'].Put0(0, 0, 5);
	Structure['Fumikiri_AR'].Put0(0, 0, 5);
	Sound3D['Fumikiri'].Put(-2.5, 3.0);


$CCWay+1000;
	Repeater['EnePole00'].Begin(0, 0, 0, 0, 0, 0, 0, 0, 25.0, 25.0, 'Pole0_2');
;// [S] 塩之沢 確認
	Structure['StaNotifyC1X00'].Put(0, -2.4, 0.0, -0.5, 0, 0, 0, 1, 1.0);


$CCWay+1025;
	Repeater['EnePole00'].Begin(0, 0, 0, 0, 0, 180, 0, 0, 25.0, 25.0, 'Pole0_4');


$CCWay+1050;
		$dist = distance;
		$radius=300;
		$cant=45;
	include 'utility\CurveStart.txt';


$CCWay+1070;
// 道路
	Track['r10'].Position(-10.00, 0.00, 0, 0); // r10


$CCWay+1200;
;// [S] 塩之沢 B
	Structure['StaNotifyA1X00'].Put(0, -2.6, 0.0, 0, 0, 0, 0, 1, 1.0);


$CCWay+1250;
		$dist = distance;
	include 'utility\CurveEnd.txt';


$CCWay+1260;
// 道路
	Track['r10'].Position(-25.00, 0.00, 0, 0); // r10


$CCWay+1270;
		$dist = distance;
	include 'utility\GraUpEnd.txt';

$CCWay+1280;
//＊＊＊ 小持川 18
	RollingNoise.Change(3);
	Repeater['Rail00u'].End();
	Repeater['Bridge00'].Begin(0, 0, 0, 5.0, 0, 0, 0, 3, 5, 5, 'Bridge01');
	Repeater['Bridge_Makuragi0'].Begin(0, 0, 0, 2.0, 0, 0, 0, 3, 2, 2, 'Bridge_Makuragi');
	Repeater['BridgeuR'].Begin(0, 0, 0, 5.0, 0, 0, 0, 3, 5.0, 5.0, 'BridgeSGR0');
$CCWay+1280;
		$dist = distance;
		$riverlen=20;
	include 'utility\River2.txt';


$CCWay+1300;
//＊＊＊
	RollingNoise.Change(0);
	Repeater['Rail00u'].Begin(0, 0, 0, 0, 0, 0, 0, 3, 5.0, 5.0, 'Ballast0', 'Ballast1', 'Ballast2', 'Ballast3', 'Ballast4');
	Repeater['Bridge00'].End();
	Repeater['Bridge_Makuragi0'].End();
	Repeater['BridgeuR'].End();


$CCWay+1305;
;// [S] 塩之沢 非B
	Structure['StaNotifyB2X00'].Put(0, -2.6, 0.0, 0, 0, 0, 0, 1, 1.0);


$CCWay+1315;
//◎ 塩之沢踏切　45.315
	Structure['Fumikiri_AL'].Put0(0, 0, 5);
	Structure['Fumikiri_AC'].Put0(0, 0, 5);
	Structure['Fumikiri_AR'].Put0(0, 0, 5);
	Sound3D['Fumikiri'].Put(-2.5, 3.0);


$CCWay+1345;
// 桜
	Structure['Sakura01'].Put(0, -16.0, -1.5, 0, 0, 0, 0, 1, 1.0);


$CCWay+1355;
	Structure['FormRS2'].Put(0, 0, 0, -0.1, 0, 0, 0, 1, 5.0);
	Structure['FormCRS2'].Put(0, 0, 0, -0.1, 0, 0, 0, 1, 5.0);


//▼▼▼ 塩之沢 (CC17)
$CCWay+1360;
	Repeater['Form00S'].Begin0(0, 1, 5.0, 5.0, 'FormR2');
	Repeater['Form00C'].Begin0(0, 1, 5.0, 5.0, 'FormCR2');
$CCWay+1360;
// [○B]
	Structure['StaStop99'].Put(0, -2.3, 3.1, 0, 0, 0, 0, 1, 1.0);
	Structure['StaPole00'].Put(0, -2.3, 3.3, 0, 0, 0, 0, 1, 1.0);


$CCWay+1365;
// 桜
	Structure['Sakura01'].Put(0, -14.0, -1.5, 0, 0, 0, 0, 1, 1.0);


$CCWay+1385;
// 桜
	Structure['Sakura01'].Put(0, -12.0, -1.5, 0, 0, 180, 0, 1, 1.0);


$CCWay+1400;
// [○B]
	Structure['StaStop02'].Put(0, -2.3, 3.1, 0, 0, 0, 0, 1, 1.0);
	Structure['StaPole00'].Put(0, -2.3, 3.3, 0, 0, 0, 0, 1, 1.0);


$CCWay+1405;
// 桜
	Structure['Sakura01'].Put(0, -15.0, -1.5, 0, 0, 0, 0, 1, 1.0);


$CCWay+1415;
// 桜
	Structure['Sakura01'].Put(0, 10.0, -1.5, 0, 0, 180, 0, 1, 1.0);


$CCWay+1420;
// [○B]
	Structure['StaStop03'].Put(0, -2.3, 3.1, 0, 0, 0, 0, 1, 1.0);
	Structure['StaPole00'].Put(0, -2.3, 3.3, 0, 0, 0, 0, 1, 1.0);


$CCWay+1440;
	Repeater['Form00S'].End();
	Repeater['Form00C'].End();
$CCWay+1440;
// [○B]
	Structure['StaStop00'].Put(0, -2.3, 3.1, 0, 0, 0, 0, 1, 1.0);
	Structure['StaPole00'].Put(0, -2.3, 3.3, 0, 0, 0, 0, 1, 1.0);
//▲▲▲ 塩之沢 (CC17)
// 道路
	Track['r10'].Position(-25.00, 0.00, 0, 0); // r10


$CCWay+1460;
		$dist = distance;
		$slope=-9.1;
	include 'utility\GraDownStart.txt';

$CCWay+1470;
		$dist = distance;
		$radius=-400;
		$cant=-40;
	include 'utility\CurveStart.txt';

$CCWay+1500;
	Repeater['Mt_R'].Begin(0, 5.0, -5.0, 0, 0, 0, 0, 0, 200.0, 25.0, 'Mt_R');


$CCWay+1505;
;// [S] 塩之沢 非B
	Structure['StaNotifyB2X00'].Put(0, 2.6, 0.0, 0, 0, 180, 0, 1, 1.0);


$CCWay+1580;
		$dist = distance;
	include 'utility\GraDownEnd.txt';
$CCWay+1580;
// [ □ 】
	Structure['SikaY'].Put(0, 2.4, -0.3, 0, 0, 0, 0, 1, 1.0);
	Structure['SpeedlimPole'].Put(0, 2.4, 0, 0, 0, 0, 0, 1, 1.0);


$CCWay+1605;
;// [S] 塩之沢 B
	Structure['StaNotifyA1X00'].Put(0, 2.6, 0.0, 0, 0, 180, 0, 1, 1.0);


$CCWay+1690;
// 道路
	Track['r10'].Position(-55.00, 0.00, 400, 0); // r10


$CCWay+1800;
		$dist = distance;
		$slope=25;
	include 'utility\GraUpStart.txt';
$CCWay+1800;
	Track['Height'].Position(0, -1.1);
	Repeater['Dike00L'].Begin(0, 0, 0, 0, 0, 0, 0, 3, 10.0, 10.0, 'DikeL');
	//Repeater['Dike00R'].Begin(0, 0, 0, 0, 0, 0, 0, 3, 10.0, 10.0, 'DikeR');


$CCWay+1830;
// 道路
	Track['r10'].Position(-60.00, 0.00, 0, 0); // r10
//
		$dist = distance;
	include 'utility\CurveEnd.txt';


$CCWay+1990;
// 道路
	Track['r10'].Position(-40.00, 0.00, 0, 0); // r10


$CCWay+2110;
// 道路
	Track['r10'].Position(-80.00, 0.00, 0, 0); // r10


$CCWay+2120;
		$dist = distance;
	include 'utility\GraUpEnd.txt';


$CCWay+2125;
	Repeater['Mt_R'].Begin(0, 150.0, -5.0, 0, 0, 0, 0, 0, 200.0, 25.0, 'Mt_R');


$CCWay+2190;
//◎ 泥之沢踏切　46.190
	Structure['Fumikiri_AL'].Put0(0, 0, 5);
	Structure['Fumikiri_AC'].Put0(0, 0, 5);
	Structure['Fumikiri_AR'].Put0(0, 0, 5);
	Sound3D['Fumikiri'].Put(-2.5, 3.0);


$CCWay+2225;
	Repeater['Mt_R'].Begin(0, 5.0, -5.0, 0, 0, 0, 0, 0, 200.0, 25.0, 'Mt_R');


$CCWay+2260;
		$dist = distance;
		$slope=-10;
	include 'utility\GraDownStart.txt';


$CCWay+2300;
	Repeater['EnePole00'].Begin(0, 0, 0, 0, 0, 0, 0, 0, 25.0, 25.0, 'Pole0_2');


$CCWay+2325;
	Repeater['EnePole00'].Begin(0, 0, 0, 0, 0, 0, 0, 0, 25.0, 25.0, 'Pole0_4');


$CCWay+2350;
	Track['Height'].Position(0, -10.1);
	Repeater['Dike00L'].Begin(0, 0, 0, 0, 0, 0, 0, 3, 10.0, 10.0, 'DikeL');
	Repeater['Dike00R'].Begin(0, 0, 0, 0, 0, 0, 0, 3, 10.0, 10.0, 'DikeR');


$CCWay+2400;
		$dist = distance;
	include 'utility\GraDownEnd.txt';


$CCWay+2520;
// 道路
	Track['r10'].Position(-210.00, 0.00, 0, 0); // r10
//
		$dist = distance;
		$slope=25;
	include 'utility\GraUpStart.txt';


$CCWay+2600;
	Repeater['Mt_R'].Begin(0, 5.0, -5.0, 0, 0, 0, 0, 0, 200.0, 25.0, 'Mt_R');


$CCWay+2665;
//◎ 上小路踏切　46.665
	Structure['Fumikiri_AL'].Put0(0, 0, 5);
	Structure['Fumikiri_AC'].Put0(0, 0, 5);
	Structure['Fumikiri_AR'].Put0(0, 0, 5);
	Sound3D['Fumikiri'].Put(-2.5, 3.0);


$CCWay+2725;
	Repeater['Mt_R'].Begin(0, 5.0, -5.0, 0, 0, 0, 0, 0, 200.0, 25.0, 'Mt_R');


$CCWay+2780;
		$dist = distance;
		$radius=-400;
		$cant=-40;
	include 'utility\CurveStart.txt';

$CCWay+2920;
		$dist = distance;
	include 'utility\CurveEnd.txt';


$CCWay+3080;
// 道路
	Track['r10'].Position(-210.00, -3.00, 0, 0); // r10
	Repeater['Roadr10'].End();
//
		$dist = distance;
		$radius=400;
		$cant=40;
	include 'utility\CurveStart.txt';


$CCWay+3100;
	Repeater['EnePole00'].Begin(0, 0, 0, 0, 0, 0, 0, 0, 25.0, 25.0, 'Pole0_2');

$CCWay+3125;
	Repeater['EnePole00'].Begin(0, 0, 0, 0, 0, 180, 0, 0, 25.0, 25.0, 'Pole0_4');


$CCWay+3320;
// [ □ 】
	Structure['SikaG'].Put(0, -2.4, -0.3, 0, 0, 0, 0, 1, 1.0);
	Structure['SpeedlimPole'].Put(0, -2.4, 0, 0, 0, 0, 0, 1, 1.0);

$CCWay+3375;
	Repeater['EnePole00'].Begin(0, 0, 0, 0, 0, 0, 0, 0, 25.0, 25.0, 'Pole0_2');

$CCWay+3400;
	Repeater['EnePole00'].Begin(0, 0, 0, 0, 0, 180, 0, 0, 25.0, 25.0, 'Pole0_4');

$CCWay+3425;
	Repeater['EnePole00'].Begin(0, 0, 0, 0, 0, 0, 0, 0, 25.0, 25.0, 'Pole0_2');

$CCWay+3430;
		$dist = distance;
	include 'utility\CurveEnd.txt';


$CCWay+3445;
;//●□● [20] 帯金 653
	Repeater['EnePole00'].End();
		$dist = distance;
	include 'utility\SingleTunnelStart.txt';


$CCWay+3450;
	Track['Height'].Position(0, -11.1);
	Repeater['Dike00L'].End();
	Repeater['Dike00R'].End();
	Repeater['Mt_R'].End();


$CCWay+3800;
		$dist = distance;
	include 'utility\GraUpEnd.txt';

$CCWay+4100;
;//○□○ [20] 帯金
		$dist = distance;
	include 'utility\SingleTunnelEnd.txt';
$CCWay+4100;
	Track['Height'].Position(0, -6.1);
	Repeater['Mt_L'].Begin(0,  3.0, -5.0, 0, 0, 0, 0, 0, 200.0, 25.0, 'Mt_L');
	Repeater['Mt_R'].Begin(0, -3.0, -5.0, 0, 0, 0, 0, 0, 200.0, 25.0, 'Mt_R');


$CCWay+4100;
		$dist = distance;
		$slope=-13.3;
	include 'utility\GraDownStart.txt';


$CCWay+4125;
	Repeater['EnePole00'].Begin(0, 0, 0, 0, 0, 0, 0, 0, 25.0, 25.0, 'Pole0_2');


$CCWay+4200;
	Repeater['EnePole00'].Begin(0, 0, 0, 0, 0, 180, 0, 0, 25.0, 25.0, 'Pole0_4');


$CCWay+4350;
	Repeater['Mt_L'].End();
	Repeater['Mt_R'].End();
	Repeater['Dike00L'].Begin(0, 0, 0, 0, 0, 0, 0, 3, 10.0, 10.0, 'DikeL');
	Repeater['Dike00R'].Begin(0, 0, 0, 0, 0, 0, 0, 3, 10.0, 10.0, 'DikeR');


$CCWay+4480;
		$dist = distance;
		$radius=-400;
		$cant=-40;
	include 'utility\CurveStart.txt';

$CCWay+4600;
	Repeater['EnePole00'].Begin(0, 0, 0, 0, 0, 0, 0, 0, 25.0, 25.0, 'Pole0_2');

$CCWay+4625;
	Repeater['EnePole00'].Begin(0, 0, 0, 0, 0, 0, 0, 0, 25.0, 25.0, 'Pole0_4');

$CCWay+4630;
		$dist = distance;
	include 'utility\CurveEnd.txt';

$CCWay+4720;
	Track['Height'].Position(0, -6.1);
	Repeater['Dike00L'].End();
	Repeater['Dike00R'].End();
	Structure['Mt_L'].Put(0,  3.0, -5.0, 0, 0, 0, 0, 0, 200.0);
	Structure['Mt_R'].Put(0, -3.0, -5.0, 0, 0, 0, 0, 0, 200.0);
	Structure['Mt'].Put(0, 0.0, 7.0, 5.0, 0, 0, 0, 0, 200.0);
;//●□● [21] 第一八木沢 227
	Repeater['EnePole00'].End();
		$dist = distance;
	include 'utility\SingleTunnelStart.txt';


$CCWay+2900;
		$dist = distance;
	include 'utility\GraDownEnd.txt';

$CCWay+4945;
;//○□○ [21] 第一八木沢
		$dist = distance;
	include 'utility\SingleTunnelEnd.txt';
$CCWay+4945;
	Track['Height'].Position(0, -6.1);
	Repeater['Dike00L'].Begin(0, 0, 0, 0, 0, 0, 0, 3, 10.0, 10.0, 'DikeL');
	Repeater['Dike00R'].Begin(0, 0, 0, 0, 0, 0, 0, 3, 10.0, 10.0, 'DikeR');

$CCWay+4950;
	Repeater['EnePole00'].Begin(0, 0, 0, 0, 0, 0, 0, 0, 25.0, 25.0, 'Pole0_2');

$CCWay+700;
	Repeater['EnePole00'].Begin(0, 0, 0, 0, 0, 0, 0, 0, 25.0, 25.0, 'Pole0_4');

$CCWay+5100;
	Repeater['EnePole00'].Begin(0, 0, 0, 0, 0, 0, 0, 0, 25.0, 25.0, 'Pole0_2');


$CCWay+5145;
;//●□● [22] 第二八木沢 418
	Repeater['EnePole00'].End();
		$dist = distance;
	include 'utility\SingleTunnelStart.txt';
$CCWay+5145;
	Track['Height'].Position(0, -6.1);
	Repeater['Dike00L'].End();
	Repeater['Dike00R'].End();
	Structure['Mt_L'].Put(0,  3.0, -5.0, 0, 0, 0, 0, 0, 200.0);
	Structure['Mt_R'].Put(0, -3.0, -5.0, 0, 0, 0, 0, 0, 200.0);
	Structure['Mt'].Put(0, 0.0, 7.0, 5.0, 0, 0, 0, 0, 200.0);


$CCWay+5160;
		$dist = distance;
		$slope=-3;
	include 'utility\GraDownStart.txt';

$CCWay+5430;
		$dist = distance;
		$radius=600;
		$cant=25;
	include 'utility\CurveStart.txt';

$CCWay+5460;
		$dist = distance;
	include 'utility\GraDownEnd.txt';


$CCWay+50;
// 山
	Track['Mt_R'].Position( 180.0, -5.0, 0, 0);
	Repeater['Mt_R1'].Begin('Mt_R',   0.0,  0.0, -10.0, 0, 0, 0, 3, 200.0, 25.0, 'Mt_R');


$CCWay+5565;
;//○□○ [22] 第二八木沢
		$dist = distance;
	include 'utility\SingleTunnelEnd.txt';

$CCWay+5575;
	Repeater['EnePole00'].Begin(0, 0, 0, 0, 0, 0, 0, 0, 25.0, 25.0, 'Pole0_2');

$CCWay+5630;
	Track['Height'].Position(0, -1.1);
//＊＊＊ 第一湯川 112
	RollingNoise.Change(3);
	Repeater['Rail00u'].End();
	Repeater['Bridge00'].Begin(0, 0, 0, 5.0, 0, 0, 0, 3, 5, 5, 'Bridge01');
	Repeater['Bridge_Makuragi0'].Begin(0, 0, 0, 2.0, 0, 0, 0, 3, 2, 2, 'Bridge_Makuragi');
	Repeater['BridgeuR'].Begin(0, 0, 0, 5.0, 0, 0, 0, 3, 5.0, 5.0, 'BridgeSGR0');
$CCWay+5630;
		$dist = distance;
		$riverlen=110;
	include 'utility\River2.txt';

$CCWay+5740;
//＊＊＊
	RollingNoise.Change(0);
	Repeater['Rail00u'].Begin(0, 0, 0, 0, 0, 0, 0, 3, 5.0, 5.0, 'Ballast0', 'Ballast1', 'Ballast2', 'Ballast3', 'Ballast4');
	Repeater['Bridge00'].End();
	Repeater['Bridge_Makuragi0'].End();
	Repeater['BridgeuR'].End();
$CCWay+5740;
	Track['Height'].Position(0, -2.1);
	Repeater['Dike00L'].Begin(0, 0, 0, 0, 0, 0, 0, 3, 10.0, 10.0, 'DikeL');
	Repeater['Dike00R'].Begin(0, 0, 0, 0, 0, 0, 0, 3, 10.0, 10.0, 'DikeR');

$CCWay+5750;
	Repeater['EnePole00'].Begin(0, 0, 0, 0, 0, 0, 0, 0, 25.0, 25.0, 'Pole0_4');




//--------------------------------------------------------------------------



// (C)Harupi
