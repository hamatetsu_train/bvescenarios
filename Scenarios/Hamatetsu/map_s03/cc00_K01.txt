﻿BveTs Map 2.00

//////////////////////////////////////////////////////////////////////////
// -500 - (275-400)  map_s03/cc00_K1
// 身延線 富士(CC00) 身１
//////////////////////////////////////////////////////////////////////////

// ※駅中心　-0K400
$CCWay = $Fuji + 0;


$CCWay-100;
//	Repeater['EnePole00'].Begin(0, 0, 0, 0, 0, 0, 0, 0, 25.0, 25.0, 'Pole0_2');

	Track['21'].Position(-47.85, 0, 329, 0); // 東海道下１
	Track['20'].Position(-38.35, 0, 329, 0); // 東海道下本
	Track['30'].Position(-34.55, 0, 329, 0); // 東海道上本
	Track['31'].Position(-26.95, 0, 329, 0); // 東海道上１
	Track['32'].Position(-18.35, 0, 329, 0); // 東海道上２
	Track['10'].Position( 11.40, 0,   0, 0); // 下本
	Repeater['Rail21L'].Begin('21', -0.5335, 0, 0, 0, 0, 0, 3, 5.0, 5.0, 'RailL0', 'RailL1', 'RailL2', 'RailL3', 'RailL4');
	Repeater['Rail21R'].Begin('21',  0.5335, 0, 0, 0, 0, 0, 3, 5.0, 5.0, 'RailR0', 'RailR1', 'RailR2', 'RailR3', 'RailR4');
	Repeater['Rail21u'].Begin('21', 0, 0, 0, 0, 0, 0, 3, 5, 5, 'Ballast0', 'Ballast1', 'Ballast2', 'Ballast3', 'Ballast4');
	Repeater['Kasen21'].Begin('21', 0, 0, 0, 0, 0, 0, 1, 25, 25, 'KasenS0');
	Repeater['Rail20L'].Begin('20', -0.5335, 0, 0, 0, 0, 0, 3, 5.0, 5.0, 'RailL0', 'RailL1', 'RailL2', 'RailL3', 'RailL4');
	Repeater['Rail20R'].Begin('20',  0.5335, 0, 0, 0, 0, 0, 3, 5.0, 5.0, 'RailR0', 'RailR1', 'RailR2', 'RailR3', 'RailR4');
	Repeater['Rail20u'].Begin('20', 0, 0, 0, 0, 0, 0, 3, 5, 5, 'Ballast0', 'Ballast1', 'Ballast2', 'Ballast3', 'Ballast4');
	Repeater['Kasen20'].Begin('20', 0, 0, 0, 0, 0, 0, 1, 25, 25, 'KasenS0');
	Repeater['Rail30L'].Begin('30', -0.5335, 0, 0, 0, 0, 0, 3, 5.0, 5.0, 'RailL0', 'RailL1', 'RailL2', 'RailL3', 'RailL4');
	Repeater['Rail30R'].Begin('30',  0.5335, 0, 0, 0, 0, 0, 3, 5.0, 5.0, 'RailR0', 'RailR1', 'RailR2', 'RailR3', 'RailR4');
	Repeater['Rail30u'].Begin('30', 0, 0, 0, 0, 0, 0, 3, 5, 5, 'Ballast0', 'Ballast1', 'Ballast2', 'Ballast3', 'Ballast4');
	Repeater['Kasen30'].Begin('30', 0, 0, 0, 0, 0, 0, 1, 25, 25, 'KasenS0');
	Repeater['Rail31L'].Begin('31', -0.5335, 0, 0, 0, 0, 0, 3, 5.0, 5.0, 'RailL0', 'RailL1', 'RailL2', 'RailL3', 'RailL4');
	Repeater['Rail31R'].Begin('31',  0.5335, 0, 0, 0, 0, 0, 3, 5.0, 5.0, 'RailR0', 'RailR1', 'RailR2', 'RailR3', 'RailR4');
	Repeater['Rail31u'].Begin('31', 0, 0, 0, 0, 0, 0, 3, 5, 5, 'Ballast0', 'Ballast1', 'Ballast2', 'Ballast3', 'Ballast4');
	Repeater['Kasen31'].Begin('31', 0, 0, 0, 0, 0, 0, 1, 25, 25, 'KasenS0');
	Repeater['Rail32L'].Begin('32', -0.5335, 0, 0, 0, 0, 0, 3, 5.0, 5.0, 'RailL0', 'RailL1', 'RailL2', 'RailL3', 'RailL4');
	Repeater['Rail32R'].Begin('32',  0.5335, 0, 0, 0, 0, 0, 3, 5.0, 5.0, 'RailR0', 'RailR1', 'RailR2', 'RailR3', 'RailR4');
	Repeater['Rail32u'].Begin('32', 0, 0, 0, 0, 0, 0, 3, 5, 5, 'Ballast0', 'Ballast1', 'Ballast2', 'Ballast3', 'Ballast4');
	Repeater['Kasen32'].Begin('32', 0, 0, 0, 0, 0, 0, 1, 25, 25, 'KasenS0');
	Repeater['Rail10L'].Begin('10', -0.5335, 0, 0, 0, 0, 0, 3, 5.0, 5.0, 'RailL0', 'RailL1', 'RailL2', 'RailL3', 'RailL4');
	Repeater['Rail10R'].Begin('10',  0.5335, 0, 0, 0, 0, 0, 3, 5.0, 5.0, 'RailR0', 'RailR1', 'RailR2', 'RailR3', 'RailR4');
	Repeater['Rail10u'].Begin('10', 0, 0, 0, 0, 0, 0, 3, 5, 5, 'Ballast0', 'Ballast1', 'Ballast2', 'Ballast3', 'Ballast4');
	Repeater['Kasen10'].Begin('10', 0, 0, 0, 0, 0, 0, 1, 25, 25, 'KasenS0');



//▼▼▼ 富士 (CC00)
$CCWay-100;
	Repeater['Form00S'].Begin0(0, 1, 5.0, 5.0, 'FormR2');
	Repeater['Form00C'].Begin0(0, 1, 5.0, 5.0, 'FormCR2');
	Repeater['Form10S'].Begin0('10', 1, 5.0, 5.0, 'FormL2');
	Repeater['Form10C'].Begin0('10', 1, 5.0, 5.0, 'FormCL2');


$CCWay-50;
	Curve.Begin(329);
	//Track['21'].Position(-43.75, 0, 329, 0); // 東海道下１
	Track['21'].Position(-35.10, 0, 329, 0); // 東海道下１
	Track['20'].Position(-25.60, 0, 329, 0); // 東海道下本
	Track['30'].Position(-22.80, 0, 329, 0); // 東海道上本
	Track['31'].Position(-15.20, 0, 329, 0); // 東海道上１
	Track['32'].Position( -7.60, 0, 329, 0); // 東海道上２
	Track['10'].Position( 11.40, 0,    0, 0); // 上本


$CCWay-25;
	Track['21'].Position(-30.40, 0,  329, 0); // 東海道下１
	Track['20'].Position(-23.75, 0,  329, 0); // 東海道下本
	Track['30'].Position(-19.95, 0,  329, 0); // 東海道上本
	Track['31'].Position(-12.35, 0, -162, 0); // 東海道上１
	Track['32'].Position( -4.75, 0,  329, 0); // 東海道上２
	Track['10'].Position( 11.40, 0,  329, 0); // 上本
	Structure['Pole4_0'].Put('31', 0.00, 0, 0, 0, 0, 0, 0, 25.0);
	Structure['Pole0_2'].Put('10', 0.00, 0, 0, 0, 0, 0, 0, 25.0);


$CCWay+0;
	Curve.End();

	$RailNo='22';
	Track[$RailNo].Position(-27.55, 0, -329, 0); // 東海道下引上
	Repeater['Rail'+$RailNo+'u'].Begin($RailNo, 0, 0, 0, 0, 0, 0, 3, 5, 5, 'Ballast0', 'Ballast1', 'Ballast2', 'Ballast3', 'Ballast4');
	Repeater['Kasen'+$RailNo].Begin($RailNo, 0, 0, 0, 0, 0, 0, 1, 25, 25, 'KasenS0');
		$dist = distance;
	include 'utility\TongStartL.txt';

	Track['21'].Position(-27.55, 0,    0, 0); // 東海道下１
	Track['20'].Position(-22.80, 0,    0, 0); // 東海道下本
	Track['30'].Position(-19.00, 0,    0, 0); // 東海道上本
	Track['31'].Position(-10.45, 0, -162, 0); // 東海道上１

	$RailNo='02';
	Track[$RailNo].Position(-10.45, 0,  329, 0); // 海上１->身下本
	Repeater['Rail'+$RailNo+'u'].Begin($RailNo, 0, 0, 0, 0, 0, 0, 3, 5, 5, 'Ballast0', 'Ballast1', 'Ballast2', 'Ballast3', 'Ballast4');
	Repeater['Kasen'+$RailNo].Begin($RailNo, 0, 0, 0, 0, 0, 0, 1, 25, 25, 'KasenS0');
		$dist = distance;
	include 'utility\TongStartR.txt';

	Track['32'].Position( -3.80, 0, -162, 0); // 東海道上２
	Track['10'].Position( 10.45, 0,    0, 0); // 上本

	Structure['Pole4_0'].Put('31', 0.00, 0, 0, 0, 0, 0, 0, 25.0);
	Structure['Pole0_2'].Put('10', 0.00, 0, 0, 0, 0, 0, 0, 25.0);


$CCWay+25;
	Curve.Begin(-329);
	Track['22'].Position(-26.60, 0, 329, 0); // 東海道下引上
	Track['21'].Position(-25.65, 0, 329, 0); // 東海道下１
	Track['20'].Position(-22.80, 0, 329, 0); // 東海道下本
	Track['30'].Position(-19.00, 0, 329, 0); // 東海道上本
	Track['31'].Position(-12.35, 0, 329, 0); // 東海道上１
	Track['02'].Position(-11.40, 0, 162, 0); // 海上１->身下本
	Track['32'].Position( -5.70, 0, 329, 0); // 東海道上２
	Track['10'].Position(  8.55, 0, 329, 0); // 上本
	Structure['Pole4_0'].Put('31', 0.00, 0, 0, 0, 0, 0, 0, 25.0);
	Structure['Pole0_2'].Put('10', 0.00, 0, 0, 0, 0, 0, 0, 25.0);


$CCWay+35;
	Repeater['Form00C'].End();
	Repeater['Form00S'].End();


$CCWay+50;
	Track['21'].Position(-22.80, 0,    0, 0); // 東海道下１
	Track['20'].Position(-21.85, 0,    0, 0); // 東海道下本
	Track['30'].Position(-18.05, 0,    0, 0); // 東海道上本
	Track['31'].Position(-15.20, 0,  108, 0); // 東海道上１
	Track['02'].Position( -9.50, 0,    0, 0); // 海上１->身下本
	Track['32'].Position( -7.60, 0,  108, 0); // 東海道上２
	Track['10'].Position(  7.60, 0,    0, 0); // 上本
	Structure['Pole4_0'].Put('21', 0.00, 0, 0, 0, 0, 0, 0, 25.0);
	Structure['Pole0_2'].Put(0, 0.00, 0, 0, 0, 0, 0, 0, 25.0);
	Structure['Pole0_2'].Put('10', 0.00, 0, 0, 0, 0, 0, 0, 25.0);


$CCWay+55;
	Repeater['Form10C'].End();
	Repeater['Form10S'].End();
$CCWay+55;
;//●●○ 出発
	Section.BeginNew(0, 2, 4);
	Structure['PoleSL1'].Put(0, 2.4, -1.0, 0, 0, 0, 0, 1, 1.0);
	Signal['type3'].Put(0, 0, 2.4, 3.8, 0, 0, 0, 0, 1, 1.0);
//▲▲▲ 富士 (CC00)


$CCWay+75;
	Curve.End();
	Track['22'].Position(-22.80, 0,    0, 0); // 東海道下引上

	$RailNo='21';
	Track[$RailNo].Position(-19.00, 0,    0, 0); // 東海道下１
	Repeater['Rail'+$RailNo+'u'].End();
	Repeater['Kasen'+$RailNo].End();
		$dist = distance;
	include 'utility\TongEndL.txt';

	Track['20'].Position(-19.00, 0,    0, 0); // 東海道下本
	Track['30'].Position(-15.20, 0,    0, 0); // 東海道上本
	Track['31'].Position(-14.25, 0,  329, 0); // 東海道上１
	Track['02'].Position( -5.70, 0,    0, 0); // 海上１->身下本

	$RailNo='32';
	Track[$RailNo].Position( -5.70, 0,    0, 0); // 東海道上２
	Repeater['Rail'+$RailNo+'u'].End();
	Repeater['Kasen'+$RailNo].End();
		$dist = distance;
	include 'utility\TongEndR.txt';

	Track['10'].Position(  7.60, 0, -329, 0); // 上本

	Structure['Pole3_0'].Put('22', 0.00, 0, 0, 0, 0, 0, 0, 25.0);


$CCWay+100;
	Track['22'].Position(-19.00, 0,    0, 0); // 東海道下引上
	Track['20'].Position(-15.20, 0,    0, 0); // 東海道下本
	Track['30'].Position(-11.40, 0,    0, 0); // 東海道上本

	$RailNo='31';
	Track[$RailNo].Position(-11.40, 0,    0, 0); // 東海道上１
	Repeater['Rail'+$RailNo+'u'].End();
	Repeater['Kasen'+$RailNo].End();
		$dist = distance;
	include 'utility\TongEndR.txt';

	Track['02'].Position( -1.90, 0, -162, 0); // 海上１->身下本
	Track['10'].Position(  6.65, 0,    0, 0); // 上本

	Structure['Pole3_0'].Put('22', 0.00, 0, 0, 0, 0, 0, 0, 25.0);
	Repeater['EnePole00'].Begin(0, -1.90, 0, 0, 0, 0, 0, 0, 25.0, 25.0, 'Pole2_0');
	JointNoise.Play(0);


$CCWay+125;
//	Curve.End();
	Track['22'].Position(-15.20, 0,    0, 0); // 東海道下引上
	Track['20'].Position(-11.40, 0,   0, 0); // 東海道下本
	Track['30'].Position( -7.60, 0,   0, 0); // 東海道上本

	$RailNo='02';
	Track[$RailNo].Position(  0.00, 0,   0, 0); // 海上１->身下本
	Repeater['Rail'+$RailNo+'u'].End();
	Repeater['Kasen'+$RailNo].End();
		$dist = distance;
	include 'utility\TongEndL.txt';

	Track['10'].Position(  4.75, 0, 162, 0); // 上本

	Structure['Pole2_0'].Put('22', 0.00, 0, 0, 0, 0, 0, 0, 25.0);
	Repeater['EnePole00'].Begin(0, 0.00, 0, 0, 0, 0, 0, 0, 25.0, 25.0, 'Pole2_0');


$CCWay+138;
	Curve.Begin(162);
	Track['22'].Position(-13.30, 0, -162, 0); // 東海道下引上
	Track['20'].Position( -9.50, 0, -162, 0); // 東海道下本
	Track['30'].Position( -5.70, 0, -162, 0); // 東海道上本
	Track['10'].Position(  3.80, 0,    0, 0); // 上本


$CCWay+150;
	Repeater['EnePole00'].Begin(0, 0.00, 0, 0, 0, 0, 0, 0, 25.0, 25.0, 'Pole1_0');


$CCWay+163;
	Curve.End();
	Track['22'].Position(-11.40, 0,    0, 0); // 東海道下引上
	Track['20'].Position( -7.60, 0, 0, 0); // 東海道下本
	Track['30'].Position( -3.80, 0, 0, 0); // 東海道上本


$CCWay+175;
	Repeater['EnePole00'].Begin(0, -11.40, 0, 0, 0, 0, 0, 0, 25.0, 25.0, 'Pole4_0');


$CCWay+200;
	Track['22'].Position(-11.40, 0,    0, 0); // 東海道下引上

	$RailNo='03';
	Track[$RailNo].Position( -7.60, 0,  329, 0); // 東海道渡
	Repeater['Rail'+$RailNo+'u'].Begin($RailNo, 0, 0, 0, 0, 0, 0, 3, 5, 5, 'Ballast0', 'Ballast1', 'Ballast2', 'Ballast3', 'Ballast4');
	Repeater['Kasen'+$RailNo].Begin($RailNo, 0, 0, 0, 0, 0, 0, 1, 25, 25, 'KasenS0');
		$dist = distance;
	include 'utility\TongStartR.txt';

	$RailNo='04';
	Track[$RailNo].Position(  3.80, 0, -329, 0); // 身上本->身下本
	Repeater['Rail'+$RailNo+'u'].Begin($RailNo, 0, 0, 0, 0, 0, 0, 3, 5, 5, 'Ballast0', 'Ballast1', 'Ballast2', 'Ballast3', 'Ballast4');
	Repeater['Kasen'+$RailNo].Begin($RailNo, 0, 0, 0, 0, 0, 0, 1, 25, 25, 'KasenS0');
		$dist = distance;
	include 'utility\TongStartL.txt';


$CCWay+225;
	Track['03'].Position(-6.65, 0, 0, 0); // 東海道上１
	Track['04'].Position( 2.85, 0, 0, 0); // 身上本->身下本


$CCWay+250;
	Track['03'].Position(-4.75, 0, -329, 0); // 東海道上１
	Track['04'].Position( 0.95, 0,  329, 0); // 身上本->身下本
	JointNoise.Play(0);


$CCWay+275;
	$RailNo='03';
	Track[$RailNo].Position(-3.80, 0, 0, 0); // 東海道上１
	Repeater['Rail'+$RailNo+'u'].End();
	Repeater['Kasen'+$RailNo].End();
		$dist = distance;
	include 'utility\TongEndL.txt';

	$RailNo='04';
	Track[$RailNo].Position(0.00, 0, 0, 0); // 身上本->身下本
	Repeater['Rail'+$RailNo+'u'].End();
	Repeater['Kasen'+$RailNo].End();
		$dist = distance;
	include 'utility\TongEndR.txt';



/////////////////////





$CCWay+1330;
// [ □ 】
///	Structure['SpeedlimBegin'].Put(0, -2.2, -1.0, 0, 0, 0, 0, 1, 1.0);
//	Structure['SpeedlimB5'].Put(0, -2.3, -1.0, 0, 0, 0, 0, 1, 1.0);
//	Structure['SpeedlimB0'].Put(0, -2.1, -1.0, 0, 0, 0, 0, 1, 1.0);
//	Structure['SpeedlimPole'].Put(0, -2.2, -1.0, 0, 0, 0, 0, 1, 1.0);
//	SpeedLimit.Begin(50);


//--------------------------------------------------------------------------



// (C)Harupi
