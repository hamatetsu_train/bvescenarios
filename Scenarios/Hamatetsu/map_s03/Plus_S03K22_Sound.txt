﻿BveTs Map 2.00

//////////////////////////////////////////////////////////////////////////
// 身延線 普通 S03-K22 サウンド
//////////////////////////////////////////////////////////////////////////


// -------------------------------------------------------------------------------------------------------------

$CCWay = $Fuji+0;


//▼▼▼ 富士 (CC00 / CAxx)

//▲▲▲ 富士 (CC00 / CAxx)

$Fuji+0 + 300;
	//Sound['n_car'].Play();


$Fuji+0 + 1800;
	Sound['o_01'].Play();


$Fuji+0 + 2800;
	//Sound['n_order'].Play();


$Iriyamase-250 +430;
	Sound['o_02'].Play();


$Fujinomiya-370 + 900;
	Sound['o_03'].Play();


$Fujinomiya-370 + 900;
//	Sound['0601'].Play();


//▼▼▼ 西富士宮 (CC07)

//▲▲▲ 西富士宮 (CC07)


$NishiFujinomiya+420 + 615;
	//Sound['n_fuji1'].Play();


$NishiFujinomiya+420 + 1900;
	//Sound['n_fuji2'].Play();


$NishiFujinomiya+420 + 2300;
	Sound['o_04'].Play();


$NishiFujinomiya+420 +7200;
	Sound['o_05'].Play();


$Shibakawa+120 +5500;
	Sound['o_06'].Play();


$Toshima - 900 + 1000;
	Sound['o_07'].Play();


$Toshima+100 +4000;
	Sound['o_08'].Play();


$Toshima+100 + 7300;
	Sound['1401'].Play();


//▼▼▼ 内船 (CC14)

//▲▲▲ 内船 (CC14)


$Utsubuna-750 + 940;
	//Sound['n_sight'].Play();


$Utsubuna+250 +3200;
	Sound['o_09'].Play();


$Oshima-390 + 2500;
	Sound['o_10'].Play();


$Oshima-390 + 3500;
	Sound['1601'].Play();


//▼▼▼ 身延 (CC16)

//▲▲▲ 身延 (CC16)


$Minobu-160 + 400;
	//Sound['n_car'].Play();


$Minobu+840 +400;
	Sound['o_11'].Play();


$Hadakajima+120+1000;
	Sound['o_12'].Play();


$Shimobe-390 + 2200;
	Sound['2001'].Play();


//▼▼▼ 甲斐常葉 (CC20)

//▲▲▲ 甲斐常葉 (CC20)


$Tokiwa-760 + 850;
	//Sound['n_car'].Play();


$Tokiwa-760 +3000;
	Sound['o_12'].Play();


$Tokiwa-760 +6500;
	Sound['o_13'].Play();


$Tokiwa-760 +10000;
	Sound['o_14'].Play();


$Tokiwa-760 + 13000;
	Sound['2501'].Play();


//▼▼▼ 鰍沢口 (CC25)

//▲▲▲ 鰍沢口 (CC25)


$Kajika-420 + 1300;
	Sound['o_15'].Play();


$Kajika-420 + 2800;
	Sound['2601'].Play();


//▼▼▼ 市川大門 (CC26)

//▲▲▲ 市川大門 (CC26)


$Daimon-490 + 700;
//	Sound['2604'].Play();


$Hanawa+20 + 2200;
	Sound['3201'].Play();


//▼▼▼ 常永 (CC32)

//▲▲▲ 常永 (CC32)


$MinamiKofu+310 + 2100;
	Sound['3801'].Play();


//▼▼▼ 甲府 (CC38/CO43)


// -------------------------------------------------------------------------------------------------------------


// (C)Harupi
