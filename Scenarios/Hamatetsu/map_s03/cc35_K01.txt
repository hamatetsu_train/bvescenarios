﻿BveTs Map 2.00

//////////////////////////////////////////////////////////////////////////
// 83575 - 51535  map_s03/cc35_K1
// 身延線 南甲府(CC35) 場 ～ 下本出
//////////////////////////////////////////////////////////////////////////

// ※駅中心　83K690
$CCWay = $MinamiKofu - 690;



$CCWay +108;
;// ■■□ 場内遠方
	Structure['PoleSL1'].Put(0, -2.6, 0, 0, 0, 0, 0, 1, 1.0);
	Signal['type6'].Put(1, 0, -2.6, 3.5, -0.5, 0, 0, 0, 1, 1.0);


$CCWay +511;
;//●●○ 場内
	Section.BeginNew(0, 2, 4);
	Structure['PoleSL1'].Put(0, -2.6, 0, 0, 0, 0, 0, 1, 1.0);
	Signal['type3'].Put(0, 0, -2.6, 4.8, 0, 0, 0, 0, 1, 1.0);
	Structure['PoleSR1'].Put(0,  2.6, -1.0, 0, 0, 0, 0, 1, 1.0);
	//Signal['type3'].Put(0, 0,  2.6, 3.8, 0, 0, 0, 0, 1, 1.0);
	Structure['Signal3L0'].Put(0, 2.6, 3.8, 0, 0, 0, 0, 1, 1.0);


$CCWay+550;
//
	Structure['Mansion03'].Put(0, -7.0, 0.0, 0.0, 0, -90, 0, 1, 1.0);


$CCWay+570;
// [ □ 】
	Structure['SpeedlimBegin'].Put(0, -2.8, 0, 0, 0, 0, 0, 1, 1.0);
	Structure['Speedlim4'].Put(0, -2.9, 0, 0, 0, 0, 0, 1, 1.0);
	Structure['Speedlim0'].Put(0, -2.7, 0, 0, 0, 0, 0, 1, 1.0);
	Structure['SpeedlimBeginR'].Put(0, -2.8, -0.5, 0, 0, 0, 0, 1, 1.0);
	Structure['Speedlim4'].Put(0, -2.9, -0.5, 0, 0, 0, 0, 1, 1.0);
	Structure['Speedlim0'].Put(0, -2.7, -0.5, 0, 0, 0, 0, 1, 1.0);
	Structure['SpeedlimPole'].Put(0, -2.8, 0, 0, 0, 0, 0, 1, 1.0);


$CCWay + 575;
	Curve.Begin(-329);

	$RailNo='10';
	Track[$RailNo].Position(0.00, 0, 162, 0); // 上本
	Repeater['Rail'+$RailNo+'u'].Begin($RailNo, 0, 0, 0, 0, 0, 0, 3, 5, 5, 'Ballast0', 'Ballast1', 'Ballast2', 'Ballast3', 'Ballast4');
	Repeater['Kasen'+$RailNo].Begin($RailNo, 0, 0, 0, 0, 0, 0, 1, 25, 25, 'KasenS0');
		$dist = distance;
	include 'utility\TongStartR.txt';

	JointNoise.Play(0);


$CCWay+576;
	Repeater['EnePole00'].End();


$CCWay + 600;
	Curve.End();
	Track['10'].Position(1.90, 0, 0, 0); // 上本


$CCWay + 625;
	Structure['Pole0_2'].Put(0, 0.00, 0, 0, 0, 0, 0, 0, 25.0);
	Structure['Pole0_4'].Put('10', 0.00, 0, 0, 0, 0, 0, 0, 25.0);


$CCWay+645;
// [ □ 】
	Structure['SpeedlimB4'].Put(0, 2.7, 0, -1.5, 0, 0, 0, 1, 1.0);
	Structure['SpeedlimB5'].Put(0, 2.9, 0, -1.5, 0, 0, 0, 1, 1.0);
	Structure['SpeedlimPole'].Put(0, 2.8, 0, -0.5, 0, 0, 0, 1, 1.0);


$CCWay + 650;
	Curve.Begin(329);

	$RailNo='01';
	Track[$RailNo].Position(0.00, 0, -329, 0); // 下1
	Repeater['Rail'+$RailNo+'u'].Begin($RailNo, 0, 0, 0, 0, 0, 0, 3, 5, 5, 'Ballast0', 'Ballast1', 'Ballast2', 'Ballast3', 'Ballast4');
	Repeater['Kasen'+$RailNo].Begin($RailNo, 0, 0, 0, 0, 0, 0, 1, 25, 25, 'KasenS0');
		$dist = distance;
	include 'utility\TongStartL.txt';

	Track['10'].Position(9.50, 0, -162, 0); // 上本

	$RailNo='11';
	Track[$RailNo].Position(9.50, 0, 0, 0); // 上1
	Repeater['Rail'+$RailNo+'u'].Begin($RailNo, 0, 0, 0, 0, 0, 0, 3, 5, 5, 'Ballast0', 'Ballast1', 'Ballast2', 'Ballast3', 'Ballast4');
	Repeater['Kasen'+$RailNo].Begin($RailNo, 0, 0, 0, 0, 0, 0, 1, 25, 25, 'KasenS0');
		$dist = distance;
	include 'utility\TongStartR.txt';

	JointNoise.Play(0);
	Structure['Pole1_0'].Put(0, -1.90, 0, 0, 0, 0, 0, 0, 25.0);
	Structure['Pole1_0'].Put('10', -1.90, 0, 0, 0, 0, 0, 0, 25.0);


$CCWay + 675;
	Curve.End();

	$RailNo='02';
	Track[$RailNo].Position(-1.90, 0, 0, 0); // 下2
	Repeater['Rail'+$RailNo+'u'].Begin($RailNo, 0, 0, 0, 0, 0, 0, 3, 5, 5, 'Ballast0', 'Ballast1', 'Ballast2', 'Ballast3', 'Ballast4');
	Repeater['Kasen'+$RailNo].Begin($RailNo, 0, 0, 0, 0, 0, 0, 1, 25, 25, 'KasenS0');
		$dist = distance;
	include 'utility\TongStartL.txt';

	Track['01'].Position(-1.90, 0,  162, 0); // 下１
	Track['10'].Position(11.40, 0,    0, 0); // 上本
	Track['11'].Position(13.30, 0, -162, 0); // 上１


$CCWay + 700;
	$RailNo='03';
	Track[$RailNo].Position(-5.70, 0, 211, 0); // 下3
	Repeater['Rail'+$RailNo+'u'].Begin($RailNo, 0, 0, 0, 0, 0, 0, 3, 5, 5, 'Ballast0', 'Ballast1', 'Ballast2', 'Ballast3', 'Ballast4');
	Repeater['Kasen'+$RailNo].Begin($RailNo, 0, 0, 0, 0, 0, 0, 1, 25, 25, 'KasenS0');
		$dist = distance;
	include 'utility\TongStartR.txt';

	Track['02'].Position(-5.70, 0,  162, 0); // 下２
	Track['01'].Position(-3.80, 0,    0, 0); // 下１
	Track['10'].Position(11.40, 0,    0, 0); // 上本
	Track['11'].Position(15.20, 0,    0, 0); // 上１
	Structure['Pole1_0'].Put('01', -5.10, 0, 0, 0, 0, 0, 0, 25.0);
	Structure['Pole0_4'].Put(   0,  0.10, 0, 0, 0, 0, 0, 0, 25.0);
	Structure['Pole0_4'].Put('10', -0.30, 0, 0, 0, 180, 0, 0, 25.0);
	Structure['Pole0_2'].Put('11',  0.80, 0, 0, 0,   0, 0, 0, 25.0);


//▼▼▼ 南甲府 (CC35)
$CCWay + 710;
	Repeater['Form00S'].Begin0(0, 1, 5.0, 5.0, 'FormR2');
	Repeater['Form00C'].Begin0(0, 1, 5.0, 5.0, 'FormCR2');
$CCWay+710;
// [○B]
	Structure['StaStop99'].Put(0, 2.3, 3.1, 0, 0, 0, 0, 1, 1.0);
	Structure['StaStop99'].Put(0, 2.8, 3.1, 0, 0, 0, 0, 1, 1.0);
	Structure['StaPole00'].Put(0, 2.5, 3.3, 0, 0, 180, 0, 1, 1.0);

$CCWay + 715;
	Repeater['Form10S'].Begin0('10', 1, 5.0, 5.0, 'FormL2');
	Repeater['Form10C'].Begin0('10', 1, 5.0, 5.0, 'FormCL2');

$CCWay + 725;
	Track['03'].Position(-9.50, 0,  162, 0); // 下３
	Track['02'].Position(-7.60, 0,    0, 0); // 下２
	Track['01'].Position(-3.80, 0,    0, 0); // 下１
	Track['10'].Position(11.40, 0,    0, 0); // 上本
	Track['11'].Position(15.20, 0,    0, 0); // 上１
	Structure['Pole1_0'].Put('01', -5.10, 0, 0, 0, 0, 0, 0, 25.0);
	Structure['Pole0_4'].Put(   0,  0.10, 0, 0, 0, 0, 0, 0, 25.0);
	Structure['Pole0_4'].Put('10', -0.30, 0, 0, 0, 180, 0, 0, 25.0);
	Structure['Pole0_2'].Put('11',  0.80, 0, 0, 0,   0, 0, 0, 25.0);

$CCWay + 725;
// [○B]
	Structure['StaStop99'].Put(0,  2.3, 3.9, 0, 0, 0, 0, 1, 1.0);
	Structure['StaStop99'].Put('10', -2.3, 3.9, 0, 0, 0, 0, 1, 1.0);
	Structure['StaStop99'].Put('10', -2.3, 3.5, 0, 0, 0, 0, 1, 1.0);
//	Structure['StaPole00'].Put(0,  2.3, 3.3, 0, 0, 0, 0, 1, 1.0);
//	Structure['StaPole00'].Put('10, -2.3, 3.3, 0, 0, 0, 0, 1, 1.0);

$CCWay + 750;
	Track['03'].Position(-11.40, 0,    0, 0); // 下３
	Track['02'].Position( -7.60, 0,    0, 0); // 下２
	Track['01'].Position( -3.80, 0,    0, 0); // 下１
	Track['10'].Position( 11.40, 0,    0, 0); // 上本
	Track['11'].Position( 15.20, 0,    0, 0); // 上１

$CCWay + 765;
// [○B]
	Structure['StaStop02'].Put(0,  2.3, 3.9, 0, 0, 0, 0, 1, 1.0);
	Structure['StaStop02'].Put('10', -2.3, 3.9, 0, 0, 0, 0, 1, 1.0);
//	Structure['StaPole00'].Put(0,  2.3, 3.3, 0, 0, 0, 0, 1, 1.0);
//	Structure['StaPole00'].Put('10', -2.3, 3.3, 0, 0, 0, 0, 1, 1.0);

$CCWay + 775;
	Structure['Pole2_0'].Put('01', -9.10, 0, 0, 0, 0, 0, 0, 25.0);
	Structure['Pole0_4'].Put(   0,  0.10, 0, 0, 0, 0, 0, 0, 25.0);
	Structure['Pole0_4'].Put('10', -0.30, 0, 0, 0, 180, 0, 0, 25.0);
	Structure['Pole0_2'].Put('11',  0.80, 0, 0, 0,   0, 0, 0, 25.0);

$CCWay+785;
// [○B]
	Structure['StaStop19'].Put(0, 2.5, 3.5, 0, 0, 0, 0, 1, 1.0);
	Structure['StaStop04'].Put(0, 2.6, 3.1, 0, 0, 0, 0, 1, 1.0);
	Structure['StaPole00'].Put(0, 2.5, 3.3, 0, 0, 180, 0, 1, 1.0);
	Structure['StaStop19'].Put('10', -2.5, 3.1, 0, 0, 0, 0, 1, 1.0);
	Structure['StaStop03'].Put('10', -2.6, 3.5, 0, 0, 0, 0, 1, 1.0);
	Structure['StaPole00'].Put('10', -2.5, 3.3, 0, 0, 0, 0, 1, 1.0);

$CCWay+815;
// [○B]
	Structure['StaStop00'].Put(0, 2.3, 3.1, 0, 0, 0, 0, 1, 1.0);
	Structure['StaPole00'].Put(0, 2.3, 3.3, 0, 0, 180, 0, 1, 1.0);
	Structure['StaStop00'].Put('10', 2.3, 3.1, 0, 0, 0, 0, 1, 1.0);
	Structure['StaPole00'].Put('10', 2.3, 3.3, 0, 0, 0, 0, 1, 1.0);

$CCWay + 825;
	Repeater['Form00S'].End();
	Repeater['Form10S'].End();
	Repeater['Form00C'].End();
	Repeater['Form10C'].End();
	Structure['Pole2_0'].Put('01', -9.10, 0, 0, 0, 0, 0, 0, 25.0);
	Structure['Pole0_4'].Put(   0,  0.10, 0, 0, 0, 0, 0, 0, 25.0);
	Structure['Pole2_0'].Put('10', -3.80, 0, 0, 0, 0, 0, 0, 25.0);
//▲▲▲ 南甲府 (CC35)
$CCWay + 825;
	Track['16'].Position(11.40, 0, -160, 0); // 上中
	Repeater['Rail16L'].Begin('16', -0.5335, 0, 0, 0, 0, 0, 3, 5.0, 5.0, 'RailL0', 'RailL1', 'RailL2', 'RailL3', 'RailL4');
	Repeater['Rail16R'].Begin('16',  0.5335, 0, 0, 0, 0, 0, 3, 5.0, 5.0, 'RailR0', 'RailR1', 'RailR2', 'RailR3', 'RailR4');
	Repeater['Rail16u'].Begin('16', 0, 0, 0, 0, 0, 0, 3, 5, 5, 'Ballast0', 'Ballast1', 'Ballast2', 'Ballast3', 'Ballast4');

$CCWay + 850;
	$RailNo='04';
	Track[$RailNo].Position(-11.40, 0, 0, 0); // 下４
	Repeater['Rail'+$RailNo+'u'].Begin($RailNo, 0, 0, 0, 0, 0, 0, 3, 5, 5, 'Ballast0', 'Ballast1', 'Ballast2', 'Ballast3', 'Ballast4');
		$dist = distance;
	include 'utility\TongStartL.txt';

	Track['03'].Position(-11.40, 0, 162, 0); // 下３
	Structure['Pole2_0'].Put('01', -9.10, 0, 0, 0, 0, 0, 0, 25.0);
	Structure['Pole0_4'].Put(   0,  0.10, 0, 0, 0, 0, 0, 0, 25.0);
	Structure['Pole3_0'].Put('10', -6.00, 0, 0, 0, 0, 0, 0, 25.0);


$CCWay + 855;
	Track['16'].Position(8.55, 0, 160, 0); // 上中

$CCWay + 885;
	Track['16'].Position(5.75, 0, 0, 0); // 上中



$CCWay + 875;
	Track['03'].Position(-9.50, 0,    0, 0); // 下３
	Track['02'].Position(-7.60, 0,  162, 0); // 下２

$CCWay + 900;
	$RailNo='03';
	Track[$RailNo].Position(-5.70, 0, 0, 0); // 下３
	Repeater['Rail'+$RailNo+'u'].End();
	Repeater['Kasen'+$RailNo].End();
		$dist = distance;
	include 'utility\TongEndL.txt';

	Track['02'].Position(-5.70, 0, -162, 0); // 下２
	Structure['Pole1_0'].Put('01', -5.10, 0, 0, 0, 0, 0, 0, 25.0);
	Structure['Pole0_4'].Put(   0,  0.10, 0, 0, 0, 0, 0, 0, 25.0);
	Structure['Pole3_0'].Put('10', -6.00, 0, 0, 0, 0, 0, 0, 25.0);


$CCWay + 925;
	Track['04'].Position(-11.40, 0, 162, 0); // 下４

	$RailNo='02';
	Track[$RailNo].Position(-3.80, 0, 0, 0); // 下2
	Repeater['Rail'+$RailNo+'u'].End();
	Repeater['Kasen'+$RailNo].End();
		$dist = distance;
	include 'utility\TongEndL.txt';


$CCWay + 950;
	Track['04'].Position(-9.50, 0,    0, 0); // 下４

	$RailNo='06';
	Track[$RailNo].Position(-3.80, 0, 0, 0); // 坂本精麦
	Repeater['Rail'+$RailNo+'u'].Begin($RailNo, 0, 0, 0, 0, 0, 0, 3, 5, 5, 'Ballast0', 'Ballast1', 'Ballast2', 'Ballast3', 'Ballast4');
		$dist = distance;
	include 'utility\TongStartL.txt';

	Track['01'].Position(-3.80, 0,  162, 0); // 下１
	Track['11'].Position(15.20, 0, -162, 0); // 上１

	$RailNo='17';
	Track[$RailNo].Position(15.20, 0, 0, 0); // 東京ガス
	Repeater['Rail'+$RailNo+'u'].Begin($RailNo, 0, 0, 0, 0, 0, 0, 3, 5, 5, 'Ballast0', 'Ballast1', 'Ballast2', 'Ballast3', 'Ballast4');
		$dist = distance;
	include 'utility\TongStartR.txt';

	Structure['Pole1_0'].Put('01', -5.10, 0, 0, 0, 0, 0, 0, 25.0);
	Structure['Pole0_4'].Put(   0,  0.10, 0, 0, 0, 0, 0, 0, 25.0);
	Structure['Pole3_0'].Put('10', -6.00, 0, 0, 0, 0, 0, 0, 25.0);


$CCWay + 975;
	Curve.Begin(329);
	Track['04'].Position(-5.70, 0, -329, 0); // 下４
	Track['06'].Position(-3.80, 0, -329, 0); // 坂本精麦
	Track['01'].Position(-1.90, 0, -162, 0); // 下１
	Track['16'].Position( 5.70, 0,    0, 0); // 上中
	Track['10'].Position(11.40, 0, -162, 0); // 上本
	Track['11'].Position(13.30, 0,  162, 0); // 上１
	Track['17'].Position(15.20, 0, -329, 0); // 東京ガス
	Repeater['Rail16L'].End();
	Repeater['Rail16R'].End();
	Repeater['Rail16u'].End();
	JointNoise.Play(0);


$CCWay + 1000;
	Curve.End();

	$RailNo='04';
	Track[$RailNo].Position(-4.75, 0, 0, 0); // 下4
	Repeater['Rail'+$RailNo+'u'].End();
	Repeater['Kasen'+$RailNo].End();
		$dist = distance;
	include 'utility\TongEndL.txt';

	Track['06'].Position(-4.75, 0, 0, 0); // 坂本精麦

	$RailNo='01';
	Track[$RailNo].Position(0.00, 0, 0, 0); // 下1
	Repeater['Rail'+$RailNo+'u'].End();
	Repeater['Kasen'+$RailNo].End();
		$dist = distance;
	include 'utility\TongEndL.txt';

	Track['10'].Position( 9.50, 0, 0, 0); // 上本

	$RailNo='11';
	Track[$RailNo].Position(9.50, 0, 0, 0); // 上1
	Repeater['Rail'+$RailNo+'u'].End();
	Repeater['Kasen'+$RailNo].End();
		$dist = distance;
	include 'utility\TongEndR.txt';

	Track['17'].Position(14.25, 0, 0, 0); // 東京ガス
	Structure['Pole2_0'].Put(   0, -6.40, 0, 0, 0, 0, 0, 0, 25.0);
	Structure['Pole1_0'].Put('10', -3.00, 0, 0, 0, 0, 0, 0, 25.0);


$CCWay + 1025;
	Track['06'].Position(-6.65, 0,   0, 0); // 坂本精麦

	$RailNo='05';
	Track[$RailNo].Position(-6.65, 0, 162, 0); // 鈴与KK
	Repeater['Rail'+$RailNo+'u'].Begin($RailNo, 0, 0, 0, 0, 0, 0, 3, 5, 5, 'Ballast0', 'Ballast1', 'Ballast2', 'Ballast3', 'Ballast4');
		$dist = distance;
	include 'utility\TongStartR.txt';

	Track['17'].Position(16.15, 0, 0, 0); // 東京ガス
	Repeater['Rail17L'].End();
	Repeater['Rail17R'].End();
	Repeater['Rail17u'].End();
// [ □ 】
	Structure['SpeedlimBegin'].Put(0, 2.8, 0, 0, 0, 0, 0, 1, 1.0);
	Structure['Speedlim4'].Put(0, 2.7, 0, 0, 0, 0, 0, 1, 1.0);
	Structure['Speedlim0'].Put(0, 2.9, 0, 0, 0, 0, 0, 1, 1.0);
	Structure['SpeedlimPole'].Put(0, 2.8, 0, 0, 0, 0, 0, 1, 1.0);


$CCWay +1046;
;//●●○ 出発
	Section.BeginNew(0, 4);
	Structure['PoleSL1'].Put(0, -2.6, 0, 0, 0, 0, 0, 1, 1.0);
	Signal['type3'].Put(0, 0, -2.6, 4.8, 0, 0, 0, 0, 1, 1.0);


$CCWay + 1050;
	Curve.Begin(-329);
	Track['06'].Position(-8.55, 0,  329, 0); // 坂本精麦
	Track['05'].Position(-6.65, 0, -162, 0); // 鈴与KK
	Track['10'].Position( 1.90, 0,  162, 0); // 上本
	Track['17'].Position(10.45, 0,  329, 0); // 東京ガス
	JointNoise.Play(0);
// [ □ 】
	Structure['SpeedlimBegin'].Put(0, -2.8, 0, 0, 0, 0, 0, 1, 1.0);
	Structure['Speedlim4'].Put(0, -2.9, 0, 0, 0, 0, 0, 1, 1.0);
	Structure['Speedlim0'].Put(0, -2.7, 0, 0, 0, 0, 0, 1, 1.0);
	Structure['SpeedlimPole'].Put(0, -2.8, 0, 0, 0, 0, 0, 1, 1.0);
	Structure['Pole1_0'].Put(   0, -1.90, 0, 0, 0, 0, 0, 0, 25.0);


$CCWay + 1075;
	Curve.End();
	Track['06'].Position(-9.50, 0, 0, 0); // 坂本精麦
	Track['05'].Position(-5.70, 0, 0, 0); // 鈴与KK

	$RailNo='10';
	Track[$RailNo].Position(0.00, 0, 0, 0); // 上本
	Repeater['Rail'+$RailNo+'u'].End();
	Repeater['Kasen'+$RailNo].End();
		$dist = distance;
	include 'utility\TongEndR.txt';

	Track['17'].Position( 9.50, 0, 0, 0); // 東京ガス
	Repeater['Rail17L'].End();
	Repeater['Rail17R'].End();
	Repeater['Rail17u'].End();

	Repeater['EnePole00'].Begin(0, 0, 0, 0, 0, 0, 0, 0, 1.0, 50.0, 'Pole0_2');


$CCWay + 1150;
	Track['06'].Position(-9.50, 0,    0, 0); // 坂本精麦
	Track['07'].Position(-5.70, 0, -162, 0); // 渡
	Track['05'].Position(-5.70, 0,    0, 0); // 鈴与KK
	Repeater['Rail07L'].Begin('07', -0.5335, 0, 0, 0, 0, 0, 3, 5.0, 5.0, 'RailL0', 'RailL1', 'RailL2', 'RailL3', 'RailL4');
	Repeater['Rail07R'].Begin('07',  0.5335, 0, 0, 0, 0, 0, 3, 5.0, 5.0, 'RailR0', 'RailR1', 'RailR2', 'RailR3', 'RailR4');
	Repeater['Rail07u'].Begin('07', 0, 0, 0, 0, 0, 0, 3, 5, 5, 'Ballast0', 'Ballast1', 'Ballast2', 'Ballast3', 'Ballast4');


$CCWay + 1175;
	Track['06'].Position(-9.50, 0, -162, 0); // 坂本精麦
	Track['07'].Position(-7.60, 0,    0, 0); // 渡
	Track['05'].Position(-5.70, 0, -162, 0); // 鈴与KK


$CCWay + 1200;
	Track['06'].Position(-11.40, 0, 0, 0); // 坂本精麦
	Track['07'].Position(-11.40, 0, 0, 0); // 渡
	Track['05'].Position( -7.60, 0, 0, 0); // 鈴与KK
	Repeater['Rail07L'].End();
	Repeater['Rail07R'].End();
	Repeater['Rail07u'].End();


$CCWay + 1225;
	Track['06'].Position(-15.20, 0, 0, 0); // 坂本精麦
	Track['05'].Position(-11.40, 0, 0, 0); // 鈴与KK
	Repeater['Rail05L'].End();
	Repeater['Rail05R'].End();
	Repeater['Rail05u'].End();
	Repeater['Rail06L'].End();
	Repeater['Rail06R'].End();
	Repeater['Rail06u'].End();




//--------------------------------------------------------------------------



// (C)Harupi
