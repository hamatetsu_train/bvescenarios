﻿BveTs Map 2.00

//////////////////////////////////////////////////////////////////////////
// map_s03/Sub_S03_Nature
// 身延線 森系
//////////////////////////////////////////////////////////////////////////



//////////////////////////////////////////////////////////////////////////
//
// CC07_CC09
//
//////////////////////////////////////////////////////////////////////////
$CCWay = $NishiFujinomiya+420;

$CCWay+43;
//◎ 淀師踏切　12.043

$CCWay+364;
//◎ 飢渇(食曷)踏切　12.364

$CCWay+370;
//＊＊＊ 飢渇川 45

$CCWay+531;
//◎ 三人踏切　12.531

$CCWay+997;
//◎ 西山踏切　12.997

$CCWay+1195;
	Structure['Grove01'].Put(0, -8.00, -3.00,  0.00, 0, 0, 0, 1, 0.5);

$CCWay+1460;
	Structure['Grove01'].Put(0, -6.00, -2.00,  0.00, 0, 0, 0, 1, 0.5);

$CCWay+1510;
	Repeater['Grove01'].Begin(0, -10.00, -1.00, 4.00, 0, 0, 0, 1, 7.0, 7.0, 'Grove01');
	Repeater['Grove02'].Begin(0,  -4.00, -5.00, 0.00, 0, 0, 0, 1, 6.0, 6.0, 'Grove03');
	Repeater['Grove03'].Begin(0,  -5.00, -4.00, 3.00, 0, 0, 0, 1, 6.0, 6.0, 'Grove03');

$CCWay+1610;
	Repeater['Grove01'].End();

$CCWay+1640;
	Repeater['Grove02'].End();
	Repeater['Grove03'].End();

$CCWay+1771;
//◎ 別所踏切　13.771

$CCWay+2276;
//◎ 第一安居山踏切　14.276

$CCWay+2401;
//◎ 第二安居山踏切　14.401

$CCWay+2622;
//◎ 東泉寺踏切　14.622

$CCWay+2708;
	Structure['Grove01'].Put(0, 16.00, -1.00, 0.00, 0, 0, 0, 1, 0.5);

$CCWay+2708;
	Structure['Grove01'].Put(0,  8.00, -1.00, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove02'].Put(0, 23.00, -1.00, 0.00, 0, 0, 0, 1, 0.5);

$CCWay+2709;
	Structure['Grove01'].Put(0, 14.00, 1.00, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove03'].Put(0, 18.00, 2.50, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove03'].Put(0, 20.00, 3.00, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove03'].Put(0, 23.00, 3.50, 0.00, 0, 0, 0, 1, 0.5);

$CCWay+2710;
// 墓地
	Structure['Bochi_CC08'].Put(0, 25.0, 0, 0, 0, 0, 0, 0, 50.0);
// 森
	Structure['Grove01'].Put(0, 20.00, 5.00, 0.00, 0, 0, 0, 1, 0.5);

$CCWay+2711;
	Structure['Grove01'].Put(0, 23.00, 7.00, 0.00, 0, 0, 0, 1, 0.5);

$CCWay+2712;
	Structure['Grove01'].Put(0, 25.00, 9.00, 0.00, 0, 0, 0, 1, 0.5);

$CCWay+2715;
// 森
	Structure['Grove03'].Put(0,  3.50, -2.50, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove01'].Put(0,  8.50, -1.50, -1.00, 0, 0, 0, 1, 0.5);

$CCWay+2720;
// Curve.Start
	Structure['Grove01'].Put(0, -7.00, -5.00, 0.00, 0, 0, 0, 1, 0.5);
// 森
	Structure['Grove03'].Put(0,  3.00, -3.00,  0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove03'].Put(0,  5.00,  3.00,  0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove01'].Put(0,  7.00, -2.50, -1.00, 0, 0, 0, 1, 0.5);

$CCWay+2725;
// 森
	Structure['Grove03'].Put(0,  3.50, -2.50, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove01'].Put(0,  8.00, -1.50, -1.00, 0, 0, 0, 1, 0.5);

$CCWay+2730;
// 森
	Structure['Grove03'].Put(0,  3.00, -3.00, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove03'].Put(0,  6.00,  3.00, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove01'].Put(0, 15.00,  4.00, -1.00, 0, 0, 0, 1, 0.5);

$CCWay+2735;
// 森
	Structure['Grove03'].Put(0,  3.50, -2.50, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove01'].Put(0,  8.50, -1.50, -1.00, 0, 0, 0, 1, 0.5);

$CCWay+2740;
// 森
	Structure['Grove03'].Put(0,  3.00, -3.00, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove03'].Put(0,  5.00,  3.00, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove01'].Put(0, 14.00,  4.00, -1.00, 0, 0, 0, 1, 0.5);

$CCWay+2745;
// 森
	Structure['Grove03'].Put(0,  3.50, -2.50, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove01'].Put(0,  8.00, -1.50, -1.00, 0, 0, 0, 1, 0.5);

$CCWay+2750;
// 森
	Structure['Grove03'].Put(0,  3.00, -3.00, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove03'].Put(0,  6.00,  3.00, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove01'].Put(0, 15.00,  4.00, -1.00, 0, 0, 0, 1, 0.5);

$CCWay+2755;
// 森
	Structure['Grove03'].Put(0,  3.50, -2.50, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove01'].Put(0,  8.50, -1.50, -1.00, 0, 0, 0, 1, 0.5);

$CCWay+2760;
// 森
	Structure['Grove03'].Put(0,  3.00, -3.00, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove03'].Put(0,  5.00,  3.00, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove01'].Put(0, 14.00,  4.00, -1.00, 0, 0, 0, 1, 0.5);

$CCWay+2765;
// 森
	Structure['Grove03'].Put(0,  3.50, -2.50, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove01'].Put(0,  8.00, -1.50, -1.00, 0, 0, 0, 1, 0.5);

$CCWay+2770;
// 森
	Structure['Grove03'].Put(0,  3.00, -3.00, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove03'].Put(0,  6.00,  3.00, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove01'].Put(0, 15.00,  4.00, -1.00, 0, 0, 0, 1, 0.5);

$CCWay+2775;
// 森
	Structure['Grove03'].Put(0,  3.50, -2.50, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove01'].Put(0,  8.50, -1.50, -1.00, 0, 0, 0, 1, 0.5);

$CCWay+2780;
// 森
	Structure['Grove03'].Put(0,  3.00, -3.00, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove03'].Put(0,  5.00,  3.00, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove01'].Put(0, 14.00,  4.00, -1.00, 0, 0, 0, 1, 0.5);


$CCWay+2940;
// 森
	Structure['Grove01'].Put(0, -16.00, -2.00, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove01'].Put(0,  -8.00, -1.00, 0.00, 0, 0, 0, 1, 0.5);


$CCWay+3097;
	Structure['Grove01'].Put(0, -14.00, -11.00, 0.00, 0, 0, 0, 1, 0.5);

$CCWay+3098;
	Structure['Grove01'].Put(0, -20.00, -11.00, 0.00, 0, 0, 0, 1, 0.5);

$CCWay+3099;
	Structure['Grove01'].Put(0,  -8.00,  -8.00, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove03'].Put(0, -12.00, -11.00, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove01'].Put(0, -16.00,  -7.00, 0.00, 0, 0, 0, 1, 0.5);

$CCWay+3100;
	Structure['Grove03'].Put(0, -15.00,  -3.50, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove03'].Put(0, -18.00,  -4.00, 0.00, 0, 0, 0, 1, 0.5);

// 森
$CCWay+3100;
	Repeater['Gr01La'].Begin(0, -10.00, -4.00,  0.00, 0, 0, 0, 1, 0.5, 10.0, 'Grove01');
$CCWay+3101;
	Repeater['Gr02La'].Begin(0,  -7.50, -2.00,  0.00, 0, 0, 0, 1, 0.5, 10.0, 'Grove02');
$CCWay+3102;
	Repeater['Gr02Lb'].Begin(0,  -6.50,  0.00,  0.00, 0, 0, 0, 1, 0.5, 10.0, 'Grove02');
$CCWay+3103;
	Repeater['Gr01Lb'].Begin(0, -12.00,  1.00,  0.00, 0, 0, 0, 1, 0.5, 10.0, 'Grove01');
	Repeater['Gr03La'].Begin(0,  -5.00,  3.00,  0.00, 0, 0, 0, 1, 0.5, 20.0, 'Grove03');
	Repeater['Gr03Lb'].Begin(0, -10.00,  3.00,  0.00, 0, 0, 0, 1, 0.5, 10.0, 'Grove03');

$CCWay+4244;
	Repeater['Gr01La'].End();
	Repeater['Gr01Lb'].End();
	Repeater['Gr02La'].End();
	Repeater['Gr02Lb'].End();
	Repeater['Gr03La'].End();
	Repeater['Gr03Lb'].End();


$CCWay+4200;
	Repeater['Gr02Ra'].Begin(0,  17.00, -3.00, 0.00, 0, 0, 0, 1, 0.5, 10.0, 'Grove02');
	Repeater['Gr02Rg'].Begin(0,  25.00,  5.00, 0.00, 0, 0, 0, 1, 0.5, 10.0, 'Grove02');
$CCWay+4201;
// 森
	Repeater['Gr02Rb'].Begin(0,  12.00, -3.00, 0.00, 0, 0, 0, 1, 0.5, 10.0, 'Grove02');
	Repeater['Gr02Rc'].Begin(0,  22.00,  2.00, 0.00, 0, 0, 0, 1, 0.5, 10.0, 'Grove02');
$CCWay+4202;
	Repeater['Gr02Rd'].Begin(0,  19.00, -2.50, 0.00, 0, 0, 0, 1, 0.5, 10.0, 'Grove02');
$CCWay+4205;
// 森
	Repeater['Gr02Rc'].Begin(0,  16.00,  1.50, 0.00, 0, 0, 0, 1, 0.5, 10.0, 'Grove02');
$CCWay+4206;
// 森
	Repeater['Gr02Re'].Begin(0,  13.00, -3.00, 0.00, 0, 0, 0, 1, 0.5, 10.0, 'Grove01');
	Repeater['Gr02Rf'].Begin(0,  24.00,  0.00, 0.00, 0, 0, 0, 1, 0.5, 10.0, 'Grove02');

$CCWay+4350;
// 森
	Structure['Grove02'].Put(0, -19.00, -8.00, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove01'].Put(0, -13.00, -5.00, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove02'].Put(0,  12.00, -4.00, 0.00, 0, 0, 0, 1, 0.5);
$CCWay+4361;
// 森
	Structure['Grove01'].Put(0,  -9.50, -3.00, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove02'].Put(0,  19.00, -1.00, 0.00, 0, 0, 0, 1, 0.5);

$CCWay+4364;
	Repeater['Gr02Ra'].End();
	Repeater['Gr02Rb'].End();
	Repeater['Gr02Rc'].End();
	Repeater['Gr02Rd'].End();
	Repeater['Gr02Re'].End();
	Repeater['Gr02Rf'].End();
	Repeater['Gr02Rg'].End();
	Repeater['Gr02R1'].End();

$CCWay+4365;
// 森
	Structure['Grove01'].Put(0, -21.00, -6.00, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove02'].Put(0, -15.00, -4.00, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove01'].Put(0,  11.00, -3.00, 0.00, 0, 0, 0, 1, 0.5);
$CCWay+4366;
// 森
	Structure['Grove02'].Put(0,  -9.50, -3.00, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove02'].Put(0,  19.00, -1.00, 0.00, 0, 0, 0, 1, 0.5);

$CCWay+4370;
// 森
	Structure['Grove02'].Put(0, -23.00, -5.00, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove02'].Put(0, -16.00, -3.00, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove01'].Put(0,  11.00, -3.00, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove02'].Put(0,  22.00,  4.00, 0.00, 0, 0, 0, 1, 0.5);
$CCWay+4371;
// 森
	Structure['Grove01'].Put(0,  -9.50, -3.00, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove02'].Put(0,  19.00, -1.00, 0.00, 0, 0, 0, 1, 0.5);

$CCWay+4375;
// 森
	Structure['Grove02'].Put(0, -25.00, -5.00, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove02'].Put(0, -17.00, -3.00, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove02'].Put(0,  11.00, -2.00, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove01'].Put(0,  22.00,  5.00, 0.00, 0, 0, 0, 1, 0.5);
$CCWay+4376;
// 森
	Structure['Grove02'].Put(0,  -9.00, -3.00, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove02'].Put(0,  18.00, -3.00, 0.00, 0, 0, 0, 1, 0.5);

$CCWay+4380;
// 森
	Structure['Grove01'].Put(0, -25.00, -5.00, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove01'].Put(0, -17.00, -4.00, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove01'].Put(0,  11.00, -2.00, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove03'].Put(0,  22.00,  5.00, 0.00, 0, 0, 0, 1, 0.5);
$CCWay+4381;
// 森
	Structure['Grove01'].Put(0,  -9.00, -2.00, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove03'].Put(0,  18.00, -3.00, 0.00, 0, 0, 0, 1, 0.5);

$CCWay+4385;
// 森
	Structure['Grove02'].Put(0, -25.00, -4.00, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove01'].Put(0, -17.00, -3.00, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove01'].Put(0,  11.00, -2.00, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove03'].Put(0,  24.00,  5.00, 0.00, 0, 0, 0, 1, 0.5);
$CCWay+4386;
// 森
	Structure['Grove02'].Put(0,  -9.00, -2.00, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove02'].Put(0,  18.00,  0.00, 0.00, 0, 0, 0, 1, 0.5);

$CCWay+4390;
// 森
	Structure['Grove01'].Put(0, -25.00, -4.00, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove01'].Put(0, -17.00, -3.00, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove01'].Put(0,  11.00, -2.00, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove02'].Put(0,  22.00,  5.00, 0.00, 0, 0, 0, 1, 0.5);
$CCWay+4391;
// 森
	Structure['Grove01'].Put(0,  -9.00, -2.00, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove03'].Put(0,  18.00, -3.00, 0.00, 0, 0, 0, 1, 0.5);

$CCWay+4395;
// 森
	Structure['Grove04'].Put(0, -25.00, -4.00, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove02'].Put(0, -17.00, -3.00, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove02'].Put(0,  11.00, -3.00, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove02'].Put(0,  22.00,  5.00, 0.00, 0, 0, 0, 1, 0.5);
$CCWay+4396;
// 森
	Structure['Grove02'].Put(0,  -9.00, -1.00, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove04'].Put(0,  18.00, -3.00, 0.00, 0, 0, 0, 1, 0.5);

$CCWay+4400;
// 森
	Structure['Grove01'].Put(0, -25.00, -4.00, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove01'].Put(0, -17.00, -3.00, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove01'].Put(0,  11.00, -2.00, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove02'].Put(0,  22.00,  5.00, 0.00, 0, 0, 0, 1, 0.5);
$CCWay+4401;
// 森
	Structure['Grove01'].Put(0,  -9.00, -1.00, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove03'].Put(0,  18.00, -3.00, 0.00, 0, 0, 0, 1, 0.5);

$CCWay+4405;
// 森
	Structure['Grove01'].Put(0, -17.00, -3.00, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove01'].Put(0,  10.00, -2.00, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove02'].Put(0,  18.00,  3.00, 0.00, 0, 0, 0, 1, 0.5);
$CCWay+4406;
// 森
	Structure['Grove01'].Put(0,  -9.00, -1.00, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove03'].Put(0,  22.00,  5.00, 0.00, 0, 0, 0, 1, 0.5);

$CCWay+4410;
// 森
	Structure['Grove01'].Put(0, -16.00, -2.00, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove02'].Put(0,  10.00, -2.00, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove02'].Put(0,  18.00,  3.00, 0.00, 0, 0, 0, 1, 0.5);
$CCWay+4411;
// 森
	Structure['Grove01'].Put(0,  -9.00, -1.00, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove03'].Put(0,  22.00,  5.00, 0.00, 0, 0, 0, 1, 0.5);

$CCWay+4415;
// 森
	Structure['Grove01'].Put(0, -15.00,  2.00, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove01'].Put(0,  10.00, -2.00, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove02'].Put(0,  18.00,  3.00, 0.00, 0, 0, 0, 1, 0.5);
$CCWay+4416;
// 森
	Structure['Grove01'].Put(0,  -9.00, -2.00, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove03'].Put(0,  13.00, -3.00, 0.00, 0, 0, 0, 1, 0.5);

$CCWay+4420;
// 森
	Structure['Grove01'].Put(0, -14.00,  3.00, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove01'].Put(0,   8.00, -2.00, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove02'].Put(0,  10.00, -1.00, 0.00, 0, 0, 0, 1, 0.5);
$CCWay+4421;
// 森
	Structure['Grove02'].Put(0,  -8.00, -2.00, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove03'].Put(0,  13.00,  1.00, 0.00, 0, 0, 0, 1, 0.5);

$CCWay+4425;
// 森
	Structure['Grove01'].Put(0, -13.00,  4.00, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove01'].Put(0,   8.00, -1.00, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove02'].Put(0,  10.00, -1.00, 0.00, 0, 0, 0, 1, 0.5);
$CCWay+4426;
// 森
	Structure['Grove01'].Put(0,  -8.00, -1.00, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove03'].Put(0,  13.00,  1.00, 0.00, 0, 0, 0, 1, 0.5);

$CCWay+4430;
// 森
	Structure['Grove02'].Put(0, -13.00,  4.00, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove01'].Put(0,   8.00,  0.00, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove03'].Put(0,  10.00, -1.00, 0.00, 0, 0, 0, 1, 0.5);
$CCWay+4431;
// 森
	Structure['Grove01'].Put(0,  -8.00, -1.00, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove03'].Put(0,  13.00,  1.00, 0.00, 0, 0, 0, 1, 0.5);

$CCWay+4435;
// 森
	Structure['Grove01'].Put(0,  -8.00, -1.00, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove01'].Put(0,   8.00,  0.00, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove03'].Put(0,  12.00,  0.00, 0.00, 0, 0, 0, 1, 0.5);
$CCWay+4436;
// 森
	Structure['Grove01'].Put(0, -10.00,  1.00, 0.00, 0, 0, 0, 1, 0.5);

//▼▼▼ 沼久保 (CC08)
$CCWay+4440;
// 森
	Structure['Wood03'].Put(0, -5.00, -2.00, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove01'].Put(0, 12.00, 0.00, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove03'].Put(0, 19.00, 3.00, 0.00, 0, 0, 0, 1, 0.5);

$CCWay+4445;
// 森
	Structure['Wood05'].Put(0, -5.00, -2.00, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove02'].Put(0, 12.00, 0.00, 0.00, 0, 0, 0, 1, 0.5);

$CCWay+4450;
// 森
	Structure['Wood03'].Put(0, -5.00, -2.00, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove01'].Put(0, 12.00, -0.50, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove03'].Put(0, 19.00, 3.00, 0.00, 0, 0, 0, 1, 0.5);

$CCWay+4455;
// 森
	Structure['Wood02'].Put(0, -5.00, -1.50, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove01'].Put(0, 12.00, 0.00, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove03'].Put(0, 18.00, 2.00, 0.00, 0, 0, 0, 1, 0.5);

$CCWay+4460;
// 森
	Structure['Wood02'].Put(0, -5.00, -1.50, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove01'].Put(0, 11.00, 0.00, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove03'].Put(0, 18.00, 0.00, 0.00, 0, 0, 0, 1, 0.5);

$CCWay+4465;
// 森
	Structure['Wood02'].Put(0, -6.00, -2.00, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove04'].Put(0, 11.00, 0.00, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove03'].Put(0, 15.00, 2.00, 0.00, 0, 0, 0, 1, 0.5);

$CCWay+4470;
// 森
	Structure['Wood01'].Put(0, -5.00, -2.00, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove01'].Put(0, 13.00, 1.00, 0.00, 0, 0, 0, 1, 0.5);

$CCWay+4475;
// 森
	Structure['Grove02'].Put(0, 11.00, 0.00, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove03'].Put(0, 17.00, 2.00, 0.00, 0, 0, 0, 1, 0.5);

$CCWay+4480;
// 森
	Structure['Grove01'].Put(0, 12.00, 0.00, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove03'].Put(0, 17.00, 2.50, 0.00, 0, 0, 0, 1, 0.5);

$CCWay+4485;
// 森
	Structure['Grove02'].Put(0, 12.00, 0.00, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove03'].Put(0, 18.00, 2.00, 0.00, 0, 0, 0, 1, 0.5);

$CCWay+4490;
// 森
	Structure['Grove01'].Put(0, 12.00, -2.00, 0.00, 0, 0, 0, 1, 0.5);

$CCWay+4495;
// 森
	Structure['Grove01'].Put(0, 15.00, -5.00, 0.00, 0, 0, 0, 1, 0.5);

$CCWay+4500;
// 森
	Structure['Grove04'].Put(0, 11.00,  2.00,  0.00, 0, 0, 0, 1, 0.5);

$CCWay+4520;
//▲▲▲ 沼久保 (CC08)

$CCWay+4533;
//◎ 沼久保踏切　16.533

$CCWay+4540;
// 森
	Structure['Grove03'].Put(0,  -4.00, -4.00, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove01'].Put(0, -12.00, -3.00, 0.00, 0, 0, 0, 1, 0.5);

$CCWay+4545;
// 森
	Structure['Grove04'].Put(0,  -8.00, -1.00, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove01'].Put(0, -14.00, -4.00, 0.00, 0, 0, 0, 1, 0.5);

$CCWay+4550;
// 森
	Structure['Grove01'].Put(0,  -6.00, -3.00, 0.00, 0, 0, 0, 1, 0.5);
	Structure['Grove01'].Put(0, -16.00, -1.00, 0.00, 0, 0, 0, 1, 0.5);

$CCWay+4645;
	Structure['Grove01'].Put(0, -25.00, 5.00, 0.00, 0, 0, 0, 1, 0.5);

$CCWay+4650;
	Structure['Grove03'].Put(0, -25.00, 5.00, 0.00, 0, 0, 0, 1, 0.5);

$CCWay+4655;
	Structure['Grove01'].Put(0, -25.00, 5.00, 0.00, 0, 0, 0, 1, 0.5);

$CCWay+4690;
;//●□● [01]沼久保 152

$CCWay+5110;
;//●□● [02]小田 91

$CCWay+5353;
//◎ 小田踏切　17.353

$CCWay+5639;
//◎ 湯ノ沢踏切　17.639

$CCWay+6354;
//◎ 月代踏切　18.354

//--------------------------------------------------------------------------



// (C)Harupi
