﻿BveTs Map 2.00

//////////////////////////////////////////////////////////////////////////
// 66700 - 69400  map_s03/cc25_cc26
// 身延線 鰍沢口(CC25) 下本出 ～ 市川大門(CC26) 場内
//////////////////////////////////////////////////////////////////////////

// ※駅中心　66K420
$CCWay = $Kajika-420;


$CCWay+700;
	Repeater['EnePole00'].Begin(0, 0, 0, 0, 0, 0, 0, 0, 25.0, 25.0, 'Pole0_2');

$CCWay+701;
		$dist = distance;
		$radius=800;
		$cant=25;
	include 'utility\CurveStart.txt';

$CCWay+722;
//◎ 久保沼踏切　66.722
	Structure['Fumikiri_AL'].Put0(0, 0, 5);
	Structure['Fumikiri_AC'].Put0(0, 0, 5);
	Structure['Fumikiri_AR'].Put0(0, 0, 5);
	Sound3D['Fumikiri'].Put(-2.5, 3.0);

$CCWay+725;
	Repeater['EnePole00'].Begin(0, 0, 0, 0, 0, 180, 0, 0, 25.0, 25.0, 'Pole0_4');

$CCWay+740;
		$dist = distance;
		$slope=10;
	include 'utility\GraUpStart.txt';

$CCWay+750;
	Repeater['EnePole00'].Begin(0, 0, 0, 0, 0, 0, 0, 0, 25.0, 25.0, 'Pole0_2');

$CCWay+775;
	Repeater['EnePole00'].Begin(0, 0, 0, 0, 0, 180, 0, 0, 25.0, 25.0, 'Pole0_4');

$CCWay+825;
;// ●●○ (上本場内)
	//Structure['PoleSL1'].Put(0, -2.8, -1.0, 0, 0, 0, 0, 1, 1.0);
	Structure['SignalB'].Put(0, -2.8,  0.0, 0, 0,  0, 0, 1, 1.0);

$CCWay+900;
		$dist = distance;
	include 'utility\CurveEnd.txt';

$CCWay+1031;
;// ｣｢ 

$CCWay+1050;
		$dist = distance;
	include 'utility\GraUpEnd.txt';

$CCWay+1080;
//＊＊＊ 八ノ尻川 13
	RollingNoise.Change(3);
	Repeater['Rail00u'].End();
	Repeater['Bridge00'].Begin(0, 0, 0, 5.0, 0, 0, 0, 3, 5, 5, 'Bridge01');
	Repeater['Bridge_Makuragi0'].Begin(0, 0, 0, 2.0, 0, 0, 0, 3, 2, 2, 'Bridge_Makuragi');
	Repeater['BridgeuR'].Begin(0, 0, 0, 5.0, 0, 0, 0, 3, 5.0, 5.0, 'BridgeSGR0');

$CCWay+1095;
//＊＊＊
	RollingNoise.Change(0);
	Repeater['Rail00u'].Begin(0, 0, 0, 0, 0, 0, 0, 3, 5.0, 5.0, 'Ballast0', 'Ballast1', 'Ballast2', 'Ballast3', 'Ballast4');
	Repeater['Bridge00'].End();
	Repeater['Bridge_Makuragi0'].End();
	Repeater['BridgeuR'].End();

$CCWay+1100;
//◎ 八の尻踏切　67.100
	Structure['Fumikiri_AL'].Put0(0, 0, 5);
	Structure['Fumikiri_AC'].Put0(0, 0, 5);
	Structure['Fumikiri_AR'].Put0(0, 0, 5);
	Sound3D['Fumikiri'].Put(-2.5, 3.0);

$CCWay+1150;
		$dist = distance;
		$slope=-16.7;
	include 'utility\GraDownStart.txt';


$CCWay+1340;
// 中部横断自動車道
	Structure['OverRoad01'].Put(0, 0, 6.0, 0, 0, 0, 0, 1, 1.0);


$CCWay+1440;
		$dist = distance;
	include 'utility\GraDownEnd.txt';

$CCWay+1450;
	Repeater['EnePole00'].Begin(0, 0, 0, 0, 0, 0, 0, 0, 25.0, 25.0, 'Pole0_2');

$CCWay+1475;
	Repeater['EnePole00'].Begin(0, 0, 0, 0, 0, 180, 0, 0, 25.0, 25.0, 'Pole0_4');

$CCWay+1549;
//◎ 大鳥居第一踏切　67.549
	Structure['Fumikiri_BL'].Put0(0, 0, 5);
	Structure['Fumikiri_BC'].Put0(0, 0, 5);
	Structure['Fumikiri_BR'].Put0(0, 0, 5);
	Sound3D['Fumikiri'].Put(-2.5, 3.0);

$CCWay+1700;
		$dist = distance;
		$radius=1200;
		$cant=25;
	include 'utility\CurveStart.txt';

$CCWay+2000;
		$dist = distance;
	include 'utility\CurveEnd.txt';

$CCWay+2010;
		$dist = distance;
		$slope=1.3;
	include 'utility\GraUpStart.txt';

$CCWay+2019;
//◎ 大鳥居第二踏切　68.019

$CCWay+2190;
		$dist = distance;
	include 'utility\GraUpEnd.txt';

$CCWay+2390;
		$dist = distance;
		$slope=1.5;
	include 'utility\GraUpStart.txt';

$CCWay+2400;
		$dist = distance;
		$radius=-400;
		$cant=-35;
	include 'utility\CurveStart.txt';

$CCWay+2531;
//◎ 川下踏切　68.531
	Structure['Fumikiri_AL'].Put0(0, 0, 5);
	Structure['Fumikiri_AC'].Put0(0, 0, 5);
	Structure['Fumikiri_AR'].Put0(0, 0, 5);
	Sound3D['Fumikiri'].Put(-2.5, 3.0);

$CCWay+2590;
		$dist = distance;
	include 'utility\CurveEnd.txt';


$CCWay+2575;
	Repeater['EnePole00'].Begin(0, 0, 0, 0, 0, 0, 0, 0, 25.0, 25.0, 'Pole0_2');


$CCWay+2595;
;//●□● [28] 押出川 26
	Repeater['EnePole00'].End();
		$dist = distance;
	include 'utility\SingleTunnelStart.txt';

	Structure['DikeL'].Put(0, -55.00,  7.00,  6.00, 0, -90, 0, 3, 10.0);
	Structure['DikeL'].Put(0, -45.00,  7.20,  6.00, 0, -90, 0, 3, 10.0);
	Structure['DikeL'].Put(0, -35.00,  7.40,  6.00, 0, -90, 0, 3, 10.0);
	Structure['DikeL'].Put(0, -25.00,  7.60,  6.00, 0, -90, 0, 3, 10.0);
	Structure['DikeL'].Put(0, -15.00,  7.80,  6.00, 0, -90, 0, 3, 10.0);
	Structure['DikeL'].Put(0,  -5.00,  8.00,  6.00, 0, -90, 0, 3, 10.0);
	Structure['DikeL'].Put(0,  15.00,  8.00,  6.00, 0, -90, 0, 3, 10.0);
	Structure['DikeL'].Put(0,  25.00,  8.00,  6.00, 0, -90, 0, 3, 10.0);
	Structure['DikeL'].Put(0,  35.00,  8.00,  6.00, 0, -90, 0, 3, 10.0);
	Structure['DikeL'].Put(0,  45.00,  8.00,  6.00, 0, -90, 0, 3, 10.0);
	Structure['DikeL'].Put(0,  55.00,  8.00,  6.00, 0, -90, 0, 3, 10.0);
	Structure['DikeL'].Put(0,  65.00,  8.00,  6.00, 0, -90, 0, 3, 10.0);
	Structure['DikeL'].Put(0,  75.00,  8.00,  6.00, 0, -90, 0, 3, 10.0);
	Structure['DikeL'].Put(0,  85.00,  8.00,  6.00, 0, -90, 0, 3, 10.0);
	Structure['DikeL'].Put(0,  95.00,  8.00,  6.00, 0, -90, 0, 3, 10.0);
	Structure['DikeL'].Put(0,  -5.00,  8.00, 12.00, 0, -90, 0, 3, 10.0);
	Structure['DikeL'].Put(0,  15.00,  8.00, 12.00, 0, -90, 0, 3, 10.0);


$CCWay+2620;
;//○□○ [28] 押出川
		$dist = distance;
	include 'utility\SingleTunnelEnd.txt';


$CCWay+2675;
	Repeater['EnePole00'].Begin(0, 0, 0, 0, 0, 0, 0, 0, 25.0, 25.0, 'Pole0_2');

$CCWay+2725;
	Repeater['EnePole00'].Begin(0, 0, 0, 0, 0, 180, 0, 0, 25.0, 25.0, 'Pole0_4');

$CCWay+2742;
//◎ 浦町踏切　68.742
	Structure['Fumikiri_AL'].Put0(0, 0, 5);
	Structure['Fumikiri_AC'].Put0(0, 0, 5);
	Structure['Fumikiri_AR'].Put0(0, 0, 5);
	Sound3D['Fumikiri'].Put(-2.5, 3.0);

$CCWay+2790;
		$dist = distance;
	include 'utility\GraUpEnd.txt';

$CCWay+2800;
		$dist = distance;
		$radius=500;
		$cant=35;
	include 'utility\CurveStart.txt';
$CCWay+2800;
// [ □ 】
//	Structure['SpeedlimBegin'].Put(0, -2.3, -1.0, 0, 0, 0, 0, 1, 1.0);
	Structure['SpeedlimB8'].Put(0, -2.4, -1.0, 0, 0, 0, 0, 1, 1.0);
	Structure['SpeedlimB0'].Put(0, -2.2, -1.0, 0, 0, 0, 0, 1, 1.0);
	Structure['SpeedlimPole'].Put(0, -2.3, -1.0, 0, 0, 0, 0, 1, 1.0);

$CCWay+2882;
//◎ 下宿踏切　68.882
	Structure['Fumikiri_BL'].Put0(0, 0, 5);
	Structure['Fumikiri_BC'].Put0(0, 0, 5);
	Structure['Fumikiri_BR'].Put0(0, 0, 5);
	Sound3D['Fumikiri'].Put(-2.5, 3.0);

$CCWay+3050;
	Repeater['EnePole00'].Begin(0, 0, 0, 0, 0, 0, 0, 0, 25.0, 25.0, 'Pole0_2');

$CCWay+3075;
	Repeater['EnePole00'].Begin(0, 0, 0, 0, 0, 180, 0, 0, 25.0, 25.0, 'Pole0_4');

$CCWay+3079;
//◎ 鳴沢踏切　69.079
	Structure['Fumikiri_BL'].Put0(0, 0, 5);
	Structure['Fumikiri_BC'].Put0(0, 0, 5);
	Structure['Fumikiri_BR'].Put0(0, 0, 5);
	Sound3D['Fumikiri'].Put(-2.5, 3.0);

$CCWay+3120;
		$dist = distance;
		$slope=10;
	include 'utility\GraUpStart.txt';

$CCWay+3257;
//◎ 今村踏切　69.257
	Structure['Fumikiri_BL'].Put0(0, 0, 5);
	Structure['Fumikiri_BC'].Put0(0, 0, 5);
	Structure['Fumikiri_BR'].Put0(0, 0, 5);
	Sound3D['Fumikiri'].Put(-2.5, 3.0);

$CCWay+3320;
		$dist = distance;
		$slope2=2.5;
	include 'utility\GraUpChange.txt';

$CCWay+3330;
		$dist = distance;
	include 'utility\CurveEnd.txt';

$CCWay+3357;
//◎ 高田踏切　69.357
	Structure['Fumikiri_AL'].Put0(0, 0, 5);
	Structure['Fumikiri_AC'].Put0(0, 0, 5);
	Structure['Fumikiri_AR'].Put0(0, 0, 5);
	Sound3D['Fumikiri'].Put(-2.5, 3.0);




//--------------------------------------------------------------------------



// (C)Harupi
