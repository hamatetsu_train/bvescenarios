﻿BveTs Map 2.00

//富士→身延　停車駅接近警報
///////////////////////////////////////
// 富士駅 (CC00)
$Fuji - 30;
	Beacon.Put(9, 0, 300);
$Fuji + 0; // <E>


///////////////////////////////////////
// 十島駅 (CC11)
$Toshima - 620; // 
	Beacon.Put(8, 0, 4);
	Beacon.Put(9, 0, 311);

$Toshima-900 + 880; // <0><E>


///////////////////////////////////////
// 内船駅 (CC14)
$Utsubuna - 550; //
	Beacon.Put(8, 0, 0);
	Beacon.Put(9, 0, 314);

$Utsubuna-750 + 810; // <2>
$Utsubuna-750 + 830; // <3><E>
$Utsubuna-750 + 840; // <4>
$Utsubuna-750 + 845; // <0>


///////////////////////////////////////
// 身延駅 (CC16)
$Minobu - 550; //
	Beacon.Put(8, 0, 0);
	Beacon.Put(9, 0, 316);

$Minobu-160 + 170; // <折><2>
$Minobu-160 + 180; // <2>
$Minobu-160 + 200; // <3>
$Minobu-160 + 205; // <E>
$Minobu-160 + 220; // <4>
$Minobu-160 + 240; // <0>


///////////////////////////////////////
// 甲斐常葉駅 (CC20)
$Tokiwa-760 + 135; // 
	Beacon.Put(8, 0, 0);
	Beacon.Put(9, 0, 320);

$Tokiwa-760 + 735; // <O><E>


///////////////////////////////////////
// 鰍沢口駅 (CC25)
$Kajika-420 - 40; // 
	Beacon.Put(8, 0, 0);
	Beacon.Put(9, 0, 325);

$Kajika-420 + 480; // <2>
$Kajika-420 + 500; // <3><E>
$Kajika-420 + 520; // <4>
$Kajika-420 + 560; // <0>


///////////////////////////////////////
// 市川大門駅 (CC26)
$Daimon-490 - 5; // 
	Beacon.Put(8, 0, 0);
	Beacon.Put(9, 0, 326);

$Daimon-490 + 560; // <2>
$Daimon-490 + 580; // <3><E>
$Daimon-490 + 595; // <0>


///////////////////////////////////////
// 東花輪駅 (CC30)
$Hanawa-980 + 470; // 
	Beacon.Put(8, 0, 4);
	Beacon.Put(9, 0, 330);

$Hanawa-980 + 1030; // <2>
$Hanawa-980 + 1050; // <3>
$Hanawa-980 + 1055; // <E>
$Hanawa-980 + 1070; // <0>


///////////////////////////////////////
// 国母駅 (CC33)
$Hanawa+20 + 4340; // 
	Beacon.Put(8, 0, 4);
	Beacon.Put(9, 0, 333);

$Hanawa+20 + 4900; // <2>
$Hanawa+20 + 4920; // <3>
$Hanawa+20 + 4930; // <E>
$Hanawa+20 + 4940; // <0>


///////////////////////////////////////
// 甲府駅 (CC38)
$Kofu-1100 + 195; // 
	Beacon.Put(8, 0, 0);
	Beacon.Put(9, 0, 338);

$Kofu-1100 + 875; // [4]<O>
$Kofu-1100 + 895; // [5]<O>


