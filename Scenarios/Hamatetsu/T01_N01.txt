﻿BveTs Map 2.00

//////////////////////////////////////////////////////////////////////////
// T01_N01
// 東海道線 普通 名古屋(CA--)[2] ～ 豊橋(CA42)
//////////////////////////////////////////////////////////////////////////

// 路線基本ファイル
	include 'map_s01\Map_T01.txt';

// ATS-S

//////////////////////////////////////////////////////////////////////////
0;
// ATS設定
	Beacon.Put(50, 1, 4); // PT
1;
// 線区最高速度
	Beacon.Put(61, 0, 120);

// 閉塞初期値代入
$SecA = 3996;
$SecB = 3996;
$Slope2=0;
//////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////
$gamagori=1000; // -310600
	include 'map_s01\CA47_N01.txt';
	include 'map_s01\CA47_CA44.txt';
$mito=$gamagori+8530; // -302070
	include 'map_s01\CA44_N01.txt';
	include 'map_s01\CA44_CA43.txt';

$kozakai=$mito+3650; // -298420
	include 'map_s01\CA43_N01.txt';
	include 'map_s01\CA43_CA42.txt';

$toyohashi=$kozakai+4820; // -293600
	include 'map_s01\CA42_N06b.txt';


//////////////////////////////////////////////////////////////////////////
// 駅ファイル
	Station.Load('map_s01\Sub_T01N01_Stations.txt');
// 停止目標
	include 'map_s01\Sub_T01N01_Stops.txt';
// 閉塞
	//include 'map_s01\Sub_S01N01_Sections.txt';
// 曲線
	//include 'map_s01\Sub_S01N01_curve.txt';
// 他列車
	include 'map_s01\Sub_S01T01_trains.txt';



// (C)Harupi
